#include <QStringList>
#include <QFileInfo>
#include <QTextCodec>
#include <QDir>
#include <QTextStream>
#include <QFile>
#ifdef DEBUG
    #include <QDebug>
#endif

#include <time.hpp>
#include <settings.hpp>
#include <string>
#include <config_parser.hpp>
#include <error.hpp>

using namespace std;

_config _gl_set::config;

class _config_file:public _config_parser_from_file
{
public:
    _config_file() {};
    virtual ~_config_file() {};

    virtual void parse_done() const;
};

// ----------------------------------------------
void _config_file::parse_done() const
{
    //_gl_set().config.notify.open_main_wnd=false;
    //_gl_set().config.notify.sound=false;
    //_gl_set().config.notify.tray==false;

    _gl_set().config.ui.ind.proc_tikcet_n=true;
    _gl_set().config.ui.ind.proc_tikcet_serv_time=true;
    _gl_set().config.ui.ind.serv2day=true;

    _gl_set().config.run.message_dif_to_close = false;

    _config_parser::_params_map::const_iterator it=cm_params_map.begin();
    while(it!=cm_params_map.end())
    {
        if      (it->first=="server") _gl_set().config.server.name=it->second;
        else if (it->first=="port") _gl_set().config.server.port=atoi(it->second.c_str());

        else if (it->first=="mark_console_type") _gl_set().config.mark_console.type=it->second;
        else if (it->first=="mark_console_port") _gl_set().config.mark_console.port=it->second;
        else if (it->first=="mark_console_number") _gl_set().config.mark_console.number=atoi(it->second.c_str());

        //else if (it->first=="notify_open_wnd") { if (it->second=="yes" || it->second=="1") _gl_set().config.notify.open_main_wnd=true; }
        //else if (it->first=="notify_sound")    { if (it->second=="yes" || it->second=="1") _gl_set().config.notify.sound=true; }
        //else if (it->first=="notify_tray")     { if (it->second=="yes" || it->second=="1") _gl_set().config.notify.tray=true; }

        else if (it->first=="report_url") _gl_set().config.external.report_url=it->second;
        else if (it->first=="video_cams") _gl_set().config.external.video_cams=it->second;
        else if (it->first=="run_at_exit") _gl_set().config.external.run_at_exit=it->second;

        else if (it->first=="log_levels") _gl_set().config.run.log_levels=it->second;
        else if (it->first=="audio_recorder") _gl_set().config.run.record_audio = QString(it->second.c_str()).toInt();
        else if (it->first=="create_ticket_enable") _gl_set().config.run.create_ticket_enable = QString(it->second.c_str()).toInt();
        else if (it->first=="message_dif_to_close") _gl_set().config.run.message_dif_to_close = QString(it->second.c_str()).toInt(); //Выводить предупреждение если превышено время работы с талоном
        else
        {
            throw(_error(-1,string("Неизвестный параметр ")+it->first+" в локальном файле конфигурации."));
        }

        it++;
    }

    //if (_monitor_status().main_server_name().empty())
    //    throw(_error(-1,"В локальном файле конфигурации не указан главный сервер (main_server)."));
}

// ------------------------------------------------------------------------
void _gl_set::load_config_file(const std::string &fname)
{
    _config_file cf;
    cf.load(fname);
}

// ------------------------------------------------------------------------
void _gl_set::update_config_file(const std::string &fname)
{

}


/*
_settings* create_rdg_settings() { return new _settings(); }

_settings::_settings(QObject *parent)
    : QObject(parent),
    m_user_settings(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName() + "User"),
    m_program_settings(get_fname(), QSettings::IniFormat),
    m_path_out_log(".\\log\\out")
{
//    QTextCodec *codec = QTextCodec::codecForName("CP1251");
    QTextCodec *codec = QTextCodec::codecForName("utf-8");
    m_program_settings.setIniCodec(codec);
    m_user_settings.setIniCodec(codec);
}

QString _settings::get_fname(void) //throw (string&)
{
    string fpath = QCoreApplication::applicationFilePath().toStdString();
#ifdef WINDOWS
    size_t pos = fpath.find(".exe");
    fpath.replace(pos, 4, ".ini");
#else
    fpath += ".ini";
#endif
    return QString::fromStdString(fpath);
}

QString _settings::reports_url() const { return m_reports_url; }

void _settings::write_settings()
{
    m_program_settings.beginGroup("/system");
        m_program_settings.setValue("/server", m_server);              // адрес сервера (ip или dns)
        m_program_settings.setValue("/port", m_port);                  // Порт сервера
        m_program_settings.setValue("/log_level", m_log_level);        //TODO different levels for log2
        m_program_settings.setValue("/mark_console_type", m_mark_console_type); // quality mark panel type
        m_program_settings.setValue("/mark_console_port", m_mark_console_port); // quality mark panel COM port
        m_program_settings.setValue("/mark_console_number", m_mark_console_number); // quality mark panel COM port
        m_program_settings.setValue("/office_id", m_office_id);        // TODO office - ?
        m_program_settings.setValue("/office_name", m_office_name);    // TODO office - ?

        m_program_settings.setValue("/run_at_exit", cm_run_at_exit);

        m_program_settings.setValue("/cams", cm_video_cams);
    m_program_settings.endGroup();

    // Сохраняются в "c:\Documents and Settings\<user>\Application Data\RDGroup\RDG_EQ_Operator.ini"
    m_user_settings.beginGroup("/auth");
        m_user_settings.setValue("/last_logon_workplace", m_workplace);
        m_user_settings.setValue("/last_logon_name", m_name);
    m_user_settings.endGroup();

    m_user_settings.beginGroup("/ui");
        m_user_settings.setValue("/ticket_notify_mainwnd", m_ticket_notify_mainwnd);
        m_user_settings.setValue("/ticket_notify_tray", m_ticket_notify_tray);
        m_user_settings.setValue("/ticket_notify_sound", m_ticket_notify_sound);
        m_user_settings.setValue("/call_ticket_one_button", m_call_ticket_single_button);
    m_user_settings.endGroup();

    m_user_settings.beginGroup("/indicators");
        m_user_settings.setValue("/ticket_in_proc", m_ticket_in_proc);
        m_user_settings.setValue("/ticket_in_proc_service_time", m_ticket_in_proc_service_time);
        m_user_settings.setValue("/served_ticket_total2day", m_served_ticket_total2day);
//        m_user_settings.setValue("/redirected_ticket_total2day", m_redirected_ticket_total2day);
//        m_user_settings.setValue("/absent_ticket_total2day", m_absent_ticket_total2day);
        m_user_settings.setValue("/waiting_time_limit", m_waiting_time_limit_ms);
        m_user_settings.setValue("/service_time_limit", m_service_time_limit_ms);
    m_user_settings.endGroup();
}

void _settings::read_settings()
{
    //TODO check settings file version

    m_program_settings.beginGroup("/system");
        m_server = m_program_settings.value("/server", "localhost").toString();
        m_port = m_program_settings.value("/port", 0).toInt();

        cm_video_cams = m_program_settings.value("/cams", "").toString();

        m_log_level = m_program_settings.value("/log_level", 0).toInt();
        m_new_log_level = m_log_level;

        m_mark_console_type = m_program_settings.value("/mark_console_type", "").toString();
        m_new_mark_console_type = m_mark_console_type;

        m_mark_console_port = m_program_settings.value("/mark_console_port", "").toString();
        m_new_mark_console_port = m_mark_console_port;

        m_mark_console_number = m_program_settings.value("/mark_console_number", "").toString();
        m_new_mark_console_number = m_mark_console_number;

        //TODO office - ?
        QVariant id_v = m_program_settings.value("/office_id", "0").toString();
        m_office_id = id_v.toString();
        QVariant name_v = m_program_settings.value("/office_name",QString::fromLocal8Bit("не установлено")).toString();
        m_office_name = name_v.toString();//QString::fromLocal8Bit(name.data());

        m_reports_url = m_program_settings.value("/reports_url","http://localhost").toString();
        m_save_log_xml = m_program_settings.value("/save_xml","false").toBool();

        cm_run_at_exit = m_program_settings.value("/run_at_exit", "").toString();

        _time t;
        QChar fill = '0';
        t.set_now();
        m_file_signature = QString("rmo_log_%1%2%3%4").arg(t.month(), 2, 10, fill).arg(t.day(), 2, 10, fill).arg(t.hour(), 2, 10, fill).arg(t.minute(), 2, 10, fill);
        QDir dir;
        if(m_save_log_xml && !dir.exists(m_path_out_log))
            dir.mkpath(m_path_out_log);
    m_program_settings.endGroup();

    m_user_settings.beginGroup("/auth");
        m_workplace = m_user_settings.value("/last_logon_workplace", "").toString();
        m_name = m_user_settings.value("/last_logon_name", "").toString();
    m_user_settings.endGroup();

    m_user_settings.beginGroup("/ui");
        m_ticket_notify_mainwnd = m_user_settings.value("/ticket_notify_mainwnd", "").toBool();
        m_ticket_notify_tray = m_user_settings.value("/ticket_notify_tray", "").toBool();
        m_ticket_notify_sound = m_user_settings.value("/ticket_notify_sound", "").toBool();
        m_call_ticket_single_button = m_user_settings.value("/call_ticket_one_button", "").toBool();
    m_user_settings.endGroup();

    m_user_settings.beginGroup("/indicators");
        m_ticket_in_proc = m_user_settings.value("/ticket_in_proc", true).toBool();
        m_ticket_in_proc_service_time = m_user_settings.value("/ticket_in_proc_service_time", true).toBool();
        m_served_ticket_total2day = m_user_settings.value("/served_ticket_total2day", true).toBool();
//        m_redirected_ticket_total2day = m_user_settings.value("/redirected_ticket_total2day", true).toBool();
//        m_absent_ticket_total2day = m_user_settings.value("/absent_ticket_total2day", true).toBool();
        m_waiting_time_limit_ms = m_user_settings.value("/waiting_time_limit", true).toBool();
        m_service_time_limit_ms = m_user_settings.value("/service_time_limit", true).toBool();
    m_user_settings.endGroup();
}

QString _settings::server() const                   { return m_server; }
void _settings::set_server(QString v)               { m_server = v; }

int _settings::port(void) const                     { return m_port; }
void _settings::set_port(int v)                     { m_port = v; }

QString _settings::mark_console_type() const                      { return m_mark_console_type; }
void _settings::set_mark_console_type(QString v)                  { m_mark_console_type = v; }
QString _settings::mark_console_port() const                      { return m_mark_console_port; }
void _settings::set_mark_console_port(QString v)                  { m_mark_console_port = v; }
QString _settings::mark_console_number() const                      { return m_mark_console_number; }
void _settings::set_mark_console_number(QString v)                  { m_mark_console_number = v; }

int _settings::log_level(void) const                { return m_log_level; }
void _settings::set_log_level(int v)                { m_log_level = v; }
void _settings::set_log(int log_level)              { m_log_level_settings = log_level;}
bool _settings::is_save_log_xml() const             { return m_save_log_xml; }

QString _settings::office_id(void) const            { return m_office_id; }
void _settings::set_office_id(QString v)            { m_office_id = v; }

QString _settings::office_name_Utf8(void) const     { return m_office_name.toLocal8Bit(); }
QString _settings::office_name(void) const          { return m_office_name; }
void _settings::set_office_name(QString v)          { m_office_name = v; }

QString _settings::file_signature() const           { return m_file_signature; }

QString _settings::user_workplace() const           { return m_workplace; }
void _settings::set_user_workplace(QString v)       { m_workplace = v; }

QString _settings::user_name() const                { return m_name; }
void _settings::set_user_name(QString v)            { m_name = v; }

bool _settings::ticket_notify_mainwnd() const        { return m_ticket_notify_mainwnd; }
void _settings::set_ticket_notify_mainwnd(bool v)    { m_ticket_notify_mainwnd = v; }

bool _settings::ticket_notify_tray() const          { return m_ticket_notify_tray; }
void _settings::set_ticket_notify_tray(bool v)      { m_ticket_notify_tray = v; }

bool _settings::ticket_notify_sound() const         { return m_ticket_notify_sound; }
void _settings::set_ticket_notify_sound(bool v)     { m_ticket_notify_sound = v; }

bool _settings::call_ticket_single_button() const         { return m_call_ticket_single_button; }
void _settings::set_call_ticket_single_button(bool v)     { m_call_ticket_single_button = v; }

QString _settings::video_cams() const                   { return cm_video_cams; }
void _settings::set_video_cams(QString v)               { cm_video_cams = v; }

QString _settings::run_at_exit() const                   { return cm_run_at_exit; }
void _settings::set_run_at_exit(QString v)               { cm_run_at_exit = v; }


QString _settings::user_path() const
{
    QString path = QFileInfo(m_user_settings.fileName()).absolutePath();
    return path;
}

QString _settings::programm_path() const
{
    QString path = QFileInfo(m_program_settings.fileName()).absolutePath();
    return path;
}

// indicators --------------------------------------------------------------------------

void _settings::set_ticket_in_proc(bool param)              { m_ticket_in_proc = param; }
bool _settings::ticket_in_proc(void) const                  { return m_ticket_in_proc; }

void _settings::set_ticket_in_proc_service_time(bool param) { m_ticket_in_proc_service_time = param; }
bool _settings::ticket_in_proc_service_time(void) const     { return m_ticket_in_proc_service_time; }

void _settings::set_served_ticket_total2day(bool param)     { m_served_ticket_total2day = param; }
bool _settings::served_ticket_total2day(void) const         { return m_served_ticket_total2day; }

//void _settings::set_redirected_ticket_total2day(bool param) { m_redirected_ticket_total2day = param; }
//bool _settings::redirected_ticket_total2day(void) const     { return m_redirected_ticket_total2day; }

//void _settings::set_absent_ticket_total2day(bool param)     { m_absent_ticket_total2day = param; }
//bool _settings::absent_ticket_total2day(void) const         { return m_absent_ticket_total2day; }

void _settings::set_waiting_time_limit_ms(bool param)    { m_waiting_time_limit_ms = param; }
bool _settings::waiting_time_limit_ms(void) const        { return m_waiting_time_limit_ms; }

void _settings::set_service_time_limit_ms(bool param)    { m_service_time_limit_ms = param; }
bool _settings::service_time_limit_ms(void) const        { return m_service_time_limit_ms; }

///////////////////////////////////////////////////////////
_settings *settings;
*/

_config_user config_user; //Инициализируем extern объект

_config_user::_config_user()
{
    //Проверим наличие конф.файла
    if(QFile(configPath()).exists()==true)
        loadConfig(); //Загружаем данные из файла
    else
    {
        //Создаём конф.файл поумолчанию
        QDir().mkpath(QDir::homePath()+"/eq_operator"); //Создаём каталог
        createDefConfigFile(); //Создать новый конфиг файл.

    }
}

_config_user::~_config_user()
{
    saveConfig(); //Сохраняем конфиги в файл
}

bool _config_user::loadConfig()
{
    QFile fileR(configPath());
    if(fileR.open(QIODevice::ReadOnly)==false) return false;
    QTextStream inp(&fileR); inp.setCodec("UTF-8");
    while(!inp.atEnd())
    {
        QString t_row = inp.readLine();
        if(t_row.left(1)=="#" || t_row.isEmpty()) continue; //Пропускаем коментарии и пустые строки
        QStringList lr_row=t_row.split("=");
        if(lr_row.count()==1) {if(confArgs.contains(t_row)==false) confArgs.append(t_row); continue;} //Не парные значения
        //Если это строка (ключ-значение)
        QString key = lr_row.first(); lr_row.removeFirst();
        confProp[key]=lr_row.join("=");
    }
    fileR.close(); return true;
}

bool _config_user::saveConfig()
{
    //Код может и усложнённый, но теперь конф.файл ещё надо потрудится сломать

    QFile fileRW(configPath());
    if(fileRW.open(QIODevice::ReadOnly)==false);
    QTextStream rw(&fileRW); rw.setCodec("UTF-8");
    QStringList rows;   //Список строк
    QStringList keys=confProp.keys(); //Список ещё не упомянутых ключей
    QStringList args=confArgs;        //Список ещё не упомянутых аргументов

    //Считываем все строки конф.файла и слегка подтусуем значения
    while(!rw.atEnd())
    {
        QString t_row = rw.readLine();
        if(t_row.left(1)=="#" || t_row.isEmpty()) {rows.append(t_row); continue;} //Пропускаем коментарии и пустые строки
        QStringList lr_row=t_row.split("=");
        if(lr_row.count()==1) //Если строка не содержит = , то проверяется что она всё ещё есть в конфигах
        {
            if(confArgs.contains(t_row)) {rows.append(t_row); args.removeOne(t_row);} continue;
        }
        //Перезапись имеющихся параметров
        if(confProp.contains(lr_row.first()))
        {
            t_row = QString("%1=%2").arg(lr_row.first()).arg(confProp.value(lr_row.first()));
            keys.removeOne(lr_row.first());
        }
        rows.append(t_row);
    }
    //Добавление новых параметров (которых в файле ещё небыло)
    for(int i=0;i<keys.count();i++) {rows.append(QString("%1=%2").arg(keys.value(i)).arg(confProp.value(keys.value(i))));}
    rows.append(args);
    //перезапись файла
    fileRW.close(); fileRW.open(QIODevice::WriteOnly);
    for(int i=0;i<rows.count();i++) rw << rows.value(i) << "\r\n";
    fileRW.close(); return true;
}

QString _config_user::configPath()
{
    return QDir::homePath()+"/eq_operator/u_config.conf";
}

QString _config_user::getConfPropertyStr(QString name, QString defVal)
{
    if(confProp.contains(name)==false) {confProp[name]=defVal;}
    return confProp.value(name);
}

int _config_user::getConfPropertyInt(QString name, int defVal)
{
    if(confProp.contains(name)==false || confProp.value(name).isEmpty())
        {confProp[name]=QString("%1").arg(defVal); return defVal;}
    return confProp.value(name).toInt();
}

bool _config_user::getConfArg(QString name)
{
    return confArgs.contains(name);
}

QStringList _config_user::getPropertiesNames()
{
    return confProp.keys();
}

void _config_user::setConfPropertyStr(QString name, QString value)
{
    confProp[name]=value;
}

void _config_user::setConfPropertyInt(QString name, int value)
{
    setConfPropertyStr(name,QString("%1").arg(value));
}

void _config_user::setConfArg(QString name, bool value)
{
    if(value==false) confArgs.removeOne(name);
    else
    {
        if(confArgs.contains(name)==false) confArgs.append(name);
    }
}

bool _config_user::createDefConfigFile()
{
    QFile fileW(configPath());
    if(fileW.open(QIODevice::WriteOnly)==false) return false;
    QTextStream out(&fileW); out.setCodec("UTF-8");
    out << QString("# Конфигурационный файл Am-клиента.\r\n");
    out << QString("# Короч электронные очереди.\r\n");
    out << QString("\r\n");
    //Основные параметры
    out << QString("last_login=\r\n");
    out << QString("last_workstation=\r\n");
    out << QString("sound_notif=1\r\n");
    out << QString("alert_in_tray=1\r\n");
    out << QString("open_window_if_notif=1\r\n");
    out << QString("show_button_call_next=0\r\n");
    out << QString("audio_recorder=\r\n");
    out << QString("audio_quality=1\r\n");
    out << QString("printer=VKP80\r\n");


    fileW.close();
    //Загрузить данные из созданного файла
    return loadConfig();
}
