#ifndef __RDG_SETTINGS_HPP__
#define __RDG_SETTINGS_HPP__

#include <QObject>
#include <QString>
#include <QTime>
#include <QSettings>
#include <QApplication>
#include <QMap>


struct _config
{
    struct _server
    {
        std::string name;
        int port;
    } server;

    struct _mark_console
    {
        std::string type;
        std::string port;
        int number;
    } mark_console;

    struct _office
    {
        QString id;
        QString name;
    } office;

    struct _user
    {
        QString name;
        QString wp_name;
    } user;

    struct _run
    {
        std::string run_path;
        std::string log_levels;
        std::string pre_log_file;
        std::string log_file;
        std::string config_file;
        int record_audio;
        int create_ticket_enable;
        bool message_dif_to_close; //Выводить-ли предупреждение если привышено время работы с талоном
    } run;

    /*
     * Эту функцию теперь исполняет _config_user
    struct _notify
    {
        bool open_main_wnd;
        bool tray;
        bool sound;
    } notify;
    */

    struct _ui
    {
        bool single_call_button;

        struct _ind
        {
            bool proc_tikcet_n;
            bool proc_tikcet_serv_time;
            bool serv2day;
        } ind;
    } ui;

    struct _extrenal
    {
        std::string video_cams;
        std::string report_url;
        std::string run_at_exit;
    } external;
};

class _gl_set
{
    public:
        static _config config;

        static void load_config_file(const std::string &fname);
        static void update_config_file(const std::string &fname);
};





//Настройки пользователя (конф.файл находитсмя в папке "/User/eq_operator/")
//Добавлены при объединении omsk и stan клиентов (20 февраля 2018г.)
class _config_user
{
protected:
    QMap<QString,QString> confProp; //Пары ключ-значение
    QStringList confArgs;           //Строки без пар


public:
    _config_user();
    ~_config_user();
    bool loadConfig(); //Загружает данные из конф.файла
    bool saveConfig(); //Сохраняет данных в конф.файл
    static QString configPath(); //Путь к файлу с пользовательскими конфигами
    //Получение значений
    QString getConfPropertyStr(QString name, QString defVal = "");
    int getConfPropertyInt(QString name, int defVal = 0);
    bool getConfArg(QString name); //Есть-ли аргумент с таким именем
    QStringList getPropertiesNames(); //Список всех свойств из QMap
    //Установка значений
    void setConfPropertyStr(QString name, QString value);
    void setConfPropertyInt(QString name, int value);
    void setConfArg(QString name, bool value);

protected:
    bool createDefConfigFile(); //Создать конфигурационный файл по умолчанию.
};

extern _config_user config_user;

/*
//#include <debug.hpp>
/*! Класс настроек
  *
  * Класс содержит интерфейсы для чтения системных настроек и
  * чтения и записи пользовательских настроек из ini-файлов.<br>
  *
  * Кодировка файлов: utf8.<br>
  * Загрузка данных из файлов выполняется методом  read_settings(), запись - write_settings().<br>
  */
/*
class _settings : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString server READ server WRITE set_server NOTIFY server_changed) // lserver ip/dns name
    Q_PROPERTY(int port READ port WRITE set_port NOTIFY port_changed)
    Q_PROPERTY(int log_level READ log_level WRITE set_log_level NOTIFY log_level_changed)

    Q_PROPERTY(QString mark_console_type READ mark_console_type WRITE set_mark_console_type NOTIFY mark_console_type_changed)
    Q_PROPERTY(QString mark_console_port READ mark_console_port WRITE set_mark_console_port NOTIFY mark_console_port_changed)
    Q_PROPERTY(QString mark_console_number READ mark_console_number WRITE set_mark_console_number NOTIFY mark_console_number_changed)

    Q_PROPERTY(QString office_name READ office_name_Utf8 WRITE set_office_name NOTIFY office_name_changed)
    Q_PROPERTY(QString office_id READ office_id WRITE set_office_id NOTIFY office_id_changed)

    Q_PROPERTY(QString video_cams READ video_cams WRITE set_video_cams NOTIFY video_cams_changed)

    Q_PROPERTY(QString run_at_exit READ run_at_exit WRITE set_run_at_exit NOTIFY run_at_exit_changed)

    Q_PROPERTY(QString user_workplace READ user_workplace WRITE set_user_workplace NOTIFY user_workplace_changed)
    Q_PROPERTY(QString user_name READ user_name WRITE set_user_name NOTIFY user_name_changed)

    Q_PROPERTY(bool ticket_notify_mainwnd READ ticket_notify_mainwnd WRITE set_ticket_notify_mainwnd NOTIFY ticket_notify_mainwnd_changed) // Notification on new ticket in former empty queue - dialog
    Q_PROPERTY(bool ticket_notify_tray READ ticket_notify_tray WRITE set_ticket_notify_tray NOTIFY ticket_notify_tray_changed) // Notification on new ticketin former empty queue - tray icon
    Q_PROPERTY(bool ticket_notify_sound READ ticket_notify_sound WRITE set_ticket_notify_sound NOTIFY ticket_notify_sound_changed) // Notification on new ticketin former empty queue - sound
    Q_PROPERTY(bool call_ticket_single_button READ call_ticket_single_button WRITE set_call_ticket_single_button NOTIFY call_ticket_single_button_changed) // Use one button only for call next ticket instead of 2 buttons fo each queue

protected:
    ~_settings(){}
    static QString get_fname(void);// throw (string&);

public:
    explicit _settings(QObject *parent = 0);
    void release() { delete this; }
    void write_settings();
    void read_settings();

    //    bool open_win();
    //    void set_open_win(bool param);
    //    bool sound();
    //    void set_sound(bool param);
    //    bool icon();
    //    void set_icon(bool param);

    QString server(void) const;
    int port(void) const;

    QString mark_console_type(void) const;
    QString mark_console_port(void) const;
    QString mark_console_number(void) const;

    int log_level(void) const;

    QString office_id(void) const;
    QString office_name(void) const;

    bool is_save_log_xml(void) const;
    QString file_signature(void) const;
    QString user_workplace(void) const;
    QString user_name(void) const;
    void set_log(int log_level);

    QString user_path(void) const;
    QString programm_path(void) const;

    bool ticket_notify_mainwnd(void) const;
    bool ticket_notify_tray(void) const;
    bool ticket_notify_sound(void) const;
    bool call_ticket_single_button(void) const;

    bool ticket_in_proc(void) const;
    bool ticket_in_proc_service_time(void) const;
    bool served_ticket_total2day(void) const;
//    bool redirected_ticket_total2day(void) const;
//    bool absent_ticket_total2day(void) const;
    bool waiting_time_limit_ms(void) const;
    bool service_time_limit_ms(void) const;

    QString video_cams(void) const;

    QString run_at_exit(void) const;

signals:
    void server_changed(QString param);
    void port_changed(int param);
    void log_level_changed (int param);
    void mark_console_type_changed(QString param);
    void mark_console_port_changed(QString param);
    void mark_console_number_changed(QString param);
    void office_name_changed(QString param);
    void office_id_changed(QString param);
    void user_name_changed(QString arg);
    void user_workplace_changed(QString arg);
    void ticket_notify_mainwnd_changed(bool arg);
    void ticket_notify_tray_changed(bool arg);
    void ticket_notify_sound_changed(bool arg);
    void call_ticket_single_button_changed(bool arg);
    void video_cams_changed(QString arg);
    void run_at_exit_changed(QString arg);

public slots:
    void set_server(QString param);
    void set_port(int param);
    void set_log_level(int param);
    void set_mark_console_type(QString param);
    void set_mark_console_port(QString param);
    void set_mark_console_number(QString param);
    void set_office_name(QString param);
    void set_office_id(QString param);
    void set_user_workplace(QString arg);
    void set_user_name(QString param);
    void set_ticket_notify_mainwnd(bool arg);
    void set_ticket_notify_tray(bool arg);
    void set_ticket_notify_sound(bool arg);
    void set_call_ticket_single_button(bool arg);
    void set_ticket_in_proc(bool param);
    void set_ticket_in_proc_service_time(bool param);
    void set_served_ticket_total2day(bool param);
//    void set_redirected_ticket_total2day(bool param);
//    void set_absent_ticket_total2day(bool param);
    void set_waiting_time_limit_ms(bool param);
    void set_service_time_limit_ms(bool param);

    void set_video_cams(QString arg);
    void set_run_at_exit(QString arg);

public:
    QString office_name_Utf8() const;
    QString reports_url() const;

private:
    QSettings m_user_settings;
    QSettings m_program_settings;

    //----------- user params

    bool m_open_win;
    bool m_sound;
    bool m_icon;
    QString m_workplace;
    QString m_name;

    //----------- side indicators

    bool m_ticket_in_proc;
    bool m_ticket_in_proc_service_time;
    bool m_served_ticket_total2day;
//    bool m_redirected_ticket_total2day;
//    bool m_absent_ticket_total2day;
    bool m_waiting_time_limit_ms;
    bool m_service_time_limit_ms;

    //=========== system params

    //----------- user editable params

    QString m_server;
    QString m_new_server;
    int m_port;
    int m_new_port;
    int m_log_level;
    int m_new_log_level;

    QString m_mark_console_type;
    QString m_new_mark_console_type;
    QString m_mark_console_port;
    QString m_new_mark_console_port;
    QString m_mark_console_number;
    QString m_new_mark_console_number;

    //----------- params non-editable by user

//    QTime m_boundary_waiting_time;
//    QString m_db_name;
    QString m_office_id;
    QString m_office_name;

    int m_log_level_settings;
//    int m_new_log_level_settings;

    QString m_reports_url;
    bool m_save_log_xml;
    QString m_file_signature;
    QString m_path_out_log;
    bool m_ticket_notify_mainwnd;
    bool m_ticket_notify_tray;
    bool m_ticket_notify_sound;
    bool m_call_ticket_single_button;

    QString cm_video_cams;

    QString cm_run_at_exit;
};

/////////////////////////////////////////////////////////////

_settings* create_rdg_settings(void);

extern _settings *settings;
*/

#endif
