#ifndef __COMBO_DOUBLE_LINE_DELEGATE_HPP__
#define __COMBO_DOUBLE_LINE_DELEGATE_HPP__

#include <QItemDelegate>

class _combo_2line_delegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit _combo_2line_delegate(QObject *parent = 0) :
        QItemDelegate(parent) {}

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif
