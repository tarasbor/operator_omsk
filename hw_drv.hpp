#ifndef __EQ_HW_DRV__
#define __EQ_HW_DRV__

#include <rs.hpp>
#include <rs_iface.hpp>

using namespace std;

// ------------------------------------------------------------------------
class _led_display_drv
{
	protected:
		virtual ~_led_display_drv() {};
		
	public:
		_led_display_drv() {};
		
		virtual bool display(const string &text) = 0;
		//virtual bool play_sound(const string &snd) = 0;
		
		virtual void release() = 0;
};

_led_display_drv *create_svt_dig_led_display(const string &port_name,unsigned char disp_number); // SVETOVOD DEGITAL LED DISPLAY


// ------------------------------------------------------------------------
class _console_drv
{
    protected:
		virtual ~_console_drv() {};
		
    public:
        
        _console_drv() {};
        
        
        virtual bool display(const string &text,unsigned int cursor_pos) = 0;
        virtual bool kbd_buffer(string &buffer) = 0;
		
		virtual void release() = 0;
};

_console_drv *create_svt_console(const string &port_name,unsigned char panel_number);  // SVETOVOD CONTROL PANEL

#ifdef SVT_EMU
_console_drv *create_svt_emu_console(const string &port_name,unsigned char panel_number);  // SVETOVOD CONTROL PANEL
#endif

#endif
