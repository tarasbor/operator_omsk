#ifndef SETTINGS_DLG_HPP
#define SETTINGS_DLG_HPP

#include <QDialog>

namespace Ui {
class _settings_dlg;
}

class _settings_dlg : public QDialog
{
    Q_OBJECT
    
public:
    explicit _settings_dlg(QWidget *parent = 0);
    ~_settings_dlg();

    bool update_qb; //Нужно-ли вызывать метод update_queue_buttons()

    void setAudioDevices(QStringList sl);
    
private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_checkBox_hide_queues_clicked();

private:
    Ui::_settings_dlg *ui;
};

#endif // SETTINGS_DLG_HPP
