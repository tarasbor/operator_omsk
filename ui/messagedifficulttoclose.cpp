#include "messagedifficulttoclose.h"
#include "ui_messagedifficulttoclose.h"
#include "../rdg_log.hpp"

MessageDifficultToClose::MessageDifficultToClose(QString text) :
    QDialog(NULL, Qt::Dialog | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::X11BypassWindowManagerHint),
    ui(new Ui::MessageDifficultToClose),
    autoDel(false),
    mousePress(false)
{
    ui->setupUi(this); this->setModal(true); hide();
    this->setWindowTitle("Alfa-M Уведомление");

    ui->teMessage->setReadOnly(true); ui->teMessage->setText(text);
    connect(ui->cbAccept,SIGNAL(clicked(bool)),ui->pbClose,SLOT(setEnabled(bool)));
    connect(ui->pbClose,SIGNAL(clicked(bool)),this,SLOT(close()));
}

MessageDifficultToClose::~MessageDifficultToClose()
{
    delete ui;
}

void MessageDifficultToClose::setText(QString text)
{
    ui->teMessage->setText(text);
    _log log;
    log << LOG_UI(0) << "open window - process ticket time more than maximum." << endl;
}

void MessageDifficultToClose::mousePressEvent(QMouseEvent *mouse)
{
    if(mouse->button() == Qt::LeftButton && mouse->y() < 30) {mousePress = true; this->setMouseTracking(true);}
}

void MessageDifficultToClose::mouseReleaseEvent(QMouseEvent *mouse)
{
    if(mouse->button() == Qt::LeftButton) mousePress = false;
}

void MessageDifficultToClose::mouseMoveEvent(QMouseEvent *mouse)
{
    if(mousePress==false) return;
    setGeometry(mouse->globalX()-width()/2,mouse->globalY()-10,width(),height());
}

void MessageDifficultToClose::closeEvent(QCloseEvent *ev)
{
    //Запрет на закрытие крестиком до ознакомления
    if(ui->cbAccept->isChecked()==false) {ev->ignore(); return;}

    QDialog::closeEvent(ev); ui->cbAccept->setChecked(false); ui->pbClose->setEnabled(false);

    _log log;
    log << LOG_UI(0) << "client have accepted & closed window - process ticket time more than maximum." << endl;

    if(autoDel) this->deleteLater();
}
