//#include <main_win.hpp>
#include <settings.hpp>
#include <tray.hpp>

_rdg_tray::_rdg_tray(QMainWindow* mainwnd, QObject* parent) :
    QObject(parent), m_win(mainwnd)//, _m_settings_window(NULL)
{
    trayIcon = new QSystemTrayIcon(mainwnd->windowIcon(), mainwnd);
    trayIcon->setToolTip(mainwnd->windowIconText());

    connect(trayIcon, SIGNAL(messageClicked()), this, SLOT(message_clicked()));
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(icon_activated(QSystemTrayIcon::ActivationReason)));

    trayIcon->show();
}

_rdg_tray::~_rdg_tray()
{
    trayIcon->hide();
    delete trayIcon;
}

void _rdg_tray::set_icon_and_tooltip(const QIcon& icon, const QString& tooltip)
{
    trayIcon->setIcon(icon);
    trayIcon->setToolTip(tooltip);
}

void _rdg_tray::icon_activated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason)
    {
    case QSystemTrayIcon::Trigger:
        {
        }
        break;
    case QSystemTrayIcon::DoubleClick:
        {
            m_win->showNormal();
            m_win->activateWindow();
        }
        break;
    case QSystemTrayIcon::MiddleClick:
        //        show_message();
        break;
    default:
        ;
    }
}

void _rdg_tray::show_message(QString title, QString message)
{
    trayIcon->showMessage(title, message, QSystemTrayIcon::Warning, 300000);
}

void _rdg_tray::message_clicked()
{
    if(m_win->isMinimized())
        m_win->setWindowState(m_win->windowState() & ~Qt::WindowMinimized | Qt::WindowActive);
}

void _rdg_tray::set_app_name(QString name_app)
{
    trayIcon->setToolTip(name_app);
}
