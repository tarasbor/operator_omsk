#include <error.hpp>
#include <rdg_log.hpp>
#include <QAudioRecorder>
#include "settings.hpp"
#include "version.h"

#include "settings_dlg.hpp"
#include "ui_settings_dlg.h"
#include <QMessageBox>
#include <QString>

//#include <debug.hpp>
#ifdef DEBUG
    #include <QDebug>
#endif

_settings_dlg::_settings_dlg(QWidget *parent) :
    QDialog(parent),
    update_qb(false),
    ui(new Ui::_settings_dlg)
{
    ui->setupUi(this);

    ui->edt_server_address->setText(QString(_gl_set().config.server.name.c_str()));
    ui->edt_server_port->setText(QString::number(_gl_set().config.server.port));
    //ui->spb_log_level->setValue(settings->log_level());
    ui->chk_new_ticket_dialog->setChecked(config_user.getConfPropertyInt("open_window_if_notif",1));
    ui->chk_new_ticket_tray->setChecked(config_user.getConfPropertyInt("alert_in_tray",1));
    ui->chk_new_ticket_sound->setChecked(config_user.getConfPropertyInt("sound_notif",1));
    ui->checkBox_hide_queues->setChecked(config_user.getConfPropertyInt("hide_empty_queues",0));
    ui->checkBoxShowButtonNext->setChecked(config_user.getConfPropertyInt("show_button_call_next",0));
    if(_gl_set().config.run.record_audio == 1){
            QAudioRecorder *ar = new QAudioRecorder(this);
            setAudioDevices(ar->audioInputs());
            delete ar;
            ui->comboBoxAudioDevice->setVisible(true);
            ui->label->setVisible(true);
    }
    string ver="Версия: ";
    ver+=VER_PRODUCTVERSION_STR;
    ui->lbl_version->setText(ver.c_str());
    //Пля одной кнопки эта настройка не нужна
    ui->checkBox_hide_queues->setEnabled(_gl_set().config.ui.single_call_button==false);
}

_settings_dlg::~_settings_dlg()
{
    delete ui;
}

void _settings_dlg::setAudioDevices(QStringList sl)
{
    ui->comboBoxAudioDevice->clear();
    foreach (const QString &device, sl) {
        ui->comboBoxAudioDevice->addItem(device, QVariant(device));
    }
}

void _settings_dlg::on_buttonBox_accepted()
{
    _log log;

    try
    {
        //TODO: check input
        _gl_set().config.server.name = ui->edt_server_address->text().toStdString();
        _gl_set().config.server.port = ui->edt_server_port->text().toInt();
        //settings->set_log_level(ui->spb_log_level->value());
        config_user.setConfPropertyInt("open_window_if_notif",ui->chk_new_ticket_dialog->isChecked());
        config_user.setConfPropertyInt("alert_in_tray",ui->chk_new_ticket_tray->isChecked());
        config_user.setConfPropertyInt("sound_notif",ui->chk_new_ticket_sound->isChecked());
        config_user.setConfPropertyInt("hide_empty_queues",ui->checkBox_hide_queues->isChecked());
        config_user.setConfPropertyInt("show_button_call_next",ui->checkBoxShowButtonNext->isChecked());
        config_user.setConfPropertyStr("audio_recorder",ui->comboBoxAudioDevice->currentText());
        //settings->write_settings();

        log << LOG_COMMON(0) << "settings changed by user:" << endl;
        log << "server=" << ui->edt_server_address->text().toStdString() << endl;
        log << "port=" << ui->edt_server_port->text().toStdString() << endl;
        //log << "log level=" << ui->spb_log_level->value() << endl;
    }
    catch(_rdg_exception& e)
    {
        log << LOG_COMMON(0) << "Settings write was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
        QMessageBox msgbox(QMessageBox::Critical, QCoreApplication::applicationName(), QString::fromLocal8Bit(e.description().c_str()), QMessageBox::Ok);
        msgbox.setModal(true);
        msgbox.exec();
    }
    catch(...)
    {
        log << LOG_COMMON(0) << "Settings write was failed by unknown reason." << endl;
        QMessageBox msgbox(QMessageBox::Critical, QCoreApplication::applicationName(), QString("Неопознанная ошибка."), QMessageBox::Ok);
        msgbox.setModal(true);
        msgbox.exec();
    }

    config_user.saveConfig();
    close();
}

void _settings_dlg::on_buttonBox_rejected()
{
    close();
}

void _settings_dlg::on_checkBox_hide_queues_clicked()
{
    update_qb = true;
}
