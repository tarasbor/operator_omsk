#ifndef MESSAGEDIFFICULTTOCLOSE_H
#define MESSAGEDIFFICULTTOCLOSE_H

#include <QDialog>
#include <QCloseEvent>

namespace Ui {
class MessageDifficultToClose;
}

class MessageDifficultToClose : public QDialog
{
    Q_OBJECT

public:
    bool autoDel; //При закрытии сообщения будет вызвын deleteLater()

private:
    Ui::MessageDifficultToClose *ui;
    bool mousePress;

public:
    explicit MessageDifficultToClose(QString text = "");
    ~MessageDifficultToClose();

    void setText(QString text);

    void mousePressEvent(QMouseEvent *mouse);
    void mouseReleaseEvent(QMouseEvent *mouse);
    void mouseMoveEvent(QMouseEvent *mouse);

protected:
    virtual void closeEvent(QCloseEvent *ev);


};

#endif // MESSAGEDIFFICULTTOCLOSE_H
