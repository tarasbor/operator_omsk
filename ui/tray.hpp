#ifndef TRAY_HPP
#define TRAY_HPP

#include <QSystemTrayIcon>
#include <QMainWindow>

class _rdg_tray : public QObject
  {
    Q_OBJECT
  public:
    explicit _rdg_tray(QMainWindow* mainwnd, QObject* parent = 0);
    ~_rdg_tray();

  signals:

  public slots:
    void set_app_name(QString name_app);

  private slots:
    void set_icon_and_tooltip(const QIcon& icon, const QString& tooltip);
    void icon_activated(QSystemTrayIcon::ActivationReason reason);
    void show_message(QString title, QString message);
    void message_clicked();

  public:
    QSystemTrayIcon *trayIcon;

  private:
    QMainWindow* m_win;

  };

#endif
