#ifndef FORM_ADD_INDICATORS_HPP
#define FORM_ADD_INDICATORS_HPP

#include <QDialog>
#include "widget_indicator.hpp"

namespace Ui {
class _form_add_indicators;
}

class _form_add_indicators : public QDialog
{
    Q_OBJECT

    signals:
        void show_side_indicator(QString name_ind);
    public:
        explicit _form_add_indicators(const QList<_widget_indicator*>& wis, QWidget* parent = 0);
        ~_form_add_indicators();

    protected:
        bool eventFilter(QObject* watched, QEvent* event);
    private slots:
        void on_btn_add_indicator_clicked();

    private:
        Ui::_form_add_indicators* ui;
        QList<QString> hidden_indicators;
        QList<QString> hidden_indicators_name;
        int            timer_id;

        void timerEvent(QTimerEvent* event);
};

#endif // FORM_ADD_INDICATORS_HPP
