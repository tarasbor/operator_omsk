#include "eqaudiorecorder.h"
#include "rdg_log.hpp"
#include <json/json.hpp>
#include <iostream>
#include "eq_net_client.hpp"
#include "main_win.hpp"
using namespace std;
using json = nlohmann::json;

EqAudioRecorder::EqAudioRecorder()
{
    aRecorder = new QAudioRecorder();
    loadDefaultSettings();
    started = false;
    QStringList sl = aRecorder->audioInputs();
    foreach(QString s, sl)
    {
        _log log;
        log << LOG_COMMON(10) << "Audio device '" << s.toStdString() << "'" << endl;
    }

}

EqAudioRecorder::~EqAudioRecorder()
{
    if(aRecorder->state())
    aRecorder->stop();
}

void EqAudioRecorder::loadDefaultSettings()
{
    setPath(QDir::homePath()+"/eq_operator/"); setFileName("default.wav");
    loadCustomSettings("audio/pcm",48000,16,QMultimedia::VeryLowQuality,QMultimedia::ConstantQualityEncoding,
                       path+filename,"wav");
}

void EqAudioRecorder::loadCustomSettings(QString codec, int samplerate, int bitrate, QMultimedia::EncodingQuality quality,
                                         QMultimedia::EncodingMode mode, QString outfile, QString containerformat)
{
    // Set audio settings
    audioRecorderSettings.setCodec(codec);
    audioRecorderSettings.setSampleRate(samplerate);
    audioRecorderSettings.setBitRate(bitrate);
    audioRecorderSettings.setQuality(quality);
    audioRecorderSettings.setEncodingMode(mode);


    // Set Audio Input
    aRecorder->setAudioInput(aRecorder->defaultAudioInput());

    // Sets Output location where to store the file
    aRecorder->setOutputLocation(QUrl::fromLocalFile(outfile));
    aRecorder->setContainerFormat(containerformat);
}

void EqAudioRecorder::startRecording()
{
    if(aRecorder->state() == aRecorder->StoppedState)
    {
        _log log;
        log << LOG_COMMON(10) << "Начата запись в звуковой файл" << endl;
        aRecorder->setEncodingSettings(audioRecorderSettings);
        aRecorder->record();
        started = true;
    }
}

void EqAudioRecorder::stopRecording()
{
    if(aRecorder->state() == aRecorder->RecordingState)
    {
        _log log;
        log << LOG_COMMON(10) << "Остановлена запись в звуковой файл" << endl;
        aRecorder->stop();
        started = false;
    }
}

bool EqAudioRecorder::getState()
{
    return started;
}

void EqAudioRecorder::setAudioDevice(QString ad)
{
    _log log;
    log << LOG_COMMON(10) << "Настроено записывающее устройство: " << ad.toStdString() << endl;
    aRecorder->setAudioInput(ad);
}

void EqAudioRecorder::setPath(QString path_new)
{
    path = path_new;
    aRecorder->setOutputLocation(QUrl::fromLocalFile(path + filename));
}

void EqAudioRecorder::setFileName(QString name_new)
{
    filename = name_new;
    aRecorder->setOutputLocation(QUrl::fromLocalFile(path + filename));
}

void EqAudioRecorder::setQuality(int q)
{
    this->quality = q;
    if(q == 1){audioRecorderSettings.setQuality(QMultimedia::VeryLowQuality);aRecorder->setEncodingSettings(audioRecorderSettings);}
    else if(q == 2){audioRecorderSettings.setQuality(QMultimedia::LowQuality);aRecorder->setEncodingSettings(audioRecorderSettings);}
    else if(q == 3){audioRecorderSettings.setQuality(QMultimedia::NormalQuality);aRecorder->setEncodingSettings(audioRecorderSettings);}
    else if(q == 4){audioRecorderSettings.setQuality(QMultimedia::HighQuality);aRecorder->setEncodingSettings(audioRecorderSettings);}
    else if(q == 5){audioRecorderSettings.setQuality(QMultimedia::VeryHighQuality);aRecorder->setEncodingSettings(audioRecorderSettings);}
}

QString EqAudioRecorder::getPath()
{
    return path;
}

QString EqAudioRecorder::getFileName()
{
    return filename;
}

void EqAudioRecorder::toFile(QString path, QByteArray arr)
{
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly))
        return;
    file.write(arr);
    file.close();
}

QByteArray EqAudioRecorder::toByteArray(QString path)
{
    QFile file(path);
    QByteArray res;
    if (!file.open(QIODevice::ReadOnly))
        return res;
    res = file.readAll();
    file.close();
}

void EqAudioRecorder::delFile()
{
    _log log;
    log << LOG_COMMON(10) << this->path.toStdString() + this->filename.toStdString() << endl;

    if(!QFile::remove( this->path + this->filename ))
    {
        log << LOG_COMMON(10) << "Файл не найден для удаления" << endl;
    }
}

bool EqAudioRecorder::sendFile()
{
    QString data = this->toByteArray(this->path + this->filename).toBase64().data();
    _log log;
    //log << LOG_COMMON(10) << data.toStdString() << endl;
    try
    {
        json jCommand; jCommand["command"] = "rmoUploadAudioRecord";

        jCommand["recording"] = data.toStdString();
        jCommand["filename"] = this->filename.toStdString();
        std::string res;
        srv->cmd_to_server(jCommand.dump(),res);

        //log << LOG_COMMON(10) << jCommand.dump() << endl;

        log << LOG_COMMON(10) << res << endl;
    }
    catch (json::exception &e)
    {
        log << LOG_COMMON(10) << "AudioRecorder sendFile json error" << endl;
        return false;
    }
}
