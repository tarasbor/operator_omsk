#include "list_selection_dlg.h"
#include "ui_list_selection_dlg.h"
#include <QtGui>

#ifdef DEBUG
    #include <QDebug>
#endif

_list_selection_dlg::_list_selection_dlg(const QStringList& headers, const QList<QList<QStandardItem*>/*QStringList*/>& rows,
                                         int x, int y, int w, int h, QWidget *parent) :
    QDialog(parent, Qt::MSWindowsFixedSizeDialogHint | Qt::FramelessWindowHint),
    ui(new Ui::_list_selection_dlg)
{
    ui->setupUi(this);

    if(parent)
    {
        QRect base_rect = parent->geometry();
        setGeometry(x + base_rect.left(), y + base_rect.top(), w, h);
    }
    else
        setGeometry(x, y, w, h);

    model.setHorizontalHeaderLabels(headers);

    foreach(QList<QStandardItem*> row, rows)
        model.appendRow(row);

    ui->treeView->setModel(&model);
}

_list_selection_dlg::~_list_selection_dlg()
{
    delete ui;
}

void _list_selection_dlg::on_btnClose_clicked()
{
    close();
}

void _list_selection_dlg::on_btnOK_clicked()
{
    sel_item_data = model.data(ui->treeView->currentIndex(), Qt::UserRole + 1);
    close();
}

void _list_selection_dlg::on_btnCancel_clicked()
{
    close();
}

void _list_selection_dlg::on_treeView_doubleClicked(const QModelIndex &index)
{
    sel_item_data = model.data(index, Qt::UserRole + 1);
//    sel_item_data = model.data(ui->treeView->currentIndex(), Qt::UserRole + 1);
    close();
}
