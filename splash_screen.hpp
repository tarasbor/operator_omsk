#ifndef SPLASH_SCREEN_HPP
#define SPLASH_SCREEN_HPP

#include <QDialog>
#include <QTimer>

namespace Ui {
class _splash_screen;
}

class _splash_screen : public QDialog
{
    Q_OBJECT
    
public:
    explicit _splash_screen(QWidget *parent = 0);
    ~_splash_screen();
    
private:
    Ui::_splash_screen *ui;
    QList<QPixmap> pixmaps;
    int currentPixmap;
    QTimer timer;

private slots:
    void changeImage();
};

#endif // SPLASH_SCREEN_HPP
