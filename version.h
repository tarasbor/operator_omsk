﻿#ifndef __VERSION_H__
#define __VERSION_H__

#define VER_FILEVERSION             0,0,5,37
#define VER_FILEVERSION_STR         "0.0.5.37\0"
#define VER_PRODUCTVERSION          VER_FILEVERSION
#define VER_PRODUCTVERSION_STR      VER_FILEVERSION_STR
const char build_date[] = __DATE__;
const char build_time[] = __TIME__;

#define VER_COMPANYNAME_STR         "A-BT Co. Ltd."
#define VER_FILEDESCRIPTION_STR     "Alfa-M RMO"
#define VER_INTERNALNAME_STR        "Alfa-M RMO"
#define VER_LEGALCOPYRIGHT_STR      "Copyright © 2014 ООО АБТ"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "am-client.exe"
#define VER_PRODUCTNAME_STR         "Alfa-M RMO"
#define VER_COMPANYDOMAIN_STR       "http://www.a-bt.ru/"


#endif // VERSION_H

