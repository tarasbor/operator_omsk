#ifndef __OVERLAY_LABEL_HPP__
#define __OVERLAY_LABEL_HPP__

#include <QLabel>

class _overlay_label : public QLabel
{
    Q_OBJECT

public:
    explicit _overlay_label(QWidget *parent = 0);
    void attach_buddy(QObject* buddy)
    {
        m_buddy = buddy;
    }

protected:
    virtual void focusInEvent(QFocusEvent* event);
    virtual void mousePressEvent(QMouseEvent * event);
    virtual void mouseReleaseEvent(QMouseEvent * event);
    
signals:
    void onFocusIn();

//public slots:

private:
    QObject* m_buddy;
};

#endif
