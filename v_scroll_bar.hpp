#ifndef SCROLL_BAR_HPP
#define SCROLL_BAR_HPP

#include <QWidget>
#include <QScrollBar>
#include <QScrollArea>
#include <QTableView>

namespace Ui {
class _v_scroll_bar;
}

class _v_scroll_bar : public QWidget
{
    Q_OBJECT

    public:
        explicit _v_scroll_bar(QWidget* parent = 0);
        ~_v_scroll_bar();

        void initialize();
    private slots:
        void on_btn_top_clicked();
        void on_btn_bottom_clicked();

private:
        int step;
        Ui::_v_scroll_bar* ui;
        QAbstractScrollArea* sa;
        int dy;
};

#endif // SCROLL_BAR_HPP
