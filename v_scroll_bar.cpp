#include "v_scroll_bar.hpp"
#include "ui_v_scroll_bar.h"

//#include <debug.hpp>
#ifdef DEBUG
    #include <QDebug>
#endif

_v_scroll_bar::_v_scroll_bar(QWidget* parent) :
    QWidget(parent),
    sa(qobject_cast<QAbstractScrollArea*>(parent)),
    ui(new Ui::_v_scroll_bar),
    step(10)
{
    ui->setupUi(this);
    Q_ASSERT(sa);
    connect(sa->verticalScrollBar(), SIGNAL(valueChanged(int)), ui->vSlider, SLOT(setValue(int)));
    connect(ui->vSlider, SIGNAL(valueChanged(int)), sa->verticalScrollBar(), SLOT(setValue(int)));
    setVisible(false);
}

_v_scroll_bar::~_v_scroll_bar()
{
    delete ui;
}

void _v_scroll_bar::initialize()
{
    Q_ASSERT(sa);
    sa->verticalScrollBar()->setVisible(false); //hide standard scroll bar
    ui->vSlider->setMaximum(sa->verticalScrollBar()->maximum());
    ui->vSlider->setMinimum(sa->verticalScrollBar()->minimum());
    step = (ui->vSlider->maximum() - ui->vSlider->minimum()) / 10;
    ui->vSlider->setValue(sa->verticalScrollBar()->value());
    setGeometry(sa->geometry().width() - 22, 0, width(), sa->geometry().height());
}

void _v_scroll_bar::on_btn_top_clicked()
{
    sa->verticalScrollBar()->setValue(ui->vSlider->value() - step);
}

void _v_scroll_bar::on_btn_bottom_clicked()
{
    sa->verticalScrollBar()->setValue(ui->vSlider->value() + step);
}
