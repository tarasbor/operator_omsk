#ifndef EQAUDIORECORDER_H
#define EQAUDIORECORDER_H
#include <QTime>
#include <QAudioRecorder>
#include <QAudioProbe>
#include <QAudioRecorder>
#include <QtMultimedia>
#include <QUrl>
#include <QDir>
#include <QByteArray>

class EqAudioRecorder
{
private:
    QAudioRecorder * aRecorder;
    QAudioEncoderSettings audioRecorderSettings;
    bool started = false;
    QString path = "C:\\Users\\abt6\\Documents\\q2\\eq\\ticket_data\\";
    QString filename = "test.wav";
    int quality;

public:
    EqAudioRecorder();
    ~EqAudioRecorder();
    void loadDefaultSettings();
    void loadCustomSettings(QString codec, int samplerate, int bitrate, QMultimedia::EncodingQuality quality,
                            QMultimedia::EncodingMode mode, QString outfile, QString containerformat);
    void startRecording();
    void stopRecording();
    bool getState();
    void setAudioDevice(QString ad);
    void setPath(QString path_new);
    void setFileName(QString name_new);
    void setQuality(int q);
    QString getPath();
    QString getFileName();
    void toFile(QString path, QByteArray arr);
    QByteArray toByteArray(QString path);
    void delFile();
    bool sendFile();
};

#endif // EQAUDIORECORDER_H
