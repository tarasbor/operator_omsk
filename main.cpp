#include <QtGui>
#include <QApplication>
#include <QTextCodec>
#include <QLocale>
#include <QTranslator>
#include <QFileInfo>
#include <QUrl>
#include <QLibraryInfo>
#include <QMessageBox>
#include <QDir>
#include <stdexcept>

#include <error.hpp>
#include <text_help.hpp>
#include <rdg_log.hpp>
#ifdef DEBUG
    #include <QDebug>
#endif

#include <eq_common.hpp>
#include <version.h>
#include <eq_net_client_rmo.hpp>
#include <console_hw.hpp>
#include <system/file_utils.hpp>
#include <system/udefs.hpp>

#include "settings.hpp"
#include "splash_screen.hpp"
#include "auth_dlg.hpp"
#include "main_win.hpp"

using namespace std;

_log_file_writer* g_log_file_writer = NULL;
_eq_net_client_rmo* srv = NULL;
_console_hw* console_hw = NULL;
_mark_console* mark_console = NULL;

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    //QString run_at_exit;

    Q_INIT_RESOURCE(operator_omsk);

    QApplication app(argc, argv);

    QTextCodec* langcodec=QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(langcodec);
    QCoreApplication::setOrganizationName(VER_COMPANYNAME_STR);
    QCoreApplication::setApplicationName(VER_PRODUCTNAME_STR);

    //for Qt plugins search
    QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath());

    // some set
    string fpath = QCoreApplication::applicationFilePath().toStdString();
    size_t pos = fpath.find("am-client.exe");
    fpath.erase(pos, 13);

    _gl_set().config.run.run_path=fpath;
    _gl_set().config.run.config_file=_gl_set().config.run.run_path+"am-client.conf";

    if (argc>1) _gl_set().config.run.config_file=argv[1];

    //Создание каталога в User (для лога и доп.конфига)
    QDir dirUser(QDir::home().path()+"/eq_operator");
    if(dirUser.exists() == false) {dirUser.mkpath(QDir::home().path()+"/eq_operator");}
    if(dirUser.exists() == false)
    {
        QMessageBox msgbox(QMessageBox::Critical, QCoreApplication::applicationName(),
                           QString("Не удалось создать пользовательский каталог: \n") + QDir::home().path()+"/eq_operator." + QString("\n\nРабота программы будет завершена.\nОбратитесь к системному администратору."),QMessageBox::Ok);
        msgbox.setModal(true); msgbox.exec(); return 1;
    }

    //Сохранение логов
    _gl_set().config.run.pre_log_file=QString(QDir::home().path()+"/eq_operator/log.prelog").toStdString();

    //initialize log

    _log log;

    _log().max_level4module(LOG_COMMON_NAME,15);

    g_log_file_writer = new _log_file_writer(_gl_set().config.run.pre_log_file);
    _log::set_writer(g_log_file_writer);

    log << LOG_COMMON(10) << "-------------------------------------------------------------------------------------" << endl;
    log << LOG_COMMON(10) << fpath << ".exe" << endl;
    log << LOG_COMMON(10) << "Starting " << QCoreApplication::applicationName().toStdString() <<  " ver. " << VER_FILEVERSION_STR << endl;

    //load settings
    try
    {
        _gl_set().load_config_file(_gl_set().config.run.config_file);
    }
    catch(_error &e) // missed settings file full path
    {
        log << LOG_COMMON(10) << "Error '" << e.str() << "'while try load config file " << _gl_set().config.run.config_file << endl;
        QMessageBox msgbox(QMessageBox::Critical, QCoreApplication::applicationName(),
                           QString("Ошибка '")+QString(e.str().c_str())+QString("' загрузки файла настроек ")
                           +QString(_gl_set().config.run.config_file.c_str())+QString("\n\nРабота программы будет завершена.\nОбратитесь к системному администратору."),
                           QMessageBox::Ok);
        msgbox.setModal(true);
        msgbox.exec();

        return 1;
    }

    int pn=0;
    while(1)
    {
        string pv=_text_help::get_field_from_st(_gl_set().config.run.log_levels,";,",pn);
        if (pv.empty()) break;

        string par=_text_help::get_field_from_st(pv,':',0);
        string val=_text_help::get_field_from_st(pv,':',1);

        _log().max_level4module(par,atoi(val.c_str()));

        pn++;
    }

    // detect tray

    if(!QSystemTrayIcon::isSystemTrayAvailable())
    {
        QMessageBox::critical(0, QCoreApplication::applicationName(), QObject::tr("I couldn't detect any system tray on this system."));
        log << LOG_UI(10) << "Error detect system tray." << endl;

        if (!_gl_set().config.external.run_at_exit.empty())
        {
            QProcess rae;
            rae.startDetached(QString(_gl_set().config.external.run_at_exit.c_str()));
        }

        return 1;
    }

    int timeout = 20;
    string errmsg;
    _auth_dlg* auth_dlg = 0;

    bool is_break = false;
login:
    try
    {
        _eq_net_client::_client_status cs;

        if(!is_break)
        {
            //make connection to server

            srv = new _eq_net_client_rmo(_gl_set().config.server.name, _gl_set().config.server.port);
            if(!srv)
            {
                errmsg = "Ошибка подключения.\nНе удалось соединиться с сервером.\nРабота программы будет завершена.";
                THROW_EXCEPTION("Connection error. Null server interface object");
            }

            //initialize led panel if present

//            console_hw = new _console_hw(srv);
//            console_hw->init("hw_port=\\\\.\\COM35,hw_number=1");
//            if(!console_hw->is_init())
//            {
//                delete console_hw;
//                console_hw = NULL;
//            }
//            else
//                console_hw->show("...");

            //initialize qality mark panel if present
            if (!_gl_set().config.mark_console.port.empty() && !_gl_set().config.mark_console.type.empty())
            {
                if      (_gl_set().config.mark_console.type == "pkp003")  mark_console = create_pkp3_mark_console(_gl_set().config.mark_console.port);
                else if(_gl_set().config.mark_console.type == "metaq")    mark_console = create_metaq_mark_console(_gl_set().config.mark_console.port /*string("\\\\.\\") + mc_com_port*/);
                else if (_gl_set().config.mark_console.type == "damask3") mark_console = create_damask3_mark_console(_gl_set().config.mark_console.port, _gl_set().config.mark_console.number);
                else
                {
                    log << LOG_COMMON(0) << "Invalid mark console type in settings. Console will not been initialized." << endl;
                    QMessageBox msgbox(QMessageBox::Warning, QCoreApplication::applicationName(), QString("Недопустимый тип панели оценки качества в настройках.\nПанель не инициализирована."), QMessageBox::Ok);
                    msgbox.setModal(true);
                    msgbox.exec();
                }
                // check connection to mark_console, by calling was_pressed()
                if(mark_console)
                {
                    _mark_console::_key k;
                    if ( mark_console->try_connect() )
                        log << LOG_COMMON(0) << "Mark console init ok (type='" << _gl_set().config.mark_console.type << "', port=" << _gl_set().config.mark_console.port << ", number=" << _gl_set().config.mark_console.number  << ")" << endl;
                    else
                        log << LOG_COMMON(0) << "Mark console connect test - failed  (type='" << _gl_set().config.mark_console.type << "', port=" << _gl_set().config.mark_console.port << ", number=" << _gl_set().config.mark_console.number << ")" << endl;
                }
            }

            //show splach screen

            _splash_screen* splash_screen = new _splash_screen();
            QCursor cursor_wait = QCursor(Qt::WaitCursor);
            qApp->setOverrideCursor(cursor_wait);

            while(1)
            {
                srv->lock();
                cs = srv->status();
                srv->unlock();
                if (cs != _eq_net_client_rmo::client_status_do_connect) break;
                if(--timeout < 0)
                {
                    delete splash_screen;
                    qApp->restoreOverrideCursor();
                    errmsg = "Ошибка подключения.\nПревышено время ожидания соединения.\nРабота программы будет завершена.";
                    THROW_EXCEPTION("Connection error. Connection waiting time is exceeded.");
                    //throw _error(srv->status(), "Authentication error. The waiting time of connection is exceeded.");
                };
                splash_screen->show();
                QApplication::processEvents();
                Sleep(500);
            }
            delete splash_screen;
            qApp->restoreOverrideCursor();

            //check connection and lock

            if(!srv)
                THROW_EXCEPTION("Server object is not initialized.");
            if(cs != _eq_net_client_rmo::client_status_login_need)
                THROW_EXCEPTION("Server object has inconsistent state.");
        }

        //get available operator

        _eq_net_client::_opers_info opers_info;
        srv->lock();
        _eq_err::_code err = srv->get_opers_info(opers_info);
        srv->unlock();
        if(err != _eq_err::ok)
            THROW_EXCEPTION(_eq_err::_server_answ[err]);

        //get available workplaces

        _eq_net_client::_wplaces_info wplaces_info;
        srv->lock();
        err = srv->get_wplaces_info(wplaces_info);
        srv->unlock();
        if(err != _eq_err::ok)
            THROW_EXCEPTION(_eq_err::_server_answ[err]);

        //create authentication dialog

        auth_dlg = new _auth_dlg(opers_info, config_user.getConfPropertyStr("last_login"), wplaces_info, config_user.getConfPropertyStr("last_workstation"), is_break);
        if(!auth_dlg)
            THROW_EXCEPTION("Authentication error. Could not create authentication dialog.");

        //authentication

        for(int count_auth = 3; count_auth != 0; --count_auth) //attempts
        {
            // reconnect after busy workplace, busy operator:
            if(!is_break) {
                if(!srv) srv = new _eq_net_client_rmo(_gl_set().config.server.name, _gl_set().config.server.port);
                if(!srv) {
                    errmsg = "Ошибка подключения.\nНе удалось соединиться с сервером.\nРабота программы будет завершена.";
                    THROW_EXCEPTION("Connection error. Null server interface object");
                }
            }

            {
            int retExec=auth_dlg->exec();
            if(!auth_dlg->get_username().isEmpty())  config_user.setConfPropertyStr("last_login",auth_dlg->get_username());
            if(!auth_dlg->get_workplace().isEmpty()) config_user.setConfPropertyStr("last_workstation",auth_dlg->get_workplace());
            if(retExec == QDialog::Rejected) // now waiting for username/passsword input signal or cancel from the dialog...
            {
                delete auth_dlg;
                if (!_gl_set().config.external.run_at_exit.empty())
                {
                    QProcess rae;
                    rae.startDetached(QString(_gl_set().config.external.run_at_exit.c_str()));
                }

                return 0; //authentication canceled by user
            }
            }

            if(is_break) //return from brake
            {
//TODO:                srv->lock();
//TODO:                if(srv->my_oper()->is_correct_pass(auth_dlg->get_password().toStdString()))
                if(true)//till is_correct_pass is realised :)
                {
                    srv->lock();//TODO:
                    err = srv->my_wplace()->continue_work();
                    srv->unlock();
                    if(err != _eq_err::ok)
                        THROW_EXCEPTION(_eq_err::_server_answ[err]);
                    break; // continue with start main window
                }
                else
                {
//TODO:                    srv->unlock();
                    QMessageBox msgbox(QMessageBox::Critical, QCoreApplication::applicationName(), "Неверный пароль.", QMessageBox::Ok);
                    msgbox.setModal(true);
                    msgbox.exec();
                    continue;
                }
            }

            //login

            srv->lock();
//            qDebug() << auth_dlg->get_username() << " | " << auth_dlg->get_password() << " | " << auth_dlg->get_workplace();
            err = srv->login(auth_dlg->get_username().toStdString(), auth_dlg->get_password().toStdString(), auth_dlg->get_workplace().toStdString());
            cs = srv->status();
            srv->unlock();

            if(err == _eq_err::ok && cs == _eq_net_client_rmo::client_status_ok)
            {
                _gl_set().config.ui.single_call_button=false;
                std::string sb_set = srv->my_oper()->get_setting("call_ticket_single_button", "0");
                if (sb_set=="yes" || sb_set=="true" || sb_set=="1") _gl_set().config.ui.single_call_button=true;

                //for top bar indicators

                _gl_set().config.office.name=QString::fromLocal8Bit(srv->my_office()->name().c_str());
                _gl_set().config.user.name=auth_dlg->get_username();
                _gl_set().config.user.wp_name=auth_dlg->get_workplace();

                //settings->write_settings();

                break; // continue with start main window
            }
            else
            {
                switch(cs)
                {
                case _eq_net_client_rmo::client_status_do_connect: //TODO: server error, remove if fixed
                case _eq_net_client_rmo::client_status_auth_err:
                    errmsg = "Ошибка аутентификации.\nНеверный логин или пароль.";
                    break;
                case _eq_net_client_rmo::client_status_xml_err:
                    errmsg = "Ошибка подключения.\nПолучен некорректный ответ от сервера (пустой XML).";
                    break;
                case _eq_net_client_rmo::client_status_dup_reg:
                    errmsg = "Ошибка аутентификации.\nПользователь с такими параметрами уже работает в системе.";
                    break;
                case _eq_net_client_rmo::client_status_halted:
                    errmsg = "Ошибка подключения.\nСоединение с сервером приостановлено.";
                    break;
                case _eq_net_client_rmo::client_status_no_such_wp:
                    errmsg = "Ошибка подключения.\nУказанное рабочее место не существует.";
                    break;
                case _eq_net_client_rmo::client_status_wp_busy:
                    errmsg = "Ошибка подключения.\nУказанное рабочее место занято.";
                    break;
                }

                log << LOG_COMMON(0) << errmsg.c_str() << endl;
                QMessageBox msgbox(QMessageBox::Critical, QCoreApplication::applicationName(), QString::fromLocal8Bit(errmsg.c_str()), QMessageBox::Ok);
                msgbox.setModal(true);
                msgbox.exec();

                RELEASE(srv);
            }
        }//for

        delete auth_dlg;
        auth_dlg = 0;

        if(srv) {
            srv->lock();
            cs = srv->status();
            srv->unlock();
        }
        if(cs != _eq_net_client_rmo::client_status_ok)
        {
            errmsg = "Ошибка подключения.\nПревышено допустимое количество попыток.\nРабота программы будет завершена.";
            THROW_EXCEPTION("Authentication error. The number of attempts of connection is exceeded.");
        }
    }
    catch(_rdg_exception &e)
    {
        if(console_hw) { delete console_hw; console_hw = NULL; }
        if(mark_console) { mark_console->release(); mark_console = NULL; }
        RELEASE(srv);
        if(auth_dlg) { delete auth_dlg; auth_dlg = NULL; }

        log << LOG_CONNECTION(0) << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
        if(g_log_file_writer) delete g_log_file_writer;
        QMessageBox msgbox(QMessageBox::Critical, QCoreApplication::applicationName(), QString::fromLocal8Bit(errmsg.c_str()), QMessageBox::Ok);
        msgbox.setModal(true);
        msgbox.exec();

        if (!_gl_set().config.external.run_at_exit.empty())
        {
            QProcess rae;
            rae.startDetached(QString(_gl_set().config.external.run_at_exit.c_str()));
        }

        return 2;
    }
    catch(...) //TODO
    {
        if(console_hw) { delete console_hw; console_hw = NULL; }
        if(mark_console) { mark_console->release(); mark_console = NULL; }
        RELEASE(srv);
        if(auth_dlg) { delete auth_dlg; auth_dlg = NULL; }

        log << LOG_CONNECTION(0) << "Unrecognized authentication error." << endl;
        if(g_log_file_writer) delete g_log_file_writer;
        QMessageBox msgbox(QMessageBox::Critical, QCoreApplication::applicationName(), QString("Ошибка подключения.\nРабота программы будет завершена."), QMessageBox::Ok);
        msgbox.setModal(true);
        msgbox.exec();

        if (!_gl_set().config.external.run_at_exit.empty())
        {
            QProcess rae;
            rae.startDetached(QString(_gl_set().config.external.run_at_exit.c_str()));
        }

        return 2;
    }

    int ret = 0;

    try
    {
        _main_win mainwnd;
        mainwnd.setFixedSize(865, 680);
        mainwnd.setWindowTitle(QCoreApplication::applicationName());
        mainwnd.init_form();
        mainwnd.show();

        ret = app.exec();
        if(ret == -2) //take break
        {
            is_break = true;
            goto login;
        }
        RELEASE(srv);
        if(console_hw) { delete console_hw; console_hw = NULL; }
        if(mark_console) { mark_console->release(); mark_console = NULL; }
        if(ret == -1) //relogin
        {
            is_break = false;
            goto login;
        }

        delete g_log_file_writer; g_log_file_writer = NULL;
    }
    catch(_rdg_exception& e)
    {
        if(console_hw) { delete console_hw; }
        if(mark_console) { mark_console->release(); }
        RELEASE(srv);
        log << LOG_COMMON(0) << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
        if(g_log_file_writer) delete g_log_file_writer;
        ret = 1;
    }
    catch(exception &e)
    {
        if(console_hw) { delete console_hw; }
        if(mark_console) { mark_console->release(); }
        RELEASE(srv);
        log << LOG_COMMON(0) << e.what() << endl;
        QMessageBox::warning(0, QString::fromLocal8Bit("Внимание: "), QString::fromUtf8(e.what()));
        if(g_log_file_writer) delete g_log_file_writer;
        ret = 1;
    }
    catch(_error &e)
    {
        if(console_hw) { delete console_hw; }
        if(mark_console) { mark_console->release(); }
        RELEASE(srv);
        log << LOG_COMMON(0) << e.num() << " : " << e.str() << endl;
        if(g_log_file_writer) delete g_log_file_writer;
        ret = e.num();
    }
    catch(int& tc) // termination code
    {
        if(console_hw) { delete console_hw; }
        if(mark_console) { mark_console->release(); }
        RELEASE(srv);
        string errStr = "The operator cancelled authentication.";
        if(g_log_file_writer) delete g_log_file_writer;
        log << LOG_CONNECTION(0) << errStr << endl;
        ret = tc;
    }
    catch(...) //TODO
    {
        if(console_hw) { delete console_hw; }
        if(mark_console) { mark_console->release(); }
        RELEASE(srv);
        log << LOG_COMMON(0) << "Unrecognized main window error error." << endl;
        if(g_log_file_writer) delete g_log_file_writer;
        QMessageBox msgbox(QMessageBox::Critical, QCoreApplication::applicationName(), QString("Неопознанная ошибка.\nРабота программы будет завершена."), QMessageBox::Ok);
        msgbox.setModal(true);
        msgbox.exec();
        ret = 2;
    }

    if (!_gl_set().config.external.run_at_exit.empty())
    {
        QProcess rae;
        rae.startDetached(QString(_gl_set().config.external.run_at_exit.c_str()));
    }

    return ret;
}

void start_main_wnd(void)
{

}
