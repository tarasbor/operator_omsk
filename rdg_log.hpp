#ifndef __RDG_LOG_H__
#define __RDG_LOG_H__

//#include <log.hpp> // use commonlib/log2
#include <log_file_writer.hpp> // use commonlib/log2

using namespace std;

extern _log_file_writer* g_log_file_writer; // mainQML.cpp

#define LOG_COMMON_NAME "prog"
#define LOG_CONNECTION_NAME "connection"
#define LOG_DATA_NAME "data"
#define LOG_UI_NAME "ui"
#ifdef DEBUG
    #define LOG_DEBUG_NAME "DEBUG"
#endif

/*
// для установки уровней логирования из настроек
#define LOG_LEVEL_COMMON(level) _log::max_level4module(LOG_COMMON_NAME, level)
#define LOG_LEVEL_CONNECTION(level) _log::max_level4module(LOG_CONNECTION_NAME, level)
#define LOG_LEVEL_DATA(level) _log::max_level4module(LOG_DATA_NAME, level)
#define LOG_LEVEL_UI(level) _log::max_level4module(LOG_UI_NAME, level)
#ifdef DEBUG
    #define LOG_LEVEL_DEBUG(level) _log::max_level4module(LOG_DEBUG_NAME, level)
#endif
*/

// для установки уровня во время записи в лог (в операторе << )
#define LOG_COMMON(level) _log::module_level(LOG_COMMON_NAME, level)
#define LOG_CONNECTION(level) _log::module_level(LOG_CONNECTION_NAME, level)
#define LOG_DATA(level) _log::module_level(LOG_DATA_NAME, level)
#define LOG_UI(level) _log::module_level(LOG_UI_NAME, level)
#ifdef DEBUG
    #define LOG_DEBUG(level) _log::module_level(LOG_DEBUG_NAME, level)
#endif

#endif
