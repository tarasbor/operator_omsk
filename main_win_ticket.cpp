#include "main_win.hpp"
#include "ui_main_win.h"

//#include "version.h"
#include "settings.hpp"
#include "list_selection_dlg.h"

#include <QPushButton>
#include <QMetaType>
#include <QDate>
#include <QTime>
#include <QString>

#include <QImage>
#include <QPainter>
#include <QCoreApplication>
#include <QLabel>
#include <QDateTime>
#include <QPrinter>
#include <QList>
#include <QPrinterInfo>
#include <string>


//#include <debug.hpp>
//#include <debug_extern.h>
#ifdef DEBUG
    #include <QDebug>
#endif

using namespace std;

// ticket operations ---------------------------------------------------------------------

void _main_win::on_ticket_open(void)
{
    m_ui->fr_queues->setEnabled(false);
    m_ui->fr_ticket->setEnabled(true);
    if(_gl_set().config.run.record_audio == 1)
    {

        _log log;
        this->aRecorder.loadDefaultSettings();
        log << "Audio recorder: loading settings" << endl;
        //this->aRecorder.loadDefaultSettings();
        log << "Audio recorder: setting filename" << endl;
        aRecorder.setFileName(QString::fromStdString(std::to_string(srv->my_wplace()->current_ticket()->id().sid))  +QString("_") + QDate::currentDate().toString("dd.MM.yyyy") +QString("_") + QString(QTime::currentTime().toString("hh.mm.ss")) +QString("_")  + QString::fromStdString(srv->my_wplace()->current_ticket()->number()) +QString("_") + QString::fromStdString(srv->my_wplace()->current_oper()->login()) + ".wav");
        log << "Audio recorder: setting audiodevice" << endl;
        aRecorder.setAudioDevice(config_user.getConfPropertyStr("audio_recorder"));
        log << "Quality: " << config_user.getConfPropertyInt("audio_quality") << endl;
        aRecorder.setQuality(config_user.getConfPropertyInt("audio_quality"));
        log << "Audio recorder: recording started" << endl;
        aRecorder.startRecording();
    }
    if (video_gw) video_gw->need_write(true);
}

void _main_win::on_ticket_close(void)
{
    if (video_gw) video_gw->need_write(false);

    m_ui->fr_queues->setEnabled(true);
    m_ui->fr_ticket->setEnabled(false);

    m_ui->btn_less_issues->setEnabled(false);
    m_ui->btn_issues_1->setText("1");
    m_ui->btn_issues_1->setChecked(true);
    m_ui->btn_issues_2->setText("2");
    m_ui->btn_issues_3->setText("3");
    m_ui->btn_issues_4->setText("4");
    m_ui->btn_issues_5->setText("5");
    m_ui->btn_more_issues->setEnabled(true);
    if(aRecorder.getState() && _gl_set().config.run.record_audio == 1)
    {
        aRecorder.stopRecording(); aRecorder.sendFile(); aRecorder.delFile();
        _log log;
        log << "Audio recorder: recording stopped" << endl;
    }
    // FIX BUG close/start ticket less then 1 second
    if(!m_ticket_in_proc_service_time.isNull())
        m_ticket_in_proc_service_time = QTime(0, 0);
}

void _main_win::printticket(QString queuename, QString ticketnum)
{
    QImage image(500, 500, QImage::Format_ARGB32_Premultiplied);
    QPixmap imagelogo;
    imagelogo.load(QCoreApplication::applicationDirPath() + "/logo-print.png");
    imagelogo = imagelogo.scaledToWidth(416);
    QPrinterInfo pi;
    QList<QPrinterInfo> printerlist = pi.availablePrinters();
    _log log;
    for(QList<QPrinterInfo>::iterator i = printerlist.begin(); i != printerlist.end(); i++)
    {
        if((*i).printerName() == config_user.getConfPropertyStr("printer",""))
        {
            log << LOG_COMMON(10) << (*i).printerName().toStdString() << endl;
            pi = (*i);
        }
    }
    QPrinter printer(pi);
    //QPrinter printer;
    printer.setOutputFormat(QPrinter::NativeFormat);
    printer.setResolution(160);
    printer.setFullPage(true);
    QPainter p;
    p.begin(&printer);
    if (!p.begin(&image)) qDebug() << false;

    p.setPen(QPen(Qt::black));
    p.setFont(QFont("Times", 18, QFont::Bold));
    p.drawPixmap(42,0,imagelogo);
    QString qnp1 = "", qnp2 = "";
    for(int i = queuename.length()/2; i >= 0; i--)
    {
        if(queuename[i] == " " && i != 0)
        {
            qnp1 = queuename.mid(0,i);
            qnp2 = queuename.mid(i,queuename.length() - 1);
            p.drawText(QRect(75, 130, 350, 50), Qt::AlignCenter, qnp1);
            p.drawText(QRect(75, 180, 350, 50), Qt::AlignCenter, qnp2);
            break;
        }
        if(i == 0)
        {
            p.drawText(QRect(75, 150, 350, 50), Qt::AlignCenter, queuename);
        }
    }


    p.setFont(QFont("Times", 60, QFont::Bold));
    p.drawText(QRect(50, 250, 400, 150), Qt::AlignCenter, ticketnum);

    QDateTime qdt;

    p.setFont(QFont("Times", 8, QFont::Bold));
    p.drawText(QRect(50, 450, 400, 50), Qt::AlignCenter, QString("Талон выдан ") + qdt.currentDateTime().toString("dd.MM.yy \t hh:mm"));

    p.end();

//    QString path = QCoreApplication::applicationDirPath() + "/output.png";
//    image.save(path);
}

void _main_win::do_call_next_ticket(/*QString queue_id*/)
{
    QString queue_id = _gl_set().config.ui.single_call_button ?
                sender()->objectName().mid(9) : //CALL_NEXT
                sender()->objectName().mid(11); //CALL_NEXT_Q

    if(sender() == buttoNextTicket2) { queue_id = queueIdCallNextTicket; }

    if(queue_id.isEmpty())
        return; //

    _log log;
    //log << LOG_UI(0) << "do_call_next_ticket (queue id = " << queue_id.toStdString() << ")..." << endl;

    try{
        SRV_CHECK_STATUS_AND_LOCK

        if(srv->my_wplace()->current_ticket())
        {
            srv->unlock();
            err_msg(QMessageBox::Warning, "Талон уже открыт.");
            log << LOG_UI(0) << "...do_call_next_ticket aborted - ticket has already open." << endl;
            return;
        }

        // get current queue
        _eq::_id id;
        id.from_c_string(queue_id.toStdString().c_str());
        _eq::_qs::const_iterator itq = srv->my_wplace()->qs()->find(id);
        if(itq == srv->my_wplace()->qs()->end())
        {
            err_msg(QMessageBox::Critical);
            THROW_EXCEPTION("Could not find selected queue's object.");
        }
        _eq::_q* q = itq->second;

        if(q->tickets()->size() == 0)
        {
            log << LOG_UI(0) << "...do_call_next_ticket aborted - q is empty." << endl;
            srv->unlock();
            return;
        }

        // get next (the oldest) ticket
        _eq::_ticket* t = q->tickets()->begin()->second;
        //log << LOG_UI(0) << "...next ticket number = " << t->number() << endl;

        //log << LOG_UI(0) << "...processing starting..." << endl;
        _eq_err::_code err = srv->my_wplace()->start_proc_ticket(t);
        if(t) log << LOG_UI(0) << "...call_next_ticket = " << t->number() << " ..." << endl;
        if(err == _eq_err::ok)
        {
            /*
            log << LOG_UI(0) << "...removing from the queue..." << endl;
            _eq_err::_code err = q->remove_ticket(q->tickets()->begin());
            if(err != _eq_err::ok)
            {
                log << LOG_UI(0) << "...can not remove from q..." << err << endl;
                err_msg(QMessageBox::Critical);
                THROW_EXCEPTION(_eq_err::_server_answ[err]);
            }
            */
        }
        else
        {
            log << LOG_UI(0) << "...can not start..." << err << endl;
            err_msg(QMessageBox::Warning, "Не удалось открыть талон. Вероятно, он был только что открыт другим оператором.");
            THROW_EXCEPTION(_eq_err::_server_answ[err]);
        }

        srv->unlock();

        //log << LOG_UI(0) << "...reflecting state change in UI..." << endl;
//        emit on_ticket_open();
        on_ticket_open();

        //log << LOG_UI(0) << "...do_call_next_ticket done." << endl;
    }
    catch(_rdg_exception& e)
    {
        srv->unlock();
        log << LOG_COMMON(0) << "Calling next ticket command was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
    }
    catch(...)
    {
        srv->unlock();
        log << LOG_COMMON(0) << "Calling next ticket command was failed by unknown reason." << endl;
    }
}

void _main_win::do_call_next_ticket_by_num(/*QString queue_id*/)
{
    QString queue_id = sender()->objectName().mid(10);//CALL_NUM_Q

    _log log;
    //log << LOG_UI(0) << "do_call_next_ticket_by_num (queue id = " << queue_id.toStdString() << ")..." << endl;


    try
    {
        SRV_CHECK_STATUS_AND_LOCK

        if(srv->my_wplace()->current_ticket())
        {
            srv->unlock();
            err_msg(QMessageBox::Warning, "Талон уже открыт.");
            log << LOG_UI(0) << "...do_call_next_ticket_by_num aborted - ticket has already open." << endl;
            return;
        }

        // get selected queue
        _eq::_id id;
        id.from_string(queue_id.toStdString());
        _eq::_qs::const_iterator itq = srv->my_wplace()->qs()->find(id);
        if(itq == srv->my_wplace()->qs()->end())
        {
            log << LOG_UI(0) << "...do_call_next_ticket_by_num aborted - can not find q." << endl;
            err_msg(QMessageBox::Critical);
            THROW_EXCEPTION("Could not find selected queue's object.");
        }
        _eq::_q* q = itq->second;

        if(q->tickets()->size() == 0)
        {
            log << LOG_UI(0) << "...do_call_next_ticket_by_num aborted - q is empty." << endl;
            srv->unlock();
            return;
        }

        // fetch list of tickets from selected queue

//        QList<QStringList> tlist;
        QList<QList<QStandardItem*> > rows;
        char tmps[128];
        for(_eq::_tickets::const_iterator itt = q->tickets()->begin(); itt != q->tickets()->end(); itt++)
        {
            QDateTime dt = QDateTime::fromMSecsSinceEpoch(itt->second->put_to_q_time().stamp() / 10000).toLocalTime();
//            tlist << (QStringList() << QString::fromLocal8Bit(itt->second->number().c_str())
//                     << QString().sprintf("%02d:%02d:%02d", dt.time().hour(), dt.time().minute(), dt.time().second()));
            QStandardItem* item_num = new QStandardItem(QString::fromLocal8Bit(itt->second->number().c_str()));
            item_num->setData(QString(itt->second->id().to_c_string(tmps)));
            QStandardItem* item_dt = new QStandardItem(QString().sprintf("%02d:%02d:%02d", dt.time().hour(), dt.time().minute(), dt.time().second()));
            item_dt->setData(QString(itt->second->id().to_c_string(tmps)));
            rows << (QList<QStandardItem*>() << item_num << item_dt);
            log << LOG_UI(0) << "list..." << itt->second->number() << " = " << tmps << endl;
        }

        srv->unlock();

        log << LOG_UI(0) << "...show tickets' list..." << endl;
        _list_selection_dlg dlg(QStringList() << "№" << "выдан", /*tlist*/rows, 400, 100, 400, 527, this);
        dlg.show();
        dlg.exec();
        QVariant tdata = dlg.get_sel_item_data();
        if(tdata.isNull())
        {
            //user canceled selection
            //srv->unlock();
            log << LOG_UI(0) << "...user canceled ticket selection." << endl;
            return;
        }
        //log <<  LOG_UI(0) << "...selected ticket = " << tdata.toString().toStdString() << " ..." << endl;

        SRV_CHECK_STATUS_AND_LOCK

        // find selected ticket
        //_eq::_tickets::const_iterator itt = q->tickets()->find(_eq::_id((tdata.toString()).toStdString()));
        _eq::_id search_id = _eq::_id((tdata.toString()).toStdString());

        // we MUST use server->...->.... to get new Q ptr - OLD Q PTR can die when we unlock server
        id.from_string(queue_id.toStdString());
        itq = srv->my_wplace()->qs()->find(id);
        if(itq == srv->my_wplace()->qs()->end())
        {
            log << LOG_UI(0) << "...do_call_next_ticket_by_num aborted - can not find q." << endl;
            err_msg(QMessageBox::Critical);
            THROW_EXCEPTION("Could not find selected queue's object.");
        }
        q = itq->second;

        _eq::_tickets::const_iterator itt = q->tickets()->begin();
        while(itt!=q->tickets()->end())
        {
            //log <<  LOG_UI(0) << "...check ticket " << tdata.toString().toStdString() << " and " << itt->second->id().to_c_string(tmps) << " " << itt->second->number() << endl;
            if (itt->second->id()==search_id)
            {
                //log <<  LOG_UI(0) << "...check ticket - found" << endl;
                break;
            }
            itt++;
        }
        if(itt == q->tickets()->end())
        {
            log << LOG_UI(0) << "...Can not find ticket (before start) - may be someone call it." << endl;
            err_msg(QMessageBox::Warning, "Талон не найден. Вероятно, он был только что открыт другим оператором.");
            THROW_EXCEPTION("Could not find queue of selected ticket.");
        }
        _eq::_ticket* t = itt->second;
        //log << LOG_UI(0) << "...selected ticket (number = " << t->number() << ") is found..." << endl;

        //log << LOG_UI(0) << "...processing starting..." << endl;
        _eq_err::_code err = srv->my_wplace()->start_proc_ticket(t);
        if(t) log << LOG_UI(0) << "...call_next_ticket_by_num = " << t->number() << "..." << endl;
        if(err == _eq_err::ok)
        {
            //log << LOG_UI(0) << "...removing from the queue..." << endl;
            /*
            _eq_err::_code err = q->remove_ticket(itt);
            if(err != _eq_err::ok)
            {
                //log << LOG_UI(0) << "...can not remove ticket from q " << err << endl;
                err_msg(QMessageBox::Critical);
                THROW_EXCEPTION(_eq_err::_server_answ[err]);
            }
            */
        }
        else
        {
            //log << LOG_UI(0) << "...can not start ticket from q " << err << endl;
            err_msg(QMessageBox::Warning, "Не удалось открыть талон. Вероятно, он был только что открыт другим оператором.");
            THROW_EXCEPTION(_eq_err::_server_answ[err]);
        }

        srv->unlock();

        //log << LOG_UI(0) << "...reflecting state change in UI..." << endl;
        on_ticket_open();

        //log << LOG_UI(0) << "...do_call_next_ticket_by_num done." << endl;
    }
    catch(_rdg_exception& e)
    {
        srv->unlock();
        log << LOG_COMMON(0) << "Calling next ticket by num command was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
    }
    catch(...)
    {
        srv->unlock();
        log << LOG_COMMON(0) << "Calling next ticket by num command was failed by unknown reason." << endl;
    }
}

void _main_win::on_btn_less_issues_clicked()
{
    if(m_ui->btn_issues_1->text().toInt() == 1)
        return;
    if(!m_ui->btn_more_issues->isEnabled())
        m_ui->btn_more_issues->setEnabled(true);
    m_ui->btn_issues_1->setText(QString::number(m_ui->btn_issues_1->text().toInt() - 1));
    m_ui->btn_issues_2->setText(QString::number(m_ui->btn_issues_2->text().toInt() - 1));
    m_ui->btn_issues_3->setText(QString::number(m_ui->btn_issues_3->text().toInt() - 1));
    m_ui->btn_issues_4->setText(QString::number(m_ui->btn_issues_4->text().toInt() - 1));
    m_ui->btn_issues_5->setText(QString::number(m_ui->btn_issues_5->text().toInt() - 1));
    if(m_ui->btn_issues_1->text().toInt() == 1)
        m_ui->btn_less_issues->setEnabled(false);
}

void _main_win::on_btn_more_issues_clicked()
{
    if(m_ui->btn_issues_5->text().toInt() == 9)
        return;
    if(!m_ui->btn_less_issues->isEnabled())
        m_ui->btn_less_issues->setEnabled(true);
    m_ui->btn_issues_1->setText(QString::number(m_ui->btn_issues_1->text().toInt() + 1));
    m_ui->btn_issues_2->setText(QString::number(m_ui->btn_issues_2->text().toInt() + 1));
    m_ui->btn_issues_3->setText(QString::number(m_ui->btn_issues_3->text().toInt() + 1));
    m_ui->btn_issues_4->setText(QString::number(m_ui->btn_issues_4->text().toInt() + 1));
    m_ui->btn_issues_5->setText(QString::number(m_ui->btn_issues_5->text().toInt() + 1));
    if(m_ui->btn_issues_5->text().toInt() == 9)
        m_ui->btn_more_issues->setEnabled(false);
}

void _main_win::on_btn_issues_1_clicked()
{
    m_ui->btn_issues_1->setChecked(true);
}

void _main_win::on_btn_issues_2_clicked()
{
    m_ui->btn_issues_2->setChecked(true);
}

void _main_win::on_btn_issues_3_clicked()
{
    m_ui->btn_issues_3->setChecked(true);
}

void _main_win::on_btn_issues_4_clicked()
{
    m_ui->btn_issues_4->setChecked(true);
}

void _main_win::on_btn_issues_5_clicked()
{
    m_ui->btn_issues_5->setChecked(true);
}

void _main_win::on_btn_close_ticket_clicked()
{
    int issues_count = 1;
    if(m_ui->btn_issues_1->isChecked())
        issues_count = m_ui->btn_issues_1->text().toInt();
    else if(m_ui->btn_issues_2->isChecked())
        issues_count = m_ui->btn_issues_2->text().toInt();
    else if(m_ui->btn_issues_3->isChecked())
        issues_count = m_ui->btn_issues_3->text().toInt();
    else if(m_ui->btn_issues_4->isChecked())
        issues_count = m_ui->btn_issues_4->text().toInt();
    else if(m_ui->btn_issues_5->isChecked())
        issues_count = m_ui->btn_issues_5->text().toInt();
//    qDebug() << "issues_count = " << issues_count;

    _log log;
    log << LOG_UI(0) << "do_ticket_processing_done (issues_count = " << issues_count << ")..." << endl;

    try
    {
        SRV_CHECK_STATUS_AND_LOCK

        if(!srv->my_wplace()->current_ticket())
        {
            srv->unlock();
            err_msg(QMessageBox::Warning, "Нет открытого талона.");
            log << "...do_ticket_processing_done aborted - no opened ticket." << endl;
            return;
        }
        log << "...ticket number = " << srv->my_wplace()->current_ticket()->number() << " ..." << endl;

        log << "...closing the ticket..." << endl;
        _eq_err::_code err = srv->my_wplace()->close_proc_ticket(_eq::_close_ticket_served, issues_count);
        if(err != _eq_err::ok)
        {
            err_msg(QMessageBox::Critical);
            THROW_EXCEPTION(_eq_err::_server_answ[err]);
        }

        srv->unlock();

        log << "...reflecting state change in UI..." << endl;
        on_ticket_close();        

        log << "...do_ticket_processing_done done." << endl;
    }
    catch(_rdg_exception& e)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Setting ticket as done command was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
    }
    catch(...)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Setting ticket as done command was failed by unknown reason." << endl;
    }
}

void _main_win::on_btn_recall_clicked()
{
    _log log;
    log << LOG_UI(10) << "do_recall_next_ticket..." << endl;

    try
    {
        SRV_CHECK_STATUS_AND_LOCK

        if(!srv->my_wplace()->current_ticket())
        {
            srv->unlock();
            err_msg(QMessageBox::Warning, "Нет открытого талона.");
            log << "...do_recall_next_ticket aborted - no opened ticket." << endl;
            return;
        }
        log << "...ticket number = " << srv->my_wplace()->current_ticket()->number() << " ..." << endl;

        log << "...recalling the ticket..." << endl;
        _eq_err::_code err = srv->my_wplace()->recall_proc_ticket();
        if(err != _eq_err::ok)
        {
            err_msg(QMessageBox::Critical);
            THROW_EXCEPTION(_eq_err::_server_answ[err]);
        }

        srv->unlock();

        log << "...do_recall_next_ticket done." << endl;
    }
    catch(_rdg_exception& e)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Recalling next ticket command was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
    }
    catch(...)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Recalling next ticket command was failed by unknown reason." << endl;
    }
}

void _main_win::on_btn_return_to_queue_clicked()
{
    _log log;
    log << LOG_UI(10) << "do_return_ticket_to_queue..." << endl;

    try
    {
        SRV_CHECK_STATUS_AND_LOCK

        if(!srv->my_wplace()->current_ticket())
        {
            srv->unlock();
            err_msg(QMessageBox::Warning, "Нет открытого талона.");
            log << "...do_return_ticket_to_queue aborted - no opened ticket." << endl;
            return;
        }

        string ticket_num = srv->my_wplace()->current_ticket()->number();
        log << "...ticket number = " << ticket_num << " ..." << endl;

        const _eq::_q* cq = srv->my_wplace()->current_ticket()->from_q();
        //if(!cq)...
        _eq::_q* q = srv->my_office()->qs()->find(cq->id())->second;
        //if(!q)...
        string sbuf;
        log << "...queue id = " << q->id().to_string(sbuf) << " ..." << endl;

        // RIGHT sequence is:
        // create ticket in q
        // if create ok - close current ticket

        //remake back the ticket in it's source queue
        _eq::_tickets::iterator tit;
        log << "...creating ticket..." << endl;
        //string bcount, wait_time;
        int wait_time;
        //_eq_err::_code err = q->create_ticket(tit, &ticket_num, 0, bcount, wait_time);
        _eq_err::_code err = q->create_ticket(tit, srv->my_wplace()->current_ticket(), true, &wait_time);
        if(err != _eq_err::ok)
        {
            if(err != _eq_err::create_ticket_q_on_rest)
            {
                srv->unlock();
                err_msg(QMessageBox::Warning, "Время работы выбранной очереди закончилось.");
                log << "...do_return_ticket_to_queue aborted - the queue is out of worktime." << endl;
                return;
            }
            else
            {
                err_msg(QMessageBox::Critical);
                THROW_EXCEPTION(_eq_err::_server_answ[err]);
            }
        }
        else
        {
            log << "...closing the ticket..." << endl;
            err = srv->my_wplace()->close_proc_ticket(_eq::_close_ticket_back_to_q, 0);
            if(err != _eq_err::ok)
            {
                err_msg(QMessageBox::Critical);
                THROW_EXCEPTION(_eq_err::_server_answ[err]);
            }
        }

        srv->unlock();

        log << "...reflecting state change in UI..." << endl;
        on_ticket_close();

        log << "...do_return_ticket_to_queue done." << endl;
    }
    catch(_rdg_exception& e)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Returning ticket to queue command was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
    }
    catch(...)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Returning ticket to queue command was failed by unknown reason." << endl;
    }
}

void _main_win::on_btn_set_absent_clicked()
{
    _log log;
    log << LOG_UI(10) << "do_reset_ticket_to_absent..." << endl;

    try
    {
        SRV_CHECK_STATUS_AND_LOCK

        if(!srv->my_wplace()->current_ticket())
        {
            srv->unlock();
            err_msg(QMessageBox::Warning, "Нет открытого талона.");
            log << "...do_reset_ticket_to_absent aborted - no opened ticket." << endl;
            return;
        }
        log << "...ticket number = " << srv->my_wplace()->current_ticket()->number() << " ..." << endl;

        log << "...closing the ticket..." << endl;
        _eq_err::_code err = srv->my_wplace()->close_proc_ticket(_eq::_close_ticket_oper_abort, 0);
        if(err != _eq_err::ok)
        {
            err_msg(QMessageBox::Critical);
            THROW_EXCEPTION(_eq_err::_server_answ[err]);
        }

        srv->unlock();

        log << "...reflecting state change in UI..." << endl;
        on_ticket_close();

        log << "...do_reset_ticket_to_absent done." << endl;
    }
    catch(_rdg_exception& e)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Ticket abortion command was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
    }
    catch(...)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Ticket abortion command was failed by unknown reason." << endl;
    }
}

struct lstq
{
    bool at_wp;
    int snum;
    QString id;
};
Q_DECLARE_METATYPE(lstq)

template <typename T>
struct q_list_less
{
    bool operator()(const T& a, const T& b) const
    {
        lstq qa, qb;
        qa = ((QVariant)a[0]->data()).value<lstq>();
        qb = ((QVariant)b[0]->data()).value<lstq>();
        if(!qa.at_wp && qb.at_wp)
            return true;
        if(qa.at_wp && !qb.at_wp)
            return false;
        if(qa.at_wp && qb.at_wp)
            return a[0]->text() < b[0]->text();
        return qa.snum < qb.snum;
    }
};

void _main_win::on_btn_forward_clicked()
{
    _log log;
    log << LOG_UI(10) << "do_return_ticket_to_queue..." << endl;

    try
    {
        SRV_CHECK_STATUS_AND_LOCK

        _eq::_ticket_in_proc* tip = srv->my_wplace()->current_ticket();
        if(!tip)
        {
            srv->unlock();
            err_msg(QMessageBox::Warning, "Нет открытого талона.");
            log << "...do_return_ticket_to_queue aborted - no opened ticket." << endl;
            return;
        }
        string ticket_num = tip->number();
        //_time ticket_time = tip->create_time();
        _time ticket_put_time = tip->put_to_q_time();
        log << "...ticket number = " << ticket_num << " ..." << endl;

        // prepare queues list

        QList<QList<QStandardItem*> > rows;
        char tmps[64];
        _eq::_qs::const_iterator itq = srv->my_office()->qs()->begin();

        while(itq != srv->my_office()->qs()->end())
        {
            lstq qdata;
            qdata.snum = itq->second->short_number();
            qdata.id = itq->second->id().to_c_string(tmps);
            qdata.at_wp = itq->second->name()[0] == '@';
            QStandardItem* item = NULL;

            if(qdata.at_wp)
            {
                if(!srv->my_office()->use_wp_queues())
                {
                    itq++;
                    continue;
                }

                _eq::_id wpid;
                wpid.from_c_string(itq->second->name().c_str() + 1);
                _eq::_wplace* wp = srv->my_office()->wplaces()->find(wpid)->second;
                if(!wp)
                {
                    err_msg(QMessageBox::Critical);
                    THROW_EXCEPTION(string("Could not find workplace for dedicated queue '") + itq->second->name() + "'");
                }

                //Скипаем не занятые рабочие места (за которыми никто не работает)
                if(wp->current_oper() == NULL)
                {
                    itq++;
                    continue;
                }

                item = new QStandardItem(QString("РМ: ") + QString::fromLocal8Bit(wp->name().c_str()));
            }
            else
            {
                // oper can not redir to q with no one work place assigment
                if (itq->second->wplaces()->empty())
                {
                    itq++;
                    continue;
                }
                // Предыдущий if принимал и отключённые рабочие места. Исправим
                _eq::_wplaces::const_iterator itWp = itq->second->wplaces()->begin();
                bool is_online = false;
                while(itWp!=itq->second->wplaces()->end())  { if(itWp->second->current_status()!=_eq::_wplace::offline) {is_online = true; break;} itWp++;}
                if(is_online==false) {itq++; continue;} //Если все подключённые окна ofline

                item = new QStandardItem(QString::fromLocal8Bit(itq->second->name().c_str()));
            }

            if(item)
            {
                item->setData(QVariant::fromValue(qdata));
                rows << (QList<QStandardItem*>() << item);
            }

            itq++;
        }

        srv->unlock(); // set interface free to allow send alive signals

        qSort(rows.begin(), rows.end(), q_list_less<QList<QStandardItem*> >());

        // show selection dialog

        log << "...show queues' list..." << endl;
        _list_selection_dlg dlg(QStringList() << "очереди", /*qlist*/rows, 400, 100, 400, 527, this);
        dlg.show();
        dlg.exec();
        QVariant qdata = dlg.get_sel_item_data();
        if(qdata.isNull())
        {
            log << "...user canceled queue selection." << endl;
            return;
        }

        SRV_CHECK_STATUS_AND_LOCK

        // get selected queue id

        _eq::_q* q = srv->my_office()->qs()->find(_eq::_id(qdata.value<lstq>().id.toStdString()))->second;

        string sbuf;
        log << "...selected queue id = " << q->id().to_string(sbuf) << " ..." << endl;

//        string ticket_num = tip->number();
//        _time ticket_time = tip->create_time();
//crash - ???

        // reopen the ticket in selected queue

        log << "...creating ticket..." << endl;

        _eq::_tickets::iterator itt;
        _eq_err::_code err;
        int wait_time;
        err = q->create_ticket(itt, srv->my_wplace()->current_ticket(), false, &wait_time);
        if(err != _eq_err::ok)
        {
            if(err == _eq_err::create_ticket_q_on_rest)
            {
                srv->unlock();
                log << "...do_return_ticket_to_queue aborted - the queue is out of worktime." << endl;
                err_msg(QMessageBox::Warning, "Указанная очередь завершила работу.\nОперация отменена.");
                return;
            }
            else
            {
                err_msg(QMessageBox::Critical);
                THROW_EXCEPTION(_eq_err::_server_answ[err]);
            }
        }
        else
        {
            // close the ticket in current queue/wplace

            log << "...closing the ticket..." << endl;

            _eq_err::_code err = srv->my_wplace()->close_proc_ticket(_eq::_close_ticket_back_to_q, 0);
            if(err != _eq_err::ok)
            {
                err_msg(QMessageBox::Critical);
                THROW_EXCEPTION(_eq_err::_server_answ[err]);
            }
        }

        srv->unlock();

        log << "...reflecting state change in UI..." << endl;
        on_ticket_close();

        log << "...do_return_ticket_to_queue done." << endl;
    }
    catch(_rdg_exception& e)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Ticket forwarding command was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
    }
    catch(...)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Ticket forwarding command was failed by unknown reason." << endl;
    }
}

void _main_win::on_btn_create_new_ticket_clicked()
{
    _log log;
    log << LOG_UI(10) << "do_create_new_ticket_to_queue..." << endl;

    try
    {
        SRV_CHECK_STATUS_AND_LOCK

        _eq::_ticket_in_proc* tip = srv->my_wplace()->current_ticket();
        if(!tip)
        {
            srv->unlock();
            err_msg(QMessageBox::Warning, "Нет открытого талона.");
            log << "...do_return_ticket_to_queue aborted - no opened ticket." << endl;
            return;
        }
        string ticket_num = tip->number();
        //_time ticket_time = tip->create_time();
        _time ticket_put_time = tip->put_to_q_time();
        log << "...ticket number = " << ticket_num << " ..." << endl;

        // prepare queues list

        QList<QList<QStandardItem*> > rows;
        char tmps[64];
        _eq::_qs::const_iterator itq = srv->my_office()->qs()->begin();

        while(itq != srv->my_office()->qs()->end())
        {
            lstq qdata;
            qdata.snum = itq->second->short_number();
            qdata.id = itq->second->id().to_c_string(tmps);
            qdata.at_wp = itq->second->name()[0] == '@';
            QStandardItem* item = NULL;

            if(qdata.at_wp) { // skip workplace queue
                itq++;
                continue;
            } else  {
                // oper can not redir to q with no one work place assigment
                if (itq->second->wplaces()->empty())
                {
                    itq++;
                    continue;
                }
                // Предыдущий if принимал и отключённые рабочие места. Исправим
                _eq::_wplaces::const_iterator itWp = itq->second->wplaces()->begin();
                bool is_online = false;
                while(itWp!=itq->second->wplaces()->end())  { if(itWp->second->current_status()!=_eq::_wplace::offline) {is_online = true; break;} itWp++;}
                if(is_online==false) {itq++; continue;} //Если все подключённые окна ofline

                item = new QStandardItem(QString::fromLocal8Bit(itq->second->name().c_str()));
            }

            if(item)
            {
                item->setData(QVariant::fromValue(qdata));
                rows << (QList<QStandardItem*>() << item);
            }

            itq++;
        }

        srv->unlock(); // set interface free to allow send alive signals

        qSort(rows.begin(), rows.end(), q_list_less<QList<QStandardItem*> >());

        // show selection dialog

        log << "...show queues' list..." << endl;
        _list_selection_dlg dlg(QStringList() << "очереди", /*qlist*/rows, 400, 100, 400, 527, this);
        dlg.show();
        dlg.exec();
        QVariant qdata = dlg.get_sel_item_data();
        if(qdata.isNull())
        {
            log << "...user canceled queue selection." << endl;
            return;
        }

        SRV_CHECK_STATUS_AND_LOCK

        // get selected queue id

        _eq::_q* q = srv->my_office()->qs()->find(_eq::_id(qdata.value<lstq>().id.toStdString()))->second;

        string sbuf;
        log << "...selected queue id = " << q->id().to_string(sbuf) << " ..." << endl;

//        string ticket_num = tip->number();
//        _time ticket_time = tip->create_time();
//crash - ???

        // reopen the ticket in selected queue

        log << "...creating ticket..." << endl;

        _eq::_tickets::iterator itt;
        _eq::_qs_priors::const_iterator itqp = srv->my_office()->default_qs_prior();
        _eq_err::_code err;
        int wait_time;
        err = q->create_ticket(itt, itqp, &wait_time);
        if(err != _eq_err::ok)
        {
            if(err == _eq_err::create_ticket_q_on_rest)
            {
                srv->unlock();
                log << "...do_return_ticket_to_queue aborted - the queue is out of worktime." << endl;
                err_msg(QMessageBox::Warning, "Указанная очередь завершила работу.\nОперация отменена.");
                return;
            }
            else
            {
                err_msg(QMessageBox::Critical);
                THROW_EXCEPTION(_eq_err::_server_answ[err]);
            }

        }
//        else
//        {
//            // close the ticket in current queue/wplace

//            log << "...closing the ticket..." << endl;

//            _eq_err::_code err = srv->my_wplace()->close_proc_ticket(_eq::_close_ticket_back_to_q, 0);
//            if(err != _eq_err::ok)
//            {
//                err_msg(QMessageBox::Critical);
//                THROW_EXCEPTION(_eq_err::_server_answ[err]);
//            }
//        }

        srv->unlock();
        printticket(QString::fromStdString(q->vname()),QString::fromStdString((*itt).second->number()));
        log << "...reflecting state change in UI..." << endl;
        //on_ticket_close();

        log << "...do_return_ticket_to_queue done." << endl;
    }
    catch(_rdg_exception& e)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Ticket forwarding command was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
    }
    catch(...)
    {
        srv->unlock();
        _log log;
        log << LOG_COMMON(0) << "Ticket forwarding command was failed by unknown reason." << endl;
    }

//    try
//    {
//        json jCommand;
//        jCommand["method"] = "new";
//        jCommand["command"] = "qCreateTicket";
//        jCommand["qId"] = srv->my_wplace()->current_ticket()->
//        jCommand["qsPriorId"] = srv->my_wplace()->current_ticket()->qs_prior();
//        std::string res;
//        srv->cmd_to_server(jCommand.dump(),res);

//        //log << LOG_COMMON(10) << jCommand.dump() << endl;

//        log << LOG_COMMON(10) << res << endl;
//    }
//    catch (json::exception &e)
//    {
//        log << LOG_COMMON(10) << "AudioRecorder sendFile json error" << endl;
//        return false;
//    }
}
