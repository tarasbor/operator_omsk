#include "hw_drv.hpp"
#include <clthread.hpp>
#include <rs_iface.hpp>

#ifdef SVT_EMU
#include <ogl_gui.hpp>
#endif

#ifdef DEBUG
    #include <QDebug>
#endif

using namespace std;

// ------------------------------------------------------------------------
// SVETOVOD DEGITAL LED DISPLAY
// ------------------------------------------------------------------------
class _svt_dig_led_display_drv:public _led_display_drv
{
	protected:
        _rs::_iface *iface;
        unsigned char cm_number;
		
	public:
		_svt_dig_led_display_drv(const string &port_name,unsigned char number);
		virtual ~_svt_dig_led_display_drv();
		
		virtual bool display(const string &text);
		//virtual bool play_sound(const string &snd);
		
		virtual void release() { delete this; };
};

// ------------------------------------------------------------------------
_svt_dig_led_display_drv::_svt_dig_led_display_drv(const string &port_name,unsigned char number)
:iface(NULL),cm_number(number)
{
    iface = _rs::create_rs_iface(port_name);
}

// ------------------------------------------------------------------------
_svt_dig_led_display_drv::~_svt_dig_led_display_drv()
{
    if (iface)
    {
        delete iface;
        iface=NULL;
    }
}

#define SEG_MASK_A		(1)
#define SEG_MASK_B		(1<<1)
#define SEG_MASK_C		(1<<2)
#define SEG_MASK_D		(1<<3)
#define SEG_MASK_E		(1<<4)
#define SEG_MASK_F		(1<<5)
#define SEG_MASK_G		(1<<6)
#define SEG_MASK_DP		(1<<7)

// ------------------------------------------------------------------------
bool _svt_dig_led_display_drv::display(const string &rtext)
{
	string text;
	
	int i;
	for(i=0;i<rtext.length();i++)
	{
		unsigned char c;
		if      (rtext[i]=='1') c=(SEG_MASK_B | SEG_MASK_C);
		else if (rtext[i]=='2') c=(SEG_MASK_A | SEG_MASK_B | SEG_MASK_G | SEG_MASK_E | SEG_MASK_D);
		else if (rtext[i]=='3') c=(SEG_MASK_A | SEG_MASK_B | SEG_MASK_G | SEG_MASK_C | SEG_MASK_D);
		else if (rtext[i]=='4') c=(SEG_MASK_F | SEG_MASK_B | SEG_MASK_G | SEG_MASK_C);
		else if (rtext[i]=='5') c=(SEG_MASK_A | SEG_MASK_F | SEG_MASK_G | SEG_MASK_C | SEG_MASK_D);
		else if (rtext[i]=='6') c=(SEG_MASK_A | SEG_MASK_F | SEG_MASK_E | SEG_MASK_G | SEG_MASK_C | SEG_MASK_D);
		else if (rtext[i]=='7') c=(SEG_MASK_A | SEG_MASK_B | SEG_MASK_C);
		else if (rtext[i]=='8') c=(SEG_MASK_A | SEG_MASK_B | SEG_MASK_C | SEG_MASK_D | SEG_MASK_E | SEG_MASK_F | SEG_MASK_G);
		else if (rtext[i]=='9') c=(SEG_MASK_A | SEG_MASK_B | SEG_MASK_C | SEG_MASK_D | SEG_MASK_F | SEG_MASK_G);
		else if (rtext[i]=='0') c=(SEG_MASK_A | SEG_MASK_B | SEG_MASK_C | SEG_MASK_D | SEG_MASK_E | SEG_MASK_F);
		else if (rtext[i]=='-') c=(SEG_MASK_G);
		else if (rtext[i]=='.' && i>0) { text[text.length()-1]|=SEG_MASK_DP; continue; }
		else c=0x00;
		
		text.append((char *)&c,1);
	}	
	
    unsigned char *cmd = new unsigned char [text.length()+8+1];
    cmd[0]=0xE0;
    cmd[1]=0x00;
    cmd[2]=cm_number;
    cmd[3]=0x00; // cmd
    cmd[4]=0x00; // 
    cmd[5]=text.length(); // data len - 33

    
    int crc=0;
    for(i=0;i<6;i++) crc+=(int)cmd[i];

    unsigned char l_crc=(crc & 0xFF);
    unsigned char h_crc=((crc & 0xFF00) >> 8);

    cmd[6]=h_crc+l_crc;
    cmd[7]=l_crc;
    
    unsigned char d_crc=0;
    for(i=0;i<text.length();i++)
    {
        cmd[i+8]=text[i];
        d_crc+=text[i];
    }    

    cmd[text.length()+8]=d_crc;

    iface->put_s(string((char *)cmd,text.length()+8+1));

    string rez;
    
    Sleep(100);
    iface->get_s(rez,500);
    if (rez.empty()) return false;
    
    return true;
}

/*
// ------------------------------------------------------------------------
bool _svt_dig_led_display_drv::play_sound(const string &rtext)
{
	string text=rtext;
	
    unsigned char *cmd = new unsigned char [text.length()+8+1];
    cmd[0]=0xE0;
    cmd[1]=0x00;
    cmd[2]=cm_number;
    cmd[3]=0x50; // cmd
    cmd[4]=0x00; // 
    cmd[5]=text.length(); // data len - 33

	int i;
    int crc=0;
    for(i=0;i<6;i++) crc+=(int)cmd[i];

    unsigned char l_crc=(crc & 0xFF);
    unsigned char h_crc=((crc & 0xFF00) >> 8);

    cmd[6]=h_crc+l_crc;
    cmd[7]=l_crc;
    
    unsigned char d_crc=0;
    for(i=0;i<text.length();i++)
    {
        cmd[i+8]=text[i];
        d_crc+=text[i];
    }    

    cmd[text.length()+8]=d_crc;

    iface->put_s(string((char *)cmd,text.length()+8+1));

    string rez;
    
    Sleep(100);
    iface->get_s(rez,500);
    if (rez.empty()) return false;
    
    return true;
}
*/

// ------------------------------------------------------------------------
_led_display_drv *create_svt_dig_led_display(const string &port_name,unsigned char disp_number)
{
	return new _svt_dig_led_display_drv(port_name,disp_number);
}

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------


// ------------------------------------------------------------------------
// SVETPVOD (GENIUS) CONTROL PANEL
// ------------------------------------------------------------------------
class _svt_console_drv:public _console_drv
{
    protected:
        
        _rs::_iface *iface;
        unsigned char cm_number;
		
    public:
        
        _svt_console_drv(const string &port_name,unsigned char number);
        virtual ~_svt_console_drv();
        
        virtual bool display(const string &text,unsigned int cursor_pos=0xFF);
        virtual bool kbd_buffer(string &buffer);
		
		virtual void release() { delete this; };
};

// ------------------------------------------------------------------------
_svt_console_drv::_svt_console_drv(const string &port_name,unsigned char number):iface(NULL),cm_number(number)
{
    iface = _rs::create_rs_iface(port_name);
}

// ------------------------------------------------------------------------
_svt_console_drv::~_svt_console_drv()
{
    if (iface)
    {
        delete iface;
        iface=NULL;
    }
}

// ------------------------------------------------------------------------
bool _svt_console_drv::display(const string &text,unsigned int cursor_pos)
{
    unsigned char cmd[32+8+1+1];
    cmd[0]=0xE0;
    cmd[1]=0x00;
    cmd[2]=cm_number;
    cmd[3]=0x00; // cmd
    cmd[4]=0x00; // 
    cmd[5]=0x21; // data len - 33

    int i;
    int crc=0;
    for(i=0;i<6;i++) crc+=(int)cmd[i];

    unsigned char l_crc=(crc & 0xFF);
    unsigned char h_crc=((crc & 0xFF00) >> 8);

    cmd[6]=h_crc+l_crc;
    cmd[7]=l_crc;

    char text_tmp[32];
    for(i=0;i<32 && i<text.length();i++) text_tmp[i]=text[i];
    for(;i<32;i++) text_tmp[i]=0x00;

    unsigned char d_crc=0;
    int len=32;
    for(i=0;i<32;i++)
    {
        cmd[i+8]=text_tmp[i];
        d_crc+=text_tmp[i];
    }    

    unsigned char cp=cursor_pos;
    cmd[32+8]=cp;
    d_crc+=cp;
    cmd[32+8+1]=d_crc;

    iface->put_s(string((char *)cmd,32+8+1+1));

    string rez;
    
    Sleep(100);
    iface->get_s(rez,500);
    if (rez.empty()) return false;
    
    return true;
}

// ------------------------------------------------------------------------
bool _svt_console_drv::kbd_buffer(string &buffer)
{
    buffer.clear();
    
    unsigned char cmd[8];
    cmd[0]=0xE0;
    cmd[1]=0x00;
    cmd[2]=cm_number;
    cmd[3]=0x60; // cmd
    cmd[4]=0x00; // 
    cmd[5]=0x16; // data len - 

    int i;
    int crc=0;
    for(i=0;i<6;i++) crc+=(int)cmd[i];

    unsigned char l_crc=(crc & 0xFF);
    unsigned char h_crc=((crc & 0xFF00) >> 8);

    cmd[6]=h_crc+l_crc;
    cmd[7]=l_crc;

    iface->put_s(string((char *)cmd,8));

    string rez;
    
    Sleep(100);
    iface->get_s(rez,500);
    if (rez.empty()) return false;
    
    int bpos=rez.find((char)0xE0);
    if (bpos==string::npos) return false;
    if (bpos+8>rez.length()) return false;
    
    bpos+=1;
    if (cm_number!=rez[bpos]) return false;
    
    bpos+=3;
    int kcount=(int)(rez[bpos]<<8)+rez[bpos+1];
    
    bpos+=4;
    if (kcount+bpos>rez.length()) return false;
    
    buffer.append(rez,bpos,kcount);    
    
    return true;
}

// ------------------------------------------------------------------------
_console_drv *create_svt_console(const string &port_name,unsigned char panel_number)
{
	return new _svt_console_drv(port_name,panel_number);
}


// -----------------------------------------------------------------------------
// ------------------------------------------------------------------------
// SVETPVOD (GENIUS) EMULATE CONTROL PANEL
// ------------------------------------------------------------------------
#ifdef SVT_EMU
class _svt_emu_wnd:public _window
{
	private:
		
        string l1;
        string l2;
        
        float s;
        
        
        
	public:
		_svt_emu_wnd();
		virtual ~_svt_emu_wnd();
		
		virtual void draw();
        virtual void display(const string &text,int cpos);
        
        static void key_f(unsigned char key, int x, int y);
        static void skey_f(int key, int x, int y);
        
        static string kbd;
};

string _svt_emu_wnd::kbd;

// ------------------------------------
void _svt_emu_wnd::key_f(unsigned char key, int x, int y )
{
    key-=0x30;
    if      (key==0) kbd.append(1,0x10);
    else if (key==1) kbd.append(1,0x0C);
    else if (key==2) kbd.append(1,0x0D);
    else if (key==3) kbd.append(1,0x0E);
    else if (key==4) kbd.append(1,0x09);
    else if (key==5) kbd.append(1,0x0A);
    else if (key==6) kbd.append(1,0x0B);
    else if (key==7) kbd.append(1,0x05);
    else if (key==8) kbd.append(1,0x06);
    else if (key==9) kbd.append(1,0x07);
}

// ------------------------------------
void _svt_emu_wnd::skey_f(int key, int x, int y )
{
    if      (key==0x01) kbd.append(1,0x01);
    else if (key==0x02) kbd.append(1,0x02);
    else if (key==0x03) kbd.append(1,0x03);
    else if (key==0x04) kbd.append(1,0x04);
    else if (key==0x05) kbd.append(1,0x08);
    else if (key==0x06) kbd.append(1,0x12);
    else if (key==0x07) kbd.append(1,0x0F);
    else if (key==0x08) kbd.append(1,0x11);
}

// ------------------------------------
_svt_emu_wnd::_svt_emu_wnd():_window(0,"",0,0,200,200,false)
{
    s=0.0;
    glutSpecialFunc(skey_f);
    glutKeyboardFunc(key_f);
}

// ------------------------------------
_svt_emu_wnd::~_svt_emu_wnd()
{
}

// ------------------------------------
void _svt_emu_wnd::draw()
{
	glClearColor(0.0,0.0,0.0,1.0);
    glClear(GL_COLOR_BUFFER_BIT);
	    
    int ww=glutGet(GLUT_WINDOW_WIDTH);
    int wh=glutGet(GLUT_WINDOW_HEIGHT);

    glViewport(0,0,ww,wh);
    
    float dy=0.10;
		
	int i=0;
			
	/*glColor3ub(0xFF,0xFF,0xFF);
	glRasterPos2f(0,0);
	glListBase(1000);
	glCallLists (l1.length(), GL_UNSIGNED_BYTE, l1.c_str());
     */ 
    
    float pos=-0.9;
    for(i=0;i<l1.length();i++)
    {
        glColor3ub(0xFF,0xFF,0xFF);
        glRasterPos2f(pos,0);	
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13,l1[i]);
        pos+=(float)glutBitmapWidth(GLUT_BITMAP_8_BY_13,l1[i])/ww*2;
    }
    
    pos=-0.9;
    for(i=0;i<l2.length();i++)
    {
        glColor3ub(0xFF,0xFF,0xFF);
        glRasterPos2f(pos,-0.1);	
        glutBitmapCharacter(GLUT_BITMAP_8_BY_13,l2[i]);
        pos+=(float)glutBitmapWidth(GLUT_BITMAP_8_BY_13,l2[i])/ww*2;
    }
	
    glutSwapBuffers();    
    
    s+=0.1;
    if (s>1.0) s=0;
}

// ------------------------------------------------------------------------
void _svt_emu_wnd::display(const string &text,int cpos)
{
    string s=text;
    l1.assign(s,0,16);
    if (s.length()>16) l2.assign(s,16,16); else l2="";
    
    //_svt_emu_wnd::draw();
    glutPostRedisplay();
}

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
class _svt_console_emu_drv:public _console_drv
{
    protected:
        _svt_emu_wnd *wnd;
    public:
        
        _svt_console_emu_drv(const string &port_name,unsigned char number);
        virtual ~_svt_console_emu_drv();
        
        virtual bool display(const string &text,unsigned int cursor_pos=0xFF);
        virtual bool kbd_buffer(string &buffer);
		
		virtual void release() { delete this; };
};

// ------------------------------------------------------------------------
_svt_console_emu_drv::_svt_console_emu_drv(const string &port_name,unsigned char number):wnd(NULL)
{    
	wnd = new _svt_emu_wnd;
}

// ------------------------------------------------------------------------
_svt_console_emu_drv::~_svt_console_emu_drv()
{
    delete wnd;
}

// ------------------------------------------------------------------------
bool _svt_console_emu_drv::display(const string &text,unsigned int cursor_pos)
{
    wnd->display(text,cursor_pos);
    
    return true;
}

// ------------------------------------------------------------------------
bool _svt_console_emu_drv::kbd_buffer(string &buffer)
{
    buffer=_svt_emu_wnd::kbd;
    _svt_emu_wnd::kbd="";
    return true;
}

// ------------------------------------------------------------------------
_console_drv *create_svt_emu_console(const string &port_name,unsigned char panel_number)
{
	return new _svt_console_emu_drv(port_name,panel_number);
}

#endif
