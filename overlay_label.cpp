#include "overlay_label.hpp"
#include <QCoreApplication>

#ifdef DEBUG
    #include <QDebug>
#endif

_overlay_label::_overlay_label(QWidget *parent) :
    QLabel(parent), m_buddy(NULL)
{
}

void _overlay_label::focusInEvent(QFocusEvent* event)
{
    QLabel::focusInEvent(event);
    emit onFocusIn();
}

void _overlay_label::mousePressEvent(QMouseEvent* event)
{
    QLabel::mousePressEvent(event);
    if(m_buddy)
        //QCoreApplication::postEvent(m_buddy, event);
        QCoreApplication::sendEvent(m_buddy, (QEvent*)event);
}

void _overlay_label::mouseReleaseEvent(QMouseEvent* event)
{
    QLabel::mouseReleaseEvent(event);
    if(m_buddy)
        //QCoreApplication::postEvent(m_buddy, event);
        QCoreApplication::sendEvent(m_buddy, (QEvent*)event);
}
