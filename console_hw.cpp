//#include <cstdlib>
#include "console_hw.hpp"
#include <hw_drv.hpp>
#include <eq_net_client_rmo.hpp>
#include <codepage.hpp>

#include "text_help.hpp"

#ifdef SVT_EMU
#include <freeglut.h>
#endif

#include <rdg_log.hpp>
#ifdef DEBUG
    #include <QDebug>
#endif

using namespace std;

#define MLOG(A) _log() << LOG_CONSOLE_HW(A)

#define HIDE_DISP_POS 17

void console_error_text_by_num(_eq_err::_code err,string &s)
{
    s="Неизвестная ошибка.";

    if      (err==_eq_err::ok) s="Ок";
    else if (err==_eq_err::unknown) s="Неизвестная ошибка.";
    else if (err==_eq_err::broken_line) s="Ошибка\nОбрыв связи";
    else if (err==_eq_err::proto_unknown_cmd) s="Ошибка на сервере";
    else if (err==_eq_err:: proto_unknown_format) s="Ошибка на сервере";
    else if (err==_eq_err::proto_login_first) s="Ошибка\nСбой регистрации";
    else if (err==_eq_err::proto_exit_fail) s="Ошибка\nСбой регистрации";
    else if (err==_eq_err::proto_incorrect_params) s="Ошибка на сервере";
    else if (err==_eq_err::proto_login_fail) s="Ошибка\nСбой регистрации";
    else if (err==_eq_err::common_no_given_wp) s="Ошибка\nНеверное р.м.";
    else if (err==_eq_err::common_no_given_oper) s="Ошибка\nНеверный опер.";
    else if (err==_eq_err::common_no_given_q) s="Ошибка\nНеверная очередь";
    else if (err==_eq_err::common_no_given_ticket) s="Ошибка\nТалона нет.";
    else if (err==_eq_err::common_wrong_state) s="Ошибка\nНеверный статус";
    else if (err==_eq_err::reg_oper_given_wp_busy) s="Р.м. уже занято";
    else if (err==_eq_err::reg_oper_given_oper_busy) s="Оператор уже занят";
    else if (err==_eq_err::start_proc_ticket_wp_in_proc) s="Ошибка\nТалон уже открыт";
    else if (err==_eq_err::start_proc_ticket_wp_no_oper) s="Рабочее место пусто";
    else if (err==_eq_err::create_ticket_wrong_param) s="Ошибка создание талона";
    else if (err==_eq_err::create_ticket_q_on_rest) s="Очередь закончила работу.";
}

// -----------------------------------------------------------------------------
_console_hw::_console_hw(_eq_net_client_rmo *r):dev(NULL),cm_timer(NULL),c_status(_eq::_wplace::offline),rmo(r)
{
    /*
    ofstream f;
    f.open("c:\\alfa-m\\123.txt",ios::out|ios::binary);
    std::string s="Тест строки";
    _codepage::utf8_to_win1251(s);

    f.write(s.data(),11);
    f.close();
    */
}

// -----------------------------------------------------------------------------
_console_hw::~_console_hw()
{
    if (cm_timer)
    {
        cm_timer->Kill();
        while(cm_timer->GetStatus()!=TH_STATUS_KILLED) Sleep(100);
        delete cm_timer;
        cm_timer=NULL;
    }
    
    if (dev)
    {
        dev->release();
        dev=NULL;
    }
}

// -----------------------------------------------------------------------------
void _console_hw::init(const string &params)
{
    int n=0;
    while(1)
    {
        string pstr=_text_help::get_field_from_st(params,',',n);
        if (pstr.empty()) break;
        
        string par=_text_help::get_field_from_st(pstr,'=',0);
        string val=_text_help::get_field_from_st(pstr,'=',1);
        
        if (par=="hw_port") cm_hw_port_name=val;
        else if (par=="hw_number") cm_dev_num=atoi(val.c_str());
        
        n++;
    }
    
    if (cm_hw_port_name.empty() || cm_dev_num<=0 || cm_dev_num>=0xFF)
    {
        MLOG(0) << "_console_hw::init - params not set/incorrect" << endl;
        return;
    }
    
    try
    {
#ifndef SVT_EMU        
        _rs::_iface *iface=_rs::create_rs_iface(cm_hw_port_name);
#ifndef WINDOWS
        if (!iface->is_params_set()) iface->set_params(_rs::b115200,_rs::no_parity,8,_rs::stop_bits_1);
#else
        //TODO ???
#endif
        dev=create_svt_console(cm_hw_port_name,cm_dev_num);
        delete iface;
#else
        dev=create_svt_emu_console(cm_hw_port_name,cm_dev_num);
#endif
    }
    catch(_rs::_error &e)
    {
        MLOG(0) << "_console_hw::init - RS error :" << e.err_str() << endl;
    }
        
    set_status(_eq::_wplace::offline);
    
    cm_timer=new _console_hw_timer(this);
    cm_timer->SetSleepTime(300);
    cm_timer->Start();
}

// -----------------------------------------------------------------------------
int _console_hw::on_timer()
{
    lock.lock();
    
    rmo->lock();
    _eq_net_client::_client_status cs=rmo->status();
    rmo->unlock();

    if (cs!=_eq_net_client::client_status_ok)
    {
        set_status(_eq::_wplace::offline);
        lock.unlock();
        return 0;
    }
    
    rmo->lock();
    if (rmo_last_update!=rmo->my_office()->last_any_change_time())
    {
        rmo_last_update=rmo->my_office()->last_any_change_time();
        _eq::_wplace::_status new_s=rmo->my_wplace()->current_status();
        if (new_s!=c_status)
        {
            set_status(new_s);
            rmo->unlock();
            lock.unlock();
            return 0;
        }
    }
    rmo->unlock();
    
    try
    {
        // at firts get keyboard buffer
        string kb;
        if (!dev->kbd_buffer(kb))
        {
            // TODO - error handling
        }
        
        // and proccess it
        if (!kb.empty()) proccess_keys(kb);
        
        // and out display buffer
        if (!dev->display(cm_disp_buffer+cm_answer_disp_buffer,cm_disp_cpos))
        {
            // TODO - error handling
        }
        
    }
    catch(_rs::_error &e)
    {
        MLOG(0) << "_console_hw::on_timer - RS error : " << e.err_str() << endl;
    }
    
    lock.unlock();
    
    //MLOG(0) << "timer" << endl;
    
    return 0;
}

// -----------------------------------------------------------------------------
void _console_hw::proccess_keys(const string &kb)
{    
    if (c_status==_eq::_wplace::offline) return;
    
    MLOG(0) << "kb have data" << endl;
    int i;
    char add='\0';
    for(i=0;i<kb.length();i++)
    {
        if      (kb[i]==0x01) { cm_answer.append(1,0x01); add=0x01; }
        else if (kb[i]==0x02) { cm_answer.append(1,0x02); add=0x02; }
        else if (kb[i]==0x03) { cm_answer.append(1,0x03); add=0x03; }
        else if (kb[i]==0x04) { cm_answer.append(1,0x04); add=0x04; }
        else if (kb[i]==0x05) { cm_answer.append(1,'7'); add='7'; }
        else if (kb[i]==0x06) { cm_answer.append(1,'8'); add='8'; }
        else if (kb[i]==0x07) { cm_answer.append(1,'9'); add='9'; }
        else if (kb[i]==0x08) { cm_answer.append(1,0x05); add=0x05; }
        else if (kb[i]==0x09) { cm_answer.append(1,'4'); add='4'; }
        else if (kb[i]==0x0A) { cm_answer.append(1,'5'); add='5'; }
        else if (kb[i]==0x0B) { cm_answer.append(1,'6'); add='6'; }
        else if (kb[i]==0x0C) { cm_answer.append(1,'1'); add='1'; }
        else if (kb[i]==0x0D) { cm_answer.append(1,'2'); add='2'; }
        else if (kb[i]==0x0E) { cm_answer.append(1,'3'); add='3'; }
        else if (kb[i]==0x0F) { cm_answer.append(1,0x07); add=0x07; }
        else if (kb[i]==0x10) { cm_answer.append(1,'0'); add='0'; }
        else if (kb[i]==0x11) { cm_answer.append(1,0x08); add=0x08; }
        else if (kb[i]==0x12) { cm_answer.append(1,0x06); add=0x06; }
        
        MLOG(0) << std::hex << (int) kb[i] << " ";
    }
    
    MLOG(0) << endl;
    MLOG(0) << "add=" << std::hex << (int)add << endl;
    
    if (c_status==_eq::_wplace::pause)
    {
        if (add==0x08)
        {
            rmo->lock();
            rmo->my_wplace()->continue_work();
            rmo->unlock();
        }
    }
    else if (c_status==_eq::_wplace::wait)
    {
        if (add==0x08)
        {
            rmo->lock();
            rmo->my_wplace()->pause_work();
            rmo->unlock();
        }
        else if (add==0x01)
        {
            cm_answer_disp_buffer.clear();
            show("Вызов\nОчередь №");
            cm_sub_state=ss_need_qn;
        }
        else if (add==0x02)
        {
            cm_answer_disp_buffer.clear();
            show("Вызов по №");
            cm_sub_state=ss_need_tn;
        }
        else if (add==0x06)
        {
            c_status=_eq::_wplace::offline;
            set_status(_eq::_wplace::wait);
        }
        else if (add>='0' && add<='9')
        {
            if (cm_sub_state==ss_need_qn || 
                cm_sub_state==ss_need_tn)
            {
                cm_answer_disp_buffer.append(1,add);
            }
        }
        else if (add==0x07 && !cm_answer_disp_buffer.empty())
        {
            if (cm_sub_state==ss_need_qn)
            {
                call_next(atoi(cm_answer_disp_buffer.c_str()));
            }
            else if (cm_sub_state==ss_need_tn)
            {
                call_by_number(atoi(cm_answer_disp_buffer.c_str()));
            }            
        }
    }
    else if (c_status==_eq::_wplace::proc)
    {
        if (add==0x06)
        {
            if (cm_sub_state==ss_none)
            {
                abort_ticket();
            }
            else
            {
                c_status=_eq::_wplace::offline;
                set_status(_eq::_wplace::proc);
            }
        }
        else if (add==0x05)
        {
            cm_answer_disp_buffer.clear();
            show("Решено\nвопросов?");
            cm_sub_state=ss_need_ac;
        }
        else if (add==0x03)
        {
            cm_answer_disp_buffer.clear();
            show("В очередь №");
            cm_sub_state=ss_need_qn;
        }
        else if (add==0x04)
        {
            close_ticket(1);
        }
        else if (add>='0' && add<='9')
        {
            if (cm_sub_state==ss_need_qn || 
                cm_sub_state==ss_need_ac)
            {
                cm_answer_disp_buffer.append(1,add);
            }
        }
        else if (add==0x07 && !cm_answer_disp_buffer.empty())
        {
            if (cm_sub_state==ss_need_qn)
            {
                to_q(atoi(cm_answer_disp_buffer.c_str()));
            }
            else if (cm_sub_state==ss_need_ac)
            {
                close_ticket(atoi(cm_answer_disp_buffer.c_str()));
            }            
        }
    }    
}

// -----------------------------------------------------------------------------
void _console_hw::format_for_disp(const string &text)
{    
    cm_disp_buffer="";
    
    int i;
    for(i=0;i<text.length();i++)
    {
        if (text[i]=='\n')
        {
            int n=16-cm_disp_buffer.length();
            if (n>0) cm_disp_buffer.append(n,' ');
            continue;
        }
        cm_disp_buffer.append(1,text[i]);
    }
}

// -----------------------------------------------------------------------------
void _console_hw::show(const string &text)
{
    lock.lock();
    
    cm_work_mode=wm_show;
    string lcp_text=text;
    _codepage::utf8_to_win1251(lcp_text);
    format_for_disp(lcp_text);
    cm_answer_disp_buffer="";
    cm_answer="";
    cm_disp_cpos=HIDE_DISP_POS;
    
    lock.unlock();
}

// -----------------------------------------------------------------------------
void _console_hw::ask(const string &qtext,const string &atext)
{
    lock.lock();
    
    cm_work_mode=wm_ask;
    format_for_disp(qtext);
    cm_answer_disp_buffer=atext;
    cm_answer=atext;
    cm_disp_cpos=cm_disp_buffer.length();
    
    lock.unlock();
}

// -----------------------------------------------------------------------------
void _console_hw::set_status(_eq::_wplace::_status new_s)
{
    if (new_s==c_status) return;
    
    cm_sub_state=ss_none;
    
    clear_answer();
    if (new_s==_eq::_wplace::wait)
    {
        show("Вызов");
    }
    else if (new_s==_eq::_wplace::offline)
    {
        show("Ожидается\nрегистрация...");
    }
    else if (new_s==_eq::_wplace::pause)
    {
        show("Перерыв...");
    }
    else if (new_s==_eq::_wplace::proc)
    {
        show("Обслуживание");
    }
                
    c_status=new_s;
}

// -----------------------------------------------------------------------------
void _console_hw::call_next(int qn)
{
    rmo->lock();
    
    _eq::_qs::const_iterator qit=rmo->my_office()->qs()->begin();
    while(qit!=rmo->my_office()->qs()->end())
    {
        if (qit->second->short_number()==qn) break;
        qit++;
    }    

    if (qit==rmo->my_office()->qs()->end())
    {
        char tmp_s[48];
        sprintf(tmp_s,"Очереди №%i нет.",qn);
        show(tmp_s);
        rmo->unlock();
        return;
    }

    _eq::_qs::const_iterator allow_qit=rmo->my_wplace()->qs()->begin();
    while(allow_qit!=rmo->my_wplace()->qs()->end())
    {
        if (allow_qit->second->id()==qit->second->id()) break;
        allow_qit++;
    }

    if (allow_qit==rmo->my_wplace()->qs()->end())
    {
        char tmp_s[48];
        sprintf(tmp_s,"Вызов из очереди №%i запрещен...",qn);
        show(tmp_s);
        rmo->unlock();
        return;
    }
    
    if (!qit->second->tickets()->size())
    {
        show("Нет талонов...");
        rmo->unlock();
        return;
    }
    
    _eq::_tickets::const_iterator tit=qit->second->tickets()->begin();
    _eq::_tickets::const_iterator oldest_tit=tit;
    while(tit!=qit->second->tickets()->end())
    {
        if (tit->second->create_time() < oldest_tit->second->create_time()) oldest_tit=tit;
        tit++;
    }
    
    _eq_err::_code err;
        
    err=rmo->my_wplace()->start_proc_ticket(oldest_tit->second);
    if (err!=_eq_err::ok)
    {
        string est;
        console_error_text_by_num(err,est);
        show(est);
        rmo->unlock();
        return;
    }
    err=qit->second->remove_ticket(oldest_tit);
    if (err!=_eq_err::ok)
    {
        string est;
        console_error_text_by_num(err,est);
        show(est);
        rmo->unlock();
        return;
    }    
        
    rmo->unlock();
}

// -----------------------------------------------------------------------------
void _console_hw::call_by_number(int tn)
{
    rmo->lock();
    
    _eq::_tickets::const_iterator tit;
    int ftn;
        
    _eq::_qs::const_iterator qit=rmo->my_office()->qs()->begin();
    while(qit!=rmo->my_office()->qs()->end())
    {
        tit=qit->second->tickets()->begin();
        while(tit!=qit->second->tickets()->end())
        {
            string number=tit->second->number();
            int i;
            
            for(i=0;i<number.length();i++)
                if (number[i]>='0' && number[i]<='9') break;
            
            ftn=atoi(number.c_str()+i);
            if (ftn==tn) break;
            tit++;
        }
        
        if (tit!=qit->second->tickets()->end()) break;
        qit++;
    }
    
    if (qit==rmo->my_office()->qs()->end())
    {        
        char tmp_s[48];
        sprintf(tmp_s,"Нет талона №%i",tn);
        show(tmp_s);
        rmo->unlock();
        return;
    }

    _eq::_qs::const_iterator allow_qit=rmo->my_wplace()->qs()->begin();
    while(allow_qit!=rmo->my_wplace()->qs()->end())
    {
        if (allow_qit->second->id()==qit->second->id()) break;
        allow_qit++;
    }

    if (allow_qit==rmo->my_wplace()->qs()->end())
    {
        char tmp_s[48];
        sprintf(tmp_s,"Вызов из очереди №%i запрещен...",qit->second->short_number());
        show(tmp_s);
        rmo->unlock();
        return;
    }
    
    _eq_err::_code err;
        
    err=rmo->my_wplace()->start_proc_ticket(tit->second);
    if (err!=_eq_err::ok)
    {
        string est;
        console_error_text_by_num(err,est);
        show(est);
        rmo->unlock();
        return;
    }
    err=qit->second->remove_ticket(tit);
    if (err!=_eq_err::ok)
    {
        string est;
        console_error_text_by_num(err,est);
        show(est);
        rmo->unlock();
        return;
    }    
        
    rmo->unlock();
}

// -----------------------------------------------------------------------------
void _console_hw::close_ticket(int ac)
{
    rmo->lock();
    
    _eq_err::_code err;
    
    err=rmo->my_wplace()->close_proc_ticket(_eq::_close_ticket_served,ac);
    if (err!=_eq_err::ok)
    {
        string est;
        console_error_text_by_num(err,est);
        show(est);
        rmo->unlock();
        return;
    }
        
    rmo->unlock();
}

// -----------------------------------------------------------------------------
void _console_hw::abort_ticket()
{
    rmo->lock();
    
    _eq_err::_code err;
    
    err=rmo->my_wplace()->close_proc_ticket(_eq::_close_ticket_oper_abort,1);
    if (err!=_eq_err::ok)
    {
        string est;
        console_error_text_by_num(err,est);
        show(est);
        rmo->unlock();
        return;
    }
        
    rmo->unlock();
}

// -----------------------------------------------------------------------------
void _console_hw::to_q(int qn)
{
    rmo->lock();
    
    _eq::_qs::const_iterator qit=rmo->my_office()->qs()->begin();
    while(qit!=rmo->my_office()->qs()->end())
    {
        if (qit->second->short_number()==qn) break;
        qit++;
    }

    if (qit==rmo->my_office()->qs()->end())
    {
        char tmp_s[48];
        sprintf(tmp_s,"Очереди №%i нет",qn);
        show(tmp_s);
        rmo->unlock();
        return;
    }
    
    _eq_err::_code err;
        
    _eq::_tickets::iterator tit;
    err=qit->second->create_ticket(tit,&rmo->my_wplace()->current_ticket()->number(),NULL);    
    if (err!=_eq_err::ok)
    {
        string est;
        console_error_text_by_num(err,est);
        show(est);
        rmo->unlock();
        return;
    }
    err=rmo->my_wplace()->close_proc_ticket(_eq::_close_ticket_back_to_q,1);
    if (err!=_eq_err::ok)
    {
        string est;
        console_error_text_by_num(err,est);
        show(est);
        rmo->unlock();
        return;
    }
        
    rmo->unlock();
}

// -----------------------------------------------------------------------------
int _console_hw_timer::Poll()
{
    return con->on_timer();
}
