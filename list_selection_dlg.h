#ifndef LIST_SELECTION_DLG_H
#define LIST_SELECTION_DLG_H

#include <QDialog>
#include <QStandardItemModel>

namespace Ui {
class _list_selection_dlg;
}

class _list_selection_dlg : public QDialog
{
    Q_OBJECT
    
    public:
        explicit _list_selection_dlg(const QStringList& headers, const QList<QList<QStandardItem*>/*QStringList*/>& rows,
                                     int x, int y, int w, int h, QWidget *parent = 0);
        ~_list_selection_dlg();
        const QVariant& get_sel_item_data() { return sel_item_data; }

    private slots:

        void on_btnClose_clicked();
        void on_btnOK_clicked();
        void on_btnCancel_clicked();

        void on_treeView_doubleClicked(const QModelIndex &index);

private:
        Ui::_list_selection_dlg *ui;
        QStandardItemModel model;
        QVariant sel_item_data;
};

#endif // LIST_SELECTION_DLG_H
