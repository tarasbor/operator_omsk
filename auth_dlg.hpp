#ifndef __AUTH_DLG_HPP__
#define __AUTH_DLG_HPP__

#include <QDialog>
#include <eq_net_client_rmo.hpp>

namespace Ui {
class _auth_dlg;
}

class _auth_dlg : public QDialog
{
    Q_OBJECT
    
public:
    explicit _auth_dlg(const _eq_net_client::_opers_info& opers_info, const QString& uname,
                       const _eq_net_client::_wplaces_info& wplaces_info, const QString& wplace,
                       bool is_brake = false,
                       QWidget* parent = 0);
    ~_auth_dlg();

    const QString& get_username() { return m_uname; }
    const QString& get_password() { return m_pwd; }
    const QString& get_workplace() { return m_wplace; }
    
private slots:
    void on_cmb_uname_currentIndexChanged(int index);
    void on_cmb_wplace_currentIndexChanged(int index);

    void on_btnOK_clicked();
    void on_btnCancel_clicked();
    void on_btnOK_pressed();
    void on_btnCancel_pressed();
    void on_btnOK_released();
    void on_btnCancel_released();

    void on_lblUname_focusIn();
    void on_cmb_uname_editTextChanged(const QString &arg1);
    void on_lblPwd_focusIn();
    void on_edt_pwd_textChanged(const QString &arg1);
    void on_lblWplace_focusIn();
    void on_cmb_wplace_editTextChanged(const QString &arg1);

private:
    Ui::_auth_dlg *ui;

    QString m_uname;
    QString m_pwd;
    QString m_wplace;
};

#endif
