#include <QtWidgets> //5.0
//#include <QPainter> //4.8
#include "combo_2line_delegate.hpp"

#ifdef DEBUG
    #include <QDebug>
#endif

void _combo_2line_delegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStringList text_parts = index.data(index.column()).toString().split("\r\n");

    QFont font;
    font.setFamily("Arial");

    font.setPointSize(10);
//    font.setCapitalization(QFont::AllUppercase);
    font.setBold(true);
    painter->setFont(font);
    painter->setPen(Qt::black);
    painter->drawText(option.rect, Qt::TextSingleLine|Qt::AlignLeft, text_parts[0]);

    font.setPointSize(9);
//    font.setCapitalization(QFont::);
    font.setBold(false);
    painter->setFont(font);
    //painter->setPen(Qt::darkGray);
    painter->setPen(Qt::darkBlue);
    painter->drawText(option.rect.x(), option.rect.y() + 17, option.rect.width(), option.rect.height(), Qt::TextSingleLine|Qt::AlignLeft, text_parts[1]);
}

QSize _combo_2line_delegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QVariant value = index.data(Qt::SizeHintRole);
    if(value.isValid())
        return qvariant_cast<QSize>(value);
    QRect decorationRect = rect(option, index, Qt::DecorationRole);
    QRect displayRect = rect(option, index, Qt::DisplayRole);
    QRect checkRect = rect(option, index, Qt::CheckStateRole);

    doLayout(option, &checkRect, &decorationRect, &displayRect, true);

    return (decorationRect|displayRect|checkRect).size();
}
