#include "auth_dlg.hpp"
#include "ui_auth_dlg.h"
#include "overlay_label.hpp"
#include "combo_2line_delegate.hpp"

#ifdef DEBUG
    #include <QDebug>
#endif

_auth_dlg::_auth_dlg(const _eq_net_client::_opers_info& opers_info, const QString& uname,
                     const _eq_net_client::_wplaces_info& wplaces_info, const QString& wplace,
                     bool is_brake /*=false*/,
                     QWidget *parent) :
    QDialog(parent, Qt::MSWindowsFixedSizeDialogHint | Qt::FramelessWindowHint),
    ui(new Ui::_auth_dlg),
    m_uname(""), m_pwd(""), m_wplace("")

{
    ui->setupUi(this);
    _combo_2line_delegate* itemdelegate = new _combo_2line_delegate;
    ui->cmb_uname->setItemDelegate(itemdelegate);
    ui->cmb_wplace->setItemDelegate(itemdelegate);

    if(!is_brake)
    {
        // fetch list of operators for current office
        for(_eq_net_client::_opers_info::const_iterator itopi = opers_info.begin(); itopi != opers_info.end(); itopi++)
        {
            ui->cmb_uname->addItem(QString::fromLocal8Bit(itopi->name.c_str()) + "\r\n" +  QString::fromLocal8Bit(itopi->login.c_str()),
                                   QString::fromLocal8Bit(itopi->login.c_str()));
            if(!uname.isEmpty())
                if(QString::fromLocal8Bit(itopi->login.c_str()) == uname)
                {
                    ui->cmb_uname->setCurrentIndex(ui->cmb_uname->count() - 1);
                    if(ui->cmb_uname->currentIndex() == 0) //fixes a bug - 0 is set by default before explicit change
                        on_cmb_uname_currentIndexChanged(0);
                }
        }

        // fetch list of workplaces for current office
        for(_eq_net_client::_wplaces_info::const_iterator itwpi = wplaces_info.begin(); itwpi != wplaces_info.end(); itwpi++)
        {
            ui->cmb_wplace->addItem(QString::fromLocal8Bit(itwpi->name.c_str()) + "\r\n" + QString::fromLocal8Bit(itwpi->place.c_str()),
                                   QString::fromLocal8Bit(itwpi->place.c_str()));
            if(!wplace.isEmpty())
                if(QString::fromLocal8Bit(itwpi->place.c_str()) == wplace)
                {
                    ui->cmb_wplace->setCurrentIndex(ui->cmb_wplace->count() - 1);
                    if(ui->cmb_wplace->currentIndex() == 0) //fixes a bug - 0 is set by default before explicit change
                        on_cmb_wplace_currentIndexChanged(0);
                }
        }
    }
    else
    {
        ui->cmb_uname->setEnabled(false);
        ui->cmb_uname->setEditText(uname);
        ui->lbl_uname->hide();
        ui->cmb_wplace->setEnabled(false);
        ui->cmb_wplace->setEditText(wplace);
        ui->lbl_wplace->hide();
    }

    ui->lbl_uname->attach_buddy(ui->cmb_uname);
    ui->lbl_wplace->attach_buddy(ui->cmb_wplace);

    if(!uname.isEmpty() && !wplace.isEmpty())
        ui->edt_pwd->setFocus();
}

_auth_dlg::~_auth_dlg()
{
    delete ui;
}

//---------- for overlayed labels

void _auth_dlg::on_lblUname_focusIn()
{
    ui->cmb_uname->setFocus();
}
void _auth_dlg::on_cmb_uname_editTextChanged(const QString& txt)
{
    if(txt.isEmpty())
        ui->lbl_uname->show();
    else
        ui->lbl_uname->hide();
}
void _auth_dlg::on_lblPwd_focusIn()
{
    ui->edt_pwd->setFocus();
}
void _auth_dlg::on_edt_pwd_textChanged(const QString& txt)
{
    if(txt.isEmpty())
        ui->lbl_pwd->show();
    else
        ui->lbl_pwd->hide();
}
void _auth_dlg::on_lblWplace_focusIn()
{
    ui->cmb_wplace->setFocus();
}
void _auth_dlg::on_cmb_wplace_editTextChanged(const QString& txt)
{
    if(txt.isEmpty())
        ui->lbl_wplace->show();
    else
        ui->lbl_wplace->hide();
}

//----------

void _auth_dlg::on_cmb_uname_currentIndexChanged(int index)
{
    ui->cmb_uname->setEditText(ui->cmb_uname->itemData(index).toString());
}
void _auth_dlg::on_cmb_wplace_currentIndexChanged(int index)
{
    ui->cmb_wplace->setEditText(ui->cmb_wplace->itemData(index).toString());
}

//----------

void _auth_dlg::on_btnOK_clicked()
{
    m_uname = ui->cmb_uname->itemData(ui->cmb_uname->currentIndex()).toString();
    m_pwd = ui->edt_pwd->text();
    m_wplace = ui->cmb_wplace->itemData(ui->cmb_wplace->currentIndex()).toString();

    accept();
}
void _auth_dlg::on_btnCancel_clicked()
{
    reject();
}

//---------- for buttons' state change

void _auth_dlg::on_btnOK_pressed()
{
//    qDebug() << "_auth_dlg::on_btnOK_pressed()";

//    QPalette palette = ui->btnOK->palette();
//    palette.setColor(QPalette::ButtonText, QColor(34, 178, 0)); //22b200
//    ui->btnOK->setPalette(palette);

    ui->btnOK->setStyleSheet("selection-background-color: rgba(0, 0, 0, 0); background-color: rgba(0, 0, 0, 0); color: rgb(34, 178, 0); font: 75 14pt \"MS Shell Dlg 2\"; image: url();");
}
void _auth_dlg::on_btnOK_released()
{
    ui->btnOK->setStyleSheet("selection-background-color: rgba(0, 0, 0, 0); background-color: rgba(0, 0, 0, 0); color: rgb(255, 255, 255); font: 75 14pt \"MS Shell Dlg 2\"; image: url();");
}
void _auth_dlg::on_btnCancel_pressed()
{
//    qDebug() << "_auth_dlg::on_btnCancel_pressed()";

//    QPalette palette = ui->btnCancel->palette();
//    palette.setColor(QPalette::ButtonText, QColor(199, 0, 0)); //c70000
//    ui->btnCancel->setPalette(palette);

    ui->btnCancel->setStyleSheet("selection-background-color: rgba(0, 0, 0, 0); background-color: rgba(0, 0, 0, 0); color: rgb(199, 0, 0); font: 75 14pt \"MS Shell Dlg 2\"; image: url();");
}
void _auth_dlg::on_btnCancel_released()
{
    ui->btnCancel->setStyleSheet("selection-background-color: rgba(0, 0, 0, 0); background-color: rgba(0, 0, 0, 0); color: rgb(255, 255, 255); font: 75 14pt \"MS Shell Dlg 2\"; image: url();");
}
