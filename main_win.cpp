#include "main_win.hpp"
#include "ui_main_win.h"

#include "version.h"
#include "settings.hpp"
#include "list_selection_dlg.h"
#include "ui/settings_dlg.hpp"
#include "form_add_indicators.hpp"
#include "json/json.hpp"

#include <limits.h>

#include <text_help.hpp>
#include <net_http_client.h>

#include <QPushButton>

//#include <debug.hpp>
//#include <debug_extern.h>
#ifdef DEBUG
    #include <QDebug>
#endif

using namespace std;
using json = nlohmann::json;

_main_win::_main_win(QWidget *parent) :
    QMainWindow(parent, Qt::MSWindowsFixedSizeDialogHint | Qt::FramelessWindowHint),
    m_ui(new Ui::main_win),
    m_tray(0),
    m_qi(0),
    m_ticket_in_proc_service_time(0, 0),
    timer(NULL),video_gw(NULL),
    buttoNextTicket2(NULL),
    labelNextTicket2(NULL)
{
    m_prev_any_change_time.set_now();

    setWindowIcon(QIcon(":/images/mainwnd_icon.ico"));
    m_tray = new _rdg_tray(this, this);

    m_ui->setupUi(this); //initialize ui
}

_main_win::~_main_win()
{
    //wa4timer
    if(timer)
    {
        timer->Kill();
        while(timer->GetStatus() != TH_STATUS_KILLED) Sleep(10);
        delete timer;
        timer = NULL;
    }

    if (video_gw)
    {
        video_gw->Kill();
        while(video_gw->GetStatus() != TH_STATUS_KILLED) Sleep(10);
        delete video_gw;
        video_gw = NULL;
    }

    if(m_tray)
        delete m_tray;

    if(m_ui)
    {
        delete m_ui;
        m_ui = NULL;
    }

    if(buttoNextTicket2!=NULL) { buttoNextTicket2->close(); buttoNextTicket2->deleteLater(); buttoNextTicket2 = NULL; }
    if(labelNextTicket2!=NULL) { labelNextTicket2->close(); labelNextTicket2->deleteLater(); labelNextTicket2 = NULL; }
}

void _main_win::err_msg(QMessageBox::Icon icon, const char* text)
{
    QMessageBox msgbox(icon, QCoreApplication::applicationName(), QString(text ? text : "Сбой приложения. Подробные сведения в лог-файле."), QMessageBox::Ok);
    msgbox.setModal(true);
    msgbox.exec();
}

void _main_win::init_form()
  {
//    if(!m_ui)
//        THROW_EXCEPTION("UI root object is not initialized!");

    //m_ui->btn_settings->setVisible(false);
    if(_gl_set().config.run.create_ticket_enable == 0)
    {
        m_ui->btn_create_new_ticket->setVisible(false);
        m_ui->btn_forward->setFixedWidth(301);
        m_ui->btn_forward->pos().setX(340);
    }
    else
    {
        m_ui->btn_create_new_ticket->setVisible(true);
    }
    m_ui->centralwidget->setEnabled(false);

    installEventFilter(this);

    if(m_tray)
    {
        connect(this, SIGNAL(show_tray_messsage(QString, QString)), m_tray, SLOT(show_message(QString, QString)));
        m_tray->set_app_name(QString::fromLocal8Bit(windowTitle().toStdString().c_str()));
    }

    //office name, operator name
    m_ui->lbl_office->setText(_gl_set().config.office.name);
    m_ui->lbl_operator->setText(_gl_set().config.user.name + " (" + _gl_set().config.user.wp_name + ")");

    // date & time
    set_cur_time();

    QButtonGroup* btn_issue_group = new QButtonGroup(this);
    btn_issue_group->addButton(m_ui->btn_issues_1);
    btn_issue_group->addButton(m_ui->btn_issues_2);
    btn_issue_group->addButton(m_ui->btn_issues_3);
    btn_issue_group->addButton(m_ui->btn_issues_4);
    btn_issue_group->addButton(m_ui->btn_issues_5);

    //disable ticket control panel
    on_ticket_close();

    //side indicators

    m_ui->sa_side_indicators->verticalScrollBar()->installEventFilter(this);
    m_sb_indicators = new _v_scroll_bar(m_ui->sa_side_indicators);

    add_side_indicator("ticket_in_proc", "Открытый талон", _gl_set().config.ui.ind.proc_tikcet_n);
    add_side_indicator("ticket_in_proc_service_time", "Длительность обслуживания<br>открытого талона", _gl_set().config.ui.ind.proc_tikcet_serv_time);
    add_side_indicator("served_ticket_total2day", "Обслуженных<br>с начала дня", _gl_set().config.ui.ind.serv2day);
//    add_side_indicator("redirected_ticket_total2day", "Перенаправленных<br>с начала дня", _gl_set().config.ui.ind.redir2day);
//    add_side_indicator("absent_ticket_total2day", "Не явившихся<br>с начала дня", _gl_set().config.ui.ind.abort2day);
    add_side_indicator("waiting_time_limit_ms", "Максимальная<br>длительность ожидания", _gl_set().config.ui.ind.proc_tikcet_n);
    add_side_indicator("service_time_limit_ms", "Максимальная<br>длительность обслуживания", true /*settings->service_time_limit_ms()*/);

    align_side_indicators();

    //add queue buttons

    if(_gl_set().config.ui.single_call_button)
    {
        QPushButton* btn = new QPushButton("Вызов", m_ui->sa_queues_content);
        connect(btn, SIGNAL(released()), this, SLOT(do_call_next_ticket()));
        btn->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);

        btn->setObjectName("CALL_NEXT"); //queue id will be appended on timer
        btn->setGeometry(100, 100, m_ui->sa_queues->width() - 200, m_ui->sa_queues->height() - 200);
        btn->setStyleSheet(
                    "QPushButton{font:75 20pt \"Myriad Pro\";font-weight:bold;color:#51585e;text-align:center;padding-left:10px;}"
                    "QPushButton#" + btn->objectName() + "{border-image:url(:/images/btn_normal.png);}"
                    "QPushButton#" + btn->objectName() + ":hover{border-image:url(:/images/btn_hover.png);}"
                    "QPushButton#" + btn->objectName() + ":pressed{border-image:url(:/images/btn_pressed.png);}"
                    "QPushButton#" + btn->objectName() + ":disabled{border-image:url(:/images/btn_disabled.png);}");
        btn->show();

        QLabel* lbl = new QLabel(btn); //button has to be parent to receive mouse events
        lbl->setObjectName("CALL_NEXT_SIZE");
        lbl->setGeometry(btn->width() - 80, 0, 55, btn->height());
        lbl->setStyleSheet("QLabel#" + lbl->objectName() +
                           "{font:75 20pt \"Myriad Pro\";font-weight: bold;color:#51585e;"
                           "text-align:right;padding-right:10px;border-image:url();}");
        lbl->show();

//        //for scrollbar
//        m_ui->sa_queues_content->setGeometry(0, 0, m_ui->sa_queues_content->width(), iy);
    }
    else
    {
        if(!srv)
        {
            err_msg(QMessageBox::Critical);
            THROW_EXCEPTION("Server object is not initialized.");
        }
        srv->lock();
        update_queue_buttons();
        srv->unlock();
    }

    m_ui->sa_queues->verticalScrollBar()->installEventFilter(this);
    m_sb_queues = new _v_scroll_bar(m_ui->sa_queues);

    // timer for clock, indicators and queues' states
    //wa4timer
//    timer_id = startTimer(1000); // 1s
    timer = new _timer(this);
    timer->SetSleepTime(1000); // 1s
    timer->Start();
    connect(this, SIGNAL(timerEventPost()), this, SLOT(timerEvent()), Qt::QueuedConnection);

    if (!_gl_set().config.external.video_cams.empty())
    {
        video_gw = new _video_gw(_gl_set().config.external.video_cams);
        video_gw->SetSleepTime(100); // 1s
        video_gw->Start();
    }
}

bool _main_win::init_scroll_bar(QObject* object, QEvent* event, _v_scroll_bar* sb, const QScrollBar* qsb)
{
    if(object == qsb)
    {
        if(event->type() == QEvent::Show)
        {
            sb->initialize();
            sb->setVisible(true);
            return false;
        }
        if(event->type() == QEvent::Hide)
        {
            sb->setVisible(false);
            return false;
        }
        return false;
    }
    return true;
}

bool _main_win::eventFilter(QObject *obj, QEvent *event)
{
    if(!m_ui)
        return false;

    if(!init_scroll_bar(obj, event, m_sb_indicators, m_ui->sa_side_indicators->verticalScrollBar()))
        return false;

    if(!init_scroll_bar(obj, event, m_sb_queues, m_ui->sa_queues->verticalScrollBar()))
        return false;

    static int posx = 0;
    static int posy = 0;
    if(event->type() == QEvent::MouseButtonPress)
    {
        //start dragging window by title
        QMouseEvent* spec_event = (QMouseEvent*)event;
        posx = spec_event->x();
        posy = spec_event->y();
        return true;
    }
    if(event->type() == QEvent::MouseMove)
    {
        //dragging window by title
        QMouseEvent* spec_event = (QMouseEvent*)event;
        move(x() + spec_event->x() - posx, y() + spec_event->y() - posy);
        return true;
    }
    return false;
}

void _main_win::closeEvent(QCloseEvent *event)
{
    _log log;
    log << LOG_UI(10) << "exiting the application..." << endl;

    SRV_CHECK_STATUS_AND_LOCK

    if(srv->my_wplace()->current_ticket())
    {
        srv->unlock();
        event->ignore();
        err_msg(QMessageBox::Warning, "Для выхода из программы необходимо закрыть талон.");
        log << "...exiting the application aborted - ticket open." << endl;
        return;
    }
    srv->unlock();

    //path from resource
    QMessageBox msgbox(QMessageBox::Question, QCoreApplication::applicationName(), QString("Выйти из программы?"), QMessageBox::Yes | QMessageBox::No);
    //dialog.move(geometry().left() + geometry().width() / 2 - 200, geometry().top() + geometry().height() / 4);
    msgbox.setModal(true);
    int res = msgbox.exec();

    if (res==QMessageBox::Yes)
    {
        // TODO - save ind user set
        /*
        QList<_widget_indicator*> wis = m_ui->fr_side_indicators->findChildren<_widget_indicator*>(QRegExp("ind_*"));
        foreach(_widget_indicator* wi, wis)
        {
            if(wi->objectName() == "ind_ticket_in_proc")
                settings->set_ticket_in_proc(wi->isVisible());
            else if(wi->objectName() == "ind_ticket_in_proc_service_time")
                settings->set_ticket_in_proc_service_time(wi->isVisible());
            else if(wi->objectName() == "ind_served_ticket_total2day")
                settings->set_served_ticket_total2day(wi->isVisible());
//                else if(wi->objectName() == "ind_redirected_ticket_total2day")
//                    settings->set_redirected_ticket_total2day(wi->isVisible());
//                else if(wi->objectName() == "ind_absent_ticket_total2day")
//                    settings->set_absent_ticket_total2day(wi->isVisible());
            else if(wi->objectName() == "ind_waiting_time_limit_ms")
                settings->set_waiting_time_limit_ms(wi->isVisible());
            else if(wi->objectName() == "ind_service_time_limit_ms")
                settings->set_service_time_limit_ms(wi->isVisible());
        }
        */
    }
    else
    {
        event->ignore();
        log << "...exiting the application aborted by user." << endl;
    }
}

void _main_win::set_cur_time(void)
{
    QDate cur_date = QDate::currentDate();
    m_ui->lbl_date->setText(cur_date.toString("dd.MM.yyyy"));
    QTime  cur_time = QTime::currentTime();
    m_ui->lbl_time->setText(cur_time.toString("hh:mm:ss"));
}

//Функция для std::sort сортировки в _main_win::update_queue_buttons(void)
//Сортирует очереди по номеру
bool sort_to_short_number(std::pair<_eq::_id, _eq::_q *> &val1, std::pair<_eq::_id, _eq::_q *> &val2)
{
    return val1.second->short_number() < val2.second->short_number();
}

void _main_win::update_queue_buttons(void)
{
    QList<QPushButton*> btns_call_next = m_ui->sa_queues_content->findChildren<QPushButton*>(QRegExp(QString("CALL_NEXT_Q")));
    QList<QPushButton*> btns_call_by_n = m_ui->sa_queues_content->findChildren<QPushButton*>(QRegExp(QString("CALL_NUM_Q")));
    QPushButton* btn;

    int iprev = 0, nprev = btns_call_next.size();
    const int rowh = 45;
    int y, iy = srv->my_office()->use_wp_queues() ? rowh : 0; //1st row for @-q

    const _eq::_qs* qs = srv->my_wplace()->qs();

    //Сортировка qs по номеру
    std::vector<std::pair<_eq::_id, _eq::_q *>> vec(qs->begin(),qs->end()); //Почему-то неполучается отсортировать map, поэтому перегоняем в vector
    std::sort(vec.begin() , vec.end() , sort_to_short_number); // Сортируем по № вместо ID

    //Заполнение списка очередей на форме (изначально цикл запускался сразу по qs, но теперь он по сортированному вектору)
    for(std::vector<std::pair<_eq::_id, _eq::_q *>>::const_iterator it = vec.begin(); it != vec.end(); it++)
    {
        const _eq::_q* q = it->second;
        bool wp_q = q->name()[0] == '@'; //dedicated queue of the workplace
        if(wp_q & !srv->my_office()->use_wp_queues())
            continue;

        //Пропускаем кнопки с 0 талонов
        if(config_user.getConfPropertyInt("hide_empty_queues",0)==1 && q->tickets()->size()==0 && wp_q==false) continue;

        char queue_id[32];
        QString qid_str = QString::fromLocal8Bit(q->id().to_c_string(queue_id));

        y = wp_q & srv->my_office()->use_wp_queues() ? 0 : iy;

        if(iprev < nprev)
        {
            // update existing button
            btn = btns_call_next[iprev];
        }
        else
        {
            // create new button
            btn = new QPushButton(m_ui->sa_queues_content);
            connect(btn, SIGNAL(released()), this, SLOT(do_call_next_ticket()));
            btn->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        }
        QString buttonName = wp_q ? QString("Рабочее место") : QString::fromLocal8Bit(q->name().c_str());
        //Обрезаем текст до 430px
        QFont font("Myriad Pro",75,QFont::Bold); font.setPixelSize(16); QFontMetrics fm(font);
        if(fm.boundingRect(buttonName).width() > 430) {btn->setToolTip(buttonName); buttonName = fm.elidedText(buttonName,Qt::ElideRight,430);}
        btn->setText(buttonName);

        btn->setText(buttonName);
        btn->setObjectName(QString("CALL_NEXT_Q") + qid_str);
        btn->setGeometry(0, y, 480, 40);
        btn->setStyleSheet(
                    "QPushButton{font:75 16px \"Myriad Pro\";font-weight:bold;color:#51585e;text-align:left;padding-left:10px;}"
                    "QPushButton#" + btn->objectName() + "{border-image:url(:/images/btn_qleft_normal.png);}"
                    "QPushButton#" + btn->objectName() + ":hover{border-image:url(:/images/btn_qleft_hover.png);}"
                    "QPushButton#" + btn->objectName() + ":pressed{border-image:url(:/images/btn_qleft_pressed.png);}"
                    "QPushButton#" + btn->objectName() + ":disabled{border-image:url(:/images/btn_qleft_disabled.png);}"
                    "QToolTip{background-color: rgb(214, 214, 214); color: rgb(0, 0, 0); font: 15px \"MS Shell Dlg 2\";}");
        btn->show();

        QLabel* lbl = iprev < nprev ? (QLabel*)btn->children()[0] : new QLabel(btn);//button has to be parent to receive mouse events
        lbl->setText(QString::number(q->tickets()->size()));
        lbl->setObjectName(QString("CALL_NEXT_Q_SIZE") + qid_str);
        lbl->setGeometry(btn->width() - 40, 0, 55, btn->height());
        lbl->setStyleSheet("QLabel#" + lbl->objectName() +
                           "{font:75 16px \"Myriad Pro\";font-weight: bold;color:#51585e;"
                           "text-align:right;padding-right:10px;border-image:url();}");
        lbl->show();

        if(iprev < nprev)
        {
            // update existing button
            btn = btns_call_by_n[iprev];
        }
        else
        {
            // create new button
            btn = new QPushButton("Вызов по №", m_ui->sa_queues_content);
            connect(btn, SIGNAL(released()), this, SLOT(do_call_next_ticket_by_num()));
            btn->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        }
        btn->setObjectName(QString("CALL_NUM_Q") + qid_str);
        btn->setGeometry(480, y, 140, 40);
        btn->setStyleSheet(
            "QPushButton{font:75 16px \"Myriad Pro\";font-weight:bold;color:#51585e;text-align:center;}"
            "QPushButton#" + btn->objectName() + "{border-image:url(:/images/btn_qright_normal.png);}"
            "QPushButton#" + btn->objectName() + ":hover{border-image:url(:/images/btn_qright_hover.png);}"
            "QPushButton#" + btn->objectName() + ":pressed{border-image:url(:/images/btn_qright_pressed.png);}"
            "QPushButton#" + btn->objectName() + ":disabled{border-image:url(:/images/btn_qright_disabled.png);}");
        btn->show();

        if(!wp_q)
            iy += rowh;
        ++iprev;
    }

    //Кнопка вызова следующего из любой очереди (которая внизу списка вызова из очередей)
    if(config_user.getConfPropertyInt("show_button_call_next",0)==1)
    {
        if(buttoNextTicket2==NULL)
        {
            buttoNextTicket2 = new QPushButton("Вызов следующего", m_ui->sa_queues_content);
            connect(buttoNextTicket2, SIGNAL(released()), this, SLOT(do_call_next_ticket()));
            buttoNextTicket2->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
            buttoNextTicket2->setObjectName("CALL_NEXT2"); //queue id will be appended on timer
            buttoNextTicket2->setStyleSheet(
                        "QPushButton{font:75 16px \"Myriad Pro\";font-weight:bold;color:#51585e;text-align:left;padding-left:10px;}}"
                        "QPushButton#" + buttoNextTicket2->objectName() + "{border-image:url(:/images/btn_normal.png);}"
                        "QPushButton#" + buttoNextTicket2->objectName() + ":hover{border-image:url(:/images/btn_hover.png);}"
                        "QPushButton#" + buttoNextTicket2->objectName() + ":pressed{border-image:url(:/images/btn_pressed.png);}"
                        "QPushButton#" + buttoNextTicket2->objectName() + ":disabled{border-image:url(:/images/btn_disabled.png);}");
        }

        if(labelNextTicket2==NULL)
        {
            labelNextTicket2 = new QLabel(buttoNextTicket2); //button has to be parent to receive mouse events
            labelNextTicket2->setObjectName("CALL_NEXT_SIZE2");
            labelNextTicket2->setText("?");
            labelNextTicket2->setStyleSheet("QLabel#" + labelNextTicket2->objectName() +
                                            "{font:75 16px \"Myriad Pro\";font-weight: bold;color:#51585e;"
                                            "text-align:right;padding-right:10px;border-image:url();}");
        }

        iy += rowh;     y += rowh;

        QPushButton* btn = buttoNextTicket2;
        btn->setGeometry(0, y, 480, 40);
        btn->show();

        QLabel* lbl = labelNextTicket2;
        lbl->setGeometry(btn->width() - 40, 0, 55, btn->height());
        lbl->show();
    }
    else
    {
        if(buttoNextTicket2!=NULL) { buttoNextTicket2->close(); buttoNextTicket2->deleteLater(); buttoNextTicket2 = NULL; }
        if(labelNextTicket2!=NULL) { labelNextTicket2->close(); labelNextTicket2->deleteLater(); labelNextTicket2 = NULL; }
    }

    // remove old extra buttons
    while(iprev < nprev)
    {
        delete btns_call_next[iprev];
        delete btns_call_by_n[iprev];
        ++iprev;
    }

    //for scrollbar
    m_ui->sa_queues_content->setGeometry(0, m_ui->sa_queues_content->y(), m_ui->sa_queues_content->width(), iy);
}

//wa4timer
int _main_win::_timer::Poll()
{
    main_win->timerEventPost();
    return 0;
}

void _main_win::timerEvent(/*QTimerEvent**/)//wa4timer
{
    set_cur_time(); // set date-time indicator

    if(!srv)
    {
        err_msg(QMessageBox::Critical);
        THROW_EXCEPTION("Server object is not initialized.");
    }
    srv->lock();

    _eq_net_client_rmo::_client_status con_status = srv->status();
    if(con_status == _eq_net_client_rmo::client_status_ok)
    {
        // set UI on connection restore

        if(!m_ui->centralwidget->isEnabled())
        {
            m_ui->centralwidget->setEnabled(true);
            m_ui->lbl_connection->setStyleSheet(
                        "QLabel#lbl_connection"
                        "{border-image:url(:/images/connection_ok.png);}");
            QGuiApplication::restoreOverrideCursor();
        }

        //check for press "quality mark" buttons on hw console

        if(mark_console)
        {
            // _log() << LOG_COMMON(0) << "_main_win::timerEvent - mark console present" << endl; - why, oh...why? Tell me why?
            _mark_console::_key key;
            if(mark_console->was_pressed(&key))
            {
                _log() << LOG_COMMON(0) << "_main_win::timerEvent - mark console key " << key << " pressed" << endl;
                switch(key)
                {
                case _mark_console::_key_good: srv->my_wplace()->set_mark(4); break;        // 5 of 5
                case _mark_console::_key_normal: srv->my_wplace()->set_mark(3); break;      // 4 of 5
                case _mark_console::_key_normalpoor: srv->my_wplace()->set_mark(23); break;      // 3 of 5
                case _mark_console::_key_poor: srv->my_wplace()->set_mark(2); break;        // 2 of 5
                case _mark_console::_key_terrific: srv->my_wplace()->set_mark(1); break;    // 1 of 5
                }
            }
        }

        // increment service time indicator

        _widget_indicator* ind;
        if(srv->my_wplace()->current_ticket())
        {
            m_ticket_in_proc_service_time = m_ticket_in_proc_service_time.addSecs(1);
            if(ind = get_indicator("ind_ticket_in_proc_service_time"))
            {
                ind->set_value(m_ticket_in_proc_service_time.toString());
                // set red color, if proc_time expired
                QTime limit = QTime(0,0);
                limit = limit.addMSecs(srv->my_office()->limit_service_ticket_ms_time());

                if(m_ticket_in_proc_service_time>=limit)
                {
                    if(ind->is_value_red() == false)
                    {
                        ind->set_value_red(true);
                        if(_gl_set().config.run.message_dif_to_close==true)
                        {
                            //Переоткрыть окно, чтоб оно было наверху
                            if(this->isActiveWindow()==false) {this->showMinimized(); this->showNormal();}
                            //Вывод предупреждающего сообщения
                            if(messageBoxT.isVisible()==false) {messageBoxT.setText("Превышено время обслуживания открытого талона"); messageBoxT.show();}
                        }
                    }
                }
                else
                {
                    ind->set_value_red(false);
                }
            }

        }

        if(srv->my_office_was_updated())
        {
            //set indicator of current ticket in proc

            if(srv->my_wplace()->current_ticket())
            {
                if(ind = get_indicator("ind_ticket_in_proc"))
                    ind->set_value(QString::fromLocal8Bit(srv->my_wplace()->current_ticket()->number().c_str()));
            }
            else
            {
                if(ind = get_indicator("ind_ticket_in_proc"))
                    ind->set_value("");
                if(!m_ticket_in_proc_service_time.isNull())
                {
                    m_ticket_in_proc_service_time = QTime(0, 0);
                    if(ind = get_indicator("ind_ticket_in_proc_service_time"))
                        ind->set_value("");
                }
            }

            // add/remove queue buttons

            if(!_gl_set().config.ui.single_call_button) update_queue_buttons();

            // set rest indicators

            if(ind = get_indicator("ind_served_ticket_total2day"))
                ind->set_value(QString::number(srv->my_oper()->serviced_ticket_cnt_at_cday()));
//            if(ind = get_indicator("ind_redirected_ticket_total2day"))
//                ind->set_value(QString::number(srv->my_oper()->redirected_ticket_cnt_at_cday()));
//            if(ind = get_indicator("ind_absent_ticket_total2day"))
//                ind->set_value(QString::number(srv->my_oper()->absent_ticket_cnt_at_cday()));
            if(ind = get_indicator("ind_service_time_limit_ms"))
                ind->set_value(ms_to_hms(srv->my_office()->limit_service_ticket_ms_time()));
            if(ind = get_indicator("ind_waiting_time_limit_ms"))
                ind->set_value(ms_to_hms(srv->my_office()->limit_wait_ticket_ms_time()));

            // notification for new ticket in former empty queue (don't notify just on start)

            string s;
            bool alert = false, init = m_queues_prev_size.size() == 0;
            for(_eq::_qs::const_iterator it = srv->my_wplace()->qs()->begin(); it != srv->my_wplace()->qs()->end(); it++)
            {
                if(m_queues_prev_size[it->first.to_string(s)] == 0 && it->second->tickets()->size() > 0 && !init)
                    alert = true;
                m_queues_prev_size[it->first.to_string(s)] = it->second->tickets()->size();
            }

            srv->unlock();

            // notify on new ticket according to settings

            if(alert)
            {
                //if(isMinimized())
                if(!isActiveWindow())
                {
                    if (config_user.getConfPropertyInt("alert_in_tray",1)) emit show_tray_messsage(QCoreApplication::applicationName(), "Появился новый талон!");
                    if (config_user.getConfPropertyInt("sound_notif",1)) PlaySound(TEXT("DingDong.wav"), NULL, SND_FILENAME);
                    if (config_user.getConfPropertyInt("open_window_if_notif",1)) setWindowState(windowState() & ~Qt::WindowMinimized | Qt::WindowActive);
                }
            }
            return;
        }

        //enum all queues...

        //_time::time_val_i ct_ms = 0;
        bigint oldest_pos = LONG_LONG_MAX;
        bool is_pr;
        QPushButton* pr_btn1 = NULL;//for multi-button call ui
        QPushButton* pr_btn2 = NULL;//for multi-button call ui
        QString pr_qid_str;//for single-button call ui
        bool qexceed = false;
        int total_tickets = 0;//for single-button call ui

        const _eq::_qs* qs = srv->my_wplace()->qs();
        for(_eq::_qs::const_iterator it = qs->begin(); it != qs->end(); it++)
        {
            const _eq::_q* q = it->second;
            bool wp_q = q->name()[0] == '@'; //dedicated queue of the workplace
            if(q->name()[0] == '@') // dedicated queue of the workplace
                if(!srv->my_office()->use_wp_queues())
                    continue;

            //Пропускаем кнопки с 0 талонов
            if(config_user.getConfPropertyInt("hide_empty_queues",0)==1 && q->tickets()->size()==0 && wp_q==false) continue;

            is_pr = false;
            if(!_gl_set().config.ui.single_call_button) qexceed = false;

            char queue_id[32];
            QString qid_str = QString::fromLocal8Bit(q->id().to_c_string(queue_id));

            if(q->tickets()->size() != 0)
            {
                total_tickets += q->tickets()->size();

                //...calc service duration

                QDateTime ct0 = QDateTime::currentDateTimeUtc();
                _time ct(ct0.date().year(), ct0.date().month(), ct0.date().day(),
                         ct0.time().hour(), ct0.time().minute(), ct0.time().second(), ct0.time().msec());
                //qDebug() << QString().sprintf("Now is  %02d.%02d.%04d %02d:%02d:%02d", ct.day(), ct.month(), ct.year(), ct.hour(), ct.minute(), ct.second());
                _time::time_val_i dt_ms = ct.diff_ms(q->tickets()->begin()->second->create_ftime()); //server time is in utc
                if(dt_ms/* - 14400000*/> srv->my_office()->limit_wait_ticket_ms_time())
                    qexceed = true;

                //...detect oldest ticket among all queues of the wplace

                //if(!ct_ms || ct_ms > q->tickets()->begin()->second->create_ftime().stamp())
                if(oldest_pos > q->tickets()->begin()->second->pos_in_q())
                {
                    //ct_ms = q->tickets()->begin()->second->create_ftime().stamp();
                    oldest_pos = q->tickets()->begin()->second->pos_in_q();
                    is_pr = true;
                    pr_qid_str = qid_str;
                }
            }

            //...mark queue with exceeded waiting ticket with red color of text

            if(!_gl_set().config.ui.single_call_button)
            {
                QString ctrl_name = QString("CALL_NEXT_Q") + qid_str;
                QPushButton* btn = m_ui->sa_queues_content->findChild<QPushButton*>(ctrl_name);
                if(btn==NULL) continue; //Этой кнопки нет
                if(is_pr)
                    pr_btn1 = btn;
                btn->setStyleSheet(
                    "QPushButton{font:75 16px \"Myriad Pro\";font-weight:bold;color:#" + QString(qexceed ? "da5b14" : "51585e") + ";text-align:left;padding-left:10px;}"
                    "QPushButton#" + ctrl_name + "{border-image:url(:/images/btn_qleft_normal.png);}"
                    "QPushButton#" + ctrl_name + ":hover{border-image:url(:/images/btn_qleft_hover.png);}"
                    "QPushButton#" + ctrl_name + ":pressed{border-image:url(:/images/btn_qleft_pressed.png);}"
                    "QPushButton#" + ctrl_name + ":disabled{border-image:url(:/images/btn_qleft_disabled.png);}"
                    "QToolTip{background-color: rgb(214, 214, 214); color: rgb(0, 0, 0); font: 15px \"MS Shell Dlg 2\";}");

                QString buttonName = q->name()[0] == '@' ? QString("Рабочее место") : QString::fromLocal8Bit(q->name().c_str());
                //Обрезаем текст до 430px
                QFont font("Myriad Pro",75,QFont::Bold); font.setPixelSize(16); QFontMetrics fm(font);
                if(fm.boundingRect(buttonName).width() > 430) {btn->setToolTip(buttonName); buttonName = fm.elidedText(buttonName,Qt::ElideRight,430);}
                btn->setText(buttonName);

                ctrl_name = QString("CALL_NEXT_Q_SIZE") + qid_str;
                QLabel* lbl = (QLabel*)btn->children()[0];
                lbl->setStyleSheet("QLabel#" + ctrl_name + "{font:75 16px \"Myriad Pro\";font-weight:bold;color:#"
                                   + QString(qexceed ? "da5b14" : "51585e") + ";text-align:right;padding-right:10px;border-image:url();}");
                lbl->setNum((int)q->tickets()->size());

                ctrl_name = QString("CALL_NUM_Q") + qid_str;
                btn = m_ui->sa_queues_content->findChild<QPushButton*>(ctrl_name);
                if(is_pr)
                    pr_btn2 = btn;
                btn->setStyleSheet(
                    "QPushButton{font:75 16px \"Myriad Pro\";font-weight:bold;color:#" + QString(qexceed ? "da5b14" : "51585e") + ";text-align:center;}"
                    "QPushButton#" + ctrl_name + "{border-image:url(:/images/btn_qright_normal.png);}"
                    "QPushButton#" + ctrl_name + ":hover{border-image:url(:/images/btn_qright_hover.png);}"
                    "QPushButton#" + ctrl_name + ":pressed{border-image:url(:/images/btn_qright_pressed.png);}"
                    "QPushButton#" + ctrl_name + ":disabled{border-image:url(:/images/btn_qright_disabled.png);}");
            }
        }

        //Обновление счётчика на кнопке "Вызов следующего", которая в обычном режиме отображения
        if(labelNextTicket2!=NULL) { labelNextTicket2->setText(QString::number(total_tickets)); }

        queueIdCallNextTicket = pr_qid_str;

        if(_gl_set().config.ui.single_call_button)
        {
            //set id of queue with oldest ticket to single button

            QPushButton* btn = m_ui->sa_queues_content->findChildren<QPushButton*>(QRegExp("CALL_NEXT"))[0];
            QString ctrl_name = QString("CALL_NEXT") + pr_qid_str;
            btn->setObjectName(ctrl_name);

            //update total number of tickets
            QLabel* lbl = m_ui->sa_queues_content->findChild<QLabel*>("CALL_NEXT_SIZE");
            lbl->setText(QString::number(total_tickets));

            //set text color according to exceeded or not waiting time

            btn->setStyleSheet(
                "QPushButton{font:75 20pt \"Myriad Pro\";font-weight:bold;color:#" + QString(qexceed ? "da5b14" : "51585e") + ";text-align:center;padding-left:10px;}"
                "QPushButton#" + ctrl_name + "{border-image:url(:/images/btn_normal.png);}"
                "QPushButton#" + ctrl_name + ":hover{border-image:url(:/images/btn_hover.png);}"
                "QPushButton#" + ctrl_name + ":pressed{border-image:url(:/images/btn_pressed.png);}"
                "QPushButton#" + ctrl_name + ":disabled{border-image:url(:/images/btn_disabled.png);}");
        }
        else
        {
            //mark queue with oldest ticket with red color of border

            if(pr_btn1 && pr_btn2)
            {
                QString ss = pr_btn1->styleSheet();
                pr_btn1->setStyleSheet(ss + "QPushButton{background-color: #da5b14;}");
                ss = pr_btn2->styleSheet();
                pr_btn2->setStyleSheet(ss + "QPushButton{background-color: #da5b14;}");
            }
        }
    }
    else
    {
        // update UI on connection lost

        if(m_ui->centralwidget->isEnabled())
        {
            m_ui->centralwidget->setEnabled(false);
            m_ui->lbl_connection->setStyleSheet("QLabel#lbl_connection"
                                                "{border-image:url(:/images/connection_err.png);}");
            QGuiApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
            on_ticket_close();
            _widget_indicator* ind;
            if(ind = get_indicator("ind_ticket_in_proc"))
                ind->set_value("");
            if(ind = get_indicator("ind_ticket_in_proc_service_time"))
                ind->set_value("");

            _log() << LOG_CONNECTION(0) << "connection status = " << con_status << endl;
        }
    }

    srv->unlock();
}
/*
void _main_win::changeEvent(QEvent *e)
{
    if(e->type() == QEvent::WindowStateChange && isMinimized())
    {
        QTimer::singleShot(0, this, SLOT(hide()));
        e->ignore();
    }
    else
        QMainWindow::changeEvent(e);
}
*/

//fr_title

//property int posx: 0
//property int posy: 0
//mousePressEvent
//onPressed: {
//    posx = mouse.x;
//    posy = mouse.y;
//}
//mouseMoveEvent
//onPositionChanged: {
//    var delta = Qt.point(mouse.x - posx, mouse.y - posy);
//    window.pos = Qt.point(window.pos.x + delta.x, window.pos.y + delta.y)
//}

// toolbar -------------------------------------------------------------------------------

void _main_win::on_btn_win_minimize_clicked()
{
    showMinimized();
}

void _main_win::on_btn_win_close_clicked()
{
    close();
}


void _main_win::on_btn_settings_clicked()
{
    _log log;
    log << LOG_UI(10) << "opening settings dialog" << endl;
    _settings_dlg dlg;
    dlg.exec();
    if(dlg.update_qb==true && _gl_set().config.ui.single_call_button==false) update_queue_buttons();

    //Изменение отображения кнопки "Вызов следующего"
    if(_gl_set().config.ui.single_call_button==false && config_user.getConfPropertyInt("show_button_call_next")==1)
    {
        if(buttoNextTicket2==NULL) update_queue_buttons();
    }
    else
    {
        if(buttoNextTicket2!=NULL) { buttoNextTicket2->close(); buttoNextTicket2->deleteLater(); buttoNextTicket2 = NULL; }
        if(labelNextTicket2!=NULL) { labelNextTicket2->close(); labelNextTicket2->deleteLater(); labelNextTicket2 = NULL; }
    }
}


void _main_win::on_btn_report_clicked()
{
    QString name = ""; //TODO: report name

    _log log;
    log << LOG_UI(10) << "preparing report (name = " << name.toStdString() << ";";

    try
    {
        // it is JOKE
        /*
        if(!settings)
        {
            err_msg(QMessageBox::Critical);
            THROW_EXCEPTION("Settings object is not initialized.");
        }
        */

        string rep = _gl_set().config.external.report_url;
        log << " report=" << rep << ";";

        QUrl url=QUrl::fromEncoded(QByteArray(rep.c_str()));
        log << " url=" << url.toString().toStdString() << ")..." << endl;
        QDesktopServices::openUrl(url);

        log << "...report prepare done." << endl;
    }
    catch(_rdg_exception& e)
    {
        _log log;
        log << LOG_COMMON(0) << "Report opening command was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
    }
    catch(...)
    {
        _log log;
        log << LOG_COMMON(0) << "Report opening command was failed by unknown reason." << endl;
    }
}

void _main_win::on_btn_relogin_clicked()
{
    _log log;
    log << LOG_UI(10) << "doing relogin..." << endl;

    SRV_CHECK_STATUS_AND_LOCK
    if(srv->my_wplace()->current_ticket())
    {
        srv->unlock();
        err_msg(QMessageBox::Warning, "Для смены пользователя необходимо закрыть талон.");
        log << LOG_UI(10) << "relogin aborted - opened ticket." << endl;
        return;
    }
    srv->unlock();
    QCoreApplication::exit(-1);

    log << LOG_UI(10) << "...relogin done." << endl;
}

//take pause, have a cup of tea and talk with collegues)
void _main_win::on_btn_break_clicked()
{
    _log log;
    log << LOG_UI(10) << "taking break..." << endl;

    try
    {
        SRV_CHECK_STATUS_AND_LOCK
        _eq_err::_code err = _eq_err::ok;
//        _eq::_wplace::_status status = srv->my_wplace()->current_status();
//        switch(status)
//        {
//            case _eq::_wplace::proc:
//            case _eq::_wplace::wait:
                if(srv->my_wplace()->current_ticket())
                {
                    srv->unlock();
                    err_msg(QMessageBox::Warning, "Для приостановки работы необходимо закрыть талон.");
                    log << LOG_UI(10) << "taking break aborted - opened ticket." << endl;
                    return;
                }
                err = srv->my_wplace()->pause_work();
                srv->unlock();
                if(err != _eq_err::ok)
                {
                    err_msg(QMessageBox::Critical);
                    THROW_EXCEPTION(_eq_err::_server_answ[err]);
                }
//                break;
//            case _eq::_wplace::pause:
//                err = srv->my_wplace()->continue_work();
//                break;
//            default: // offline
//                break; //TODO ?
//        }
//        srv->unlock();
        QCoreApplication::exit(-2);

        log << LOG_UI(10) << "...take break done." << endl;
    }
    catch(_rdg_exception& e)
    {
        _log log;
        log << LOG_COMMON(0) << "Taking break (lock user) command was failed. " << GET_FULL_EXCEPTION_DESCRIPTION(e) << endl;
    }
    catch(...)
    {
        _log log;
        log << LOG_COMMON(0) << "Taking break (lock user) command was failed by unknown reason." << endl;
    }
}

// indicators -------------------------------------------------------------------------------

const QList<_widget_indicator*> _main_win::get_side_indicators_list()
{
    return m_ui->fr_side_indicators->findChildren<_widget_indicator*>(QRegExp("ind_*"));
}

void _main_win::set_state_side_indicator(QString name_ind, bool state)
{
    _widget_indicator* ind;
    if(ind = get_indicator(name_ind))
        ind->setVisible(state);
}

void _main_win::add_side_indicator(QString name, QString caption, bool state)
{
    QList<_widget_indicator*> wis = get_side_indicators_list();
    QRegExp rx_exist(name);
    QList<_widget_indicator*> wi_exist = m_ui->fr_side_indicators->findChildren<_widget_indicator*>(rx_exist);
    if(wi_exist.size())
        return;
    _widget_indicator* widget_indicator;
    widget_indicator = new _widget_indicator(m_ui->fr_side_indicators);
    widget_indicator->setObjectName("ind_" + name);
    widget_indicator->set_name(caption);
    widget_indicator->setVisible(state);
    widget_indicator->set_value("");
    connect(widget_indicator, SIGNAL(hide_indicator()), this, SLOT(hide_side_indicator()));

    m_ui->verticalLayout_2->insertWidget(wis.size(), widget_indicator);
}

int _main_win::count_visible_side_indicators(void)
{
    QList<_widget_indicator*> wis = get_side_indicators_list();
    int c = 0;
    foreach(_widget_indicator* wi, wis)
        if(!wi->isHidden())
            ++c;
    return c;
}

_widget_indicator* _main_win::get_indicator(QString name_indicator)
{
    QRegExp rx(name_indicator);//"ind_"
    QList<_widget_indicator*> wi;
//    rx.setPattern(name_indicator);
    wi = m_ui->fr_side_indicators->findChildren<_widget_indicator*>(rx);
    if(wi.size() > 0)
        return wi.at(0);
    return NULL;
}

QString _main_win::ms_to_hms(int ms)
{
    int sec = ms / 1000;
    int hour = sec / 3600;
    sec %= 3600;
    int min = sec / 60;
    sec %= 60;
    return QString().sprintf("%02d:%02d:%02d", hour, min, sec);
}

// indicators slots -------------------------------------------------------------------------

void _main_win::align_side_indicators()
{
    m_ui->btn_add_side_indicators->setVisible(count_visible_side_indicators() < get_side_indicators_list().count());
    int bh = m_ui->btn_add_side_indicators->isVisible() ? m_ui->btn_add_side_indicators->height() - 1 : 0;
    m_ui->sa_side_indicators->setGeometry(
                0,
                0,
                m_ui->fr_side_indicators_area->width(),
                m_ui->fr_side_indicators_area->height() - bh);
    m_sb_indicators->setVisible(m_ui->verticalLayout_2->contentsRect().height() > m_ui->sa_side_indicators->height());
}

void _main_win::on_btn_add_side_indicators_clicked()
{
    _form_add_indicators f_add(get_side_indicators_list());
    connect(&f_add, SIGNAL(show_side_indicator(QString)), this, SLOT(show_side_indicator(QString)));
    f_add.setGeometry(
                x() + m_ui->fr_side_indicators_area->x() - f_add.geometry().width(),
                y() + m_ui->fr_side_indicators_area->y() + m_ui->fr_side_indicators_area->height() - f_add.height(),
                f_add.geometry().width(),
                f_add.geometry().height());
    f_add.setWindowFlags(Qt::FramelessWindowHint);
    f_add.exec();
    disconnect(&f_add, SIGNAL(show_side_indicator(QString)), this, SLOT(show_side_indicator(QString)));
}

void _main_win::show_side_indicator(QString name_ind)
{
    set_state_side_indicator(name_ind, true);
    align_side_indicators();
}

void _main_win::hide_side_indicator(void)
{
    _widget_indicator* wi = (_widget_indicator*)sender();
    wi->setVisible(false);
    align_side_indicators();
}

// --------------------------------------------------------------------------
// VIDEO GW
// --------------------------------------------------------------------------
_main_win::_video_gw::_video_gw(const std::string &cams)
{
    int i=0;
    while(1)
    {
        _cam c;
        c.url=_text_help::get_field_from_st(cams,',',i);
        if (c.url.empty()) break;
        c.send_at.stamp(0);
        cm_cams.push_back(c);
        i++;
    }
}

// --------------------------------------------------------------------------
int _main_win::_video_gw::Poll()
{
    if (!cm_need_write)
    {
        Sleep(500);
        return 0;
    }

    _http_options opts;
    _http_answer rez;

    list<_cam>::iterator it=cm_cams.begin();
    while(it!=cm_cams.end())
    {
        _time tn;
        tn.set_now();

        if (tn.diff_s(it->send_at)>1) // each 1 s
        {
            _http_client *hc=create_http_client();
            hc->get(it->url,opts,rez);
            hc->release();
            it->send_at.set_now();
        }

        it++;
    }

    return 0;
}




