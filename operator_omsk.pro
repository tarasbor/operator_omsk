QT += widgets
QT += widgets-private
QT -= network
QT -= opengl
QT += multimedia printsupport

TARGET = am-client

QMAKE_CXXFLAGS += -std=c++0x

DEFINES *= WINDOWS
#DEFINES *= __GNUC__
DEFINES -= UNICODE
#DEFINES *=SVT_EMU
DEFINES += QT_USE_USING_NAMESPACE


CONFIG(debug, debug|release) {
    DEFINES *= DEBUG
    message("--- debug ---")
}
else {
    DEFINES *= QT_NO_DEBUG
    message("--- release ---")
}

win32 {
        RC_FILE     += rc_info.rc
        OTHER_FILES += rc_info.rc
}

#INCLUDE_DIRECTORIES(
#    ${Qt5Widgets_PRIVATE_INCLUDE_DIRS}
#)
#INCLUDEPATH += Qt5Widgets_PRIVATE_INCLUDE_DIRS

#office
#win32:INCLUDEPATH += C:/Qt/5.2.1/5.2.1/mingw48_32/include/QtWidgets/5.2.1/QtWidgets
#win32:INCLUDEPATH += C:/Qt/5.2.1/5.2.1/mingw48_32/include/QtCore/5.2.1/QtCore


#INCLUDEPATH += C:/Qt/Qt5.2.1/5.2.1/mingw48_32/include/QtWidgets/5.2.1/QtWidgets
#INCLUDEPATH += C:/Qt/Qt5.2.1/5.2.1/mingw48_32/include/QtCore/5.2.1/QtCore

#home
##win32:INCLUDEPATH += C:/DevTools/Qt/5.2.1/5.2.1/mingw48_32/include/QtWidgets/5.2.1/QtWidgets
##win32:INCLUDEPATH += C:/DevTools/Qt/5.2.1/5.2.1/mingw48_32/include/QtCore/5.2.1/QtCore


INCLUDEPATH += ../../libcommon/
INCLUDEPATH += ../../libcommon//system
INCLUDEPATH += ../../libcommon//log2
INCLUDEPATH += ../../libcommon//thread
INCLUDEPATH += ../../libcommon//xml
INCLUDEPATH += ../../libcommon//xml/lib
INCLUDEPATH += ../../libcommon//xml/bin
INCLUDEPATH += ../../libcommon//net
INCLUDEPATH += ../../libcommon//com
INCLUDEPATH += ../../libcommon//ogl
INCLUDEPATH += ../../libcommon//freeglut/mingw/include/GL
INCLUDEPATH += ../eq_common
INCLUDEPATH += ./ui

HEADERS += \
    ../../libcommon/net/net_lib.h \
    ../../libcommon/net/net_lib_error.h \
    ../../libcommon/net/net_line_tcp.h \
    ../../libcommon/net/net_line.h \
    ../../libcommon/net/net_datagram_client.hpp \
    ../../libcommon/net/datagram.hpp \
    ../../libcommon/net/net_http_client.h \
    ../../libcommon/net/net_http_req.h \
    ../../libcommon/system/time.hpp \
    ../../libcommon/system/codepage.hpp \
    ../../libcommon/system/error.hpp \
    ../../libcommon/system/text_help.hpp \
    ../../libcommon/thread/clthread.hpp \
    ../../libcommon/xml/ut_xml_par.hpp \
    ../../libcommon/xml/ut_xml_tag.hpp \
    ../../libcommon/log2/log.hpp \
    ../../libcommon/log2/log_file_writer.hpp \
    ../../libcommon/com/rs.hpp \
    ../../libcommon/ogl/ogl_utils.hpp \
    ../../libcommon/ogl/ogl_gui.hpp \
    ../../libcommon/net/net_server_tcp.h \
    ../../libcommon/net/net_cmd_client.hpp \
    ../../libcommon/system/config_parser.hpp \
    ../../libcommon/com/rs_iface.hpp \
    ui/tray.hpp \
    settings.hpp \
    version.h \
    list_selection_dlg.h \
    auth_dlg.hpp \
    splash_screen.hpp \
    hw_drv.hpp \
    overlay_label.hpp \
    main_win.hpp \
    combo_2line_delegate.hpp \
    form_add_indicators.hpp \
    office_indicators_model.hpp \
    widget_indicator.hpp \
    v_scroll_bar.hpp \
    rdg_log.hpp \
    ../eq_common/eq_auth.hpp \
    ../eq_common/eq_classes.hpp \
    ../eq_common/eq_common.hpp \
    ../eq_common/eq_err_code.hpp \
    ../eq_common/eq_net_auth.hpp \
    ../eq_common/eq_net_client.hpp \
    ../eq_common/eq_net_client_rmo.hpp \    
    ../eq_common/console_hw.hpp \
    ../../libcommon/system/file_utils.hpp \
    ../../libcommon/system/udefs.hpp \
    ui/messagedifficulttoclose.h \
    ui/settings_dlg.hpp \
    ../eq_common/version.hpp \
    eqaudiorecorder.h \
    ../../libcommon/json/json.hpp

SOURCES += \
    ../../libcommon/net/net_lib_error.cpp \
    ../../libcommon/net/net_line_tcp.cpp \
    ../../libcommon/net/datagram.cpp \
    ../../libcommon/net/net_datagram_client.cpp \
    ../../libcommon/net/net_http_client.cpp \
    ../../libcommon/net/net_http_req.cpp \
    ../../libcommon/thread/clthread.cpp \
    ../../libcommon/system/time.cpp \
    ../../libcommon/system/codepage.cpp \
    ../../libcommon/system/error.cpp \
    ../../libcommon/system/text_help.cpp \
    ../../libcommon/xml/lib/ut_xml_par.cpp \
    ../../libcommon/xml/lib/ut_xml_tag.cpp \
    ../../libcommon/log2/log.cpp \
    ../../libcommon/log2/log_file_writer.cpp \
    ../../libcommon/net/net_server_tcp.cpp \
    ../../libcommon/net/net_cmd_client.cpp \
    ../../libcommon/com/rs.cpp \
    ../../libcommon/system/config_parser.cpp \
    ../../libcommon/com/rs_iface.cpp \
    ui/tray.cpp \
    settings.cpp \
    list_selection_dlg.cpp \
    auth_dlg.cpp \
    splash_screen.cpp \
    hw_drv.cpp \
    overlay_label.cpp \
    main_win.cpp \
    main.cpp \
    combo_2line_delegate.cpp \
    form_add_indicators.cpp \
    widget_indicator.cpp \
    v_scroll_bar.cpp \
    main_win_ticket.cpp \
    ../eq_common/eq_err_code.cpp \
    ../eq_common/eq_net_auth.cpp \
    ../eq_common/eq_net_client.cpp \
    ../eq_common/eq_net_client_rmo.cpp \
    ../eq_common/console_hw.cpp \
    ../eq_common/eq_classes.cpp \
    ../../libcommon/system/file_utils.cpp \
    ui/messagedifficulttoclose.cpp \
    ui/settings_dlg.cpp \
    eqaudiorecorder.cpp


#online icon maker
#http://www.rw-designer.com/image-to-icon

OTHER_FILES += \
    rc_info.rc \
    DingDong.wav \
    signal3.png \
    signal2.png \
    signal1.png \
    ui/images/signal3.png \
    ui/images/signal2.png \
    ui/images/signal1.png \
    ui/images/signal0.png \
    ui/images/bg.png \
    ui/images/tb_bg.png \
    ui/images/mainwnd_rest.png \
    ui/images/mainwnd_min.png \
    ui/images/mainwnd_close.png \
    ui/images/mainwnd_title.png \
    ui/images/mainwnd_icon.png \
    ui/images/mainwnd_title_bg.png \
    ui/images/btn_cmd_normal.png \
    ui/images/btn_cmd_pressed.png \
    ui/images/btn_cmd_hover.png \
    mainwnd_icon.ico \
    ui/images/panel_ticket.png \
    ui/images/panel_queues.png \
    ui/images/connection_ok.png \
    ui/images/connection_err.png \
    ui/images/auth_dlg.png \
    ui/images/A_red-64x64.png \
    ui/images/A_blue-64x64.png \
    ui/images/btn_qleft_pressed.png \
    ui/images/btn_qleft_disabled.png \
    ui/images/btn_qleft_hover.png \
    ui/images/btn_qleft_normal.png \
    ui/images/btn_qright_pressed.png \
    ui/images/btn_qright_disabled.png \
    ui/images/btn_qright_hover.png \
    ui/images/btn_qright_normal.png \
    ui/images/btn_pressed.png \
    ui/images/btn_normal.png \
    ui/images/btn_hover.png \
    ui/images/btn_disabled.png \
    ui/images/btn_div_pressed.png \
    ui/images/btn_div_disabled.png \
    ui/images/btn_div_hover.png \
    ui/images/btn_div_normal.png \
    ui/images/btn_issless_pressed.png \
    ui/images/btn_issless_disabled.png \
    ui/images/btn_issless_hover.png \
    ui/images/btn_issless_normal.png \
    ui/images/btn_issmore_pressed.png \
    ui/images/btn_issmore_disabled.png \
    ui/images/btn_issmore_hover.png \
    ui/images/btn_issmore_normal.png \
    ui/images/btn_iss_pressed.png \
    ui/images/btn_iss_disabled.png \
    ui/images/btn_iss_hover.png \
    ui/images/btn_iss_normal.png \
    ui/images/tb_btn_break_pressed.png \
    ui/images/tb_btn_break_disabled.png \
    ui/images/tb_btn_break_hover.png \
    ui/images/tb_btn_break_normal.png \
    ui/images/tb_btn_relogin_pressed.png \
    ui/images/tb_btn_relogin_disabled.png \
    ui/images/tb_btn_relogin_hover.png \
    ui/images/tb_btn_relogin_normal.png \
    ui/images/tb_btn_report_pressed.png \
    ui/images/tb_btn_report_disabled.png \
    ui/images/tb_btn_report_hover.png \
    ui/images/tb_btn_report_normal.png \
    ui/images/tb_btn_settings_pressed.png \
    ui/images/tb_btn_settings_disabled.png \
    ui/images/tb_btn_settings_hover.png \
    ui/images/tb_btn_settings_normal.png \
    ui/images/btn_win_close_pressed.png \
    ui/images/btn_win_close_normal.png \
    ui/images/btn_win_close_hover.png \
    ui/images/btn_win_close_disabled.png \
    ui/images/btn_win_minimize_pressed.png \
    ui/images/btn_win_minimize_normal.png \
    ui/images/btn_win_minimize_hover.png \
    ui/images/btn_win_minimize_disabled.png \
    ui/images/btn_close_pressed.png \
    ui/images/btn_close_normal.png \
    ui/images/btn_close_hover.png \
    ui/images/btn_close_disabled.png \
    ui/images/vsb.png \
    ui/images/sb_up_pressed.png \
    ui/images/sb_up_normal.png \
    ui/images/sb_up_hover.png \
    ui/images/sb_slider.png \
    ui/images/sb_down_pressed.png \
    ui/images/sb_down_normal.png \
    ui/images/sb_down_hover.png \
    ui/images/plus-sign_green.png \
    ui/images/flipper.png \
    am-client.ini

RESOURCES += \
    operator_omsk.qrc



#MINGW_LIBS = C:\Qt\Tools\mingw530_32\i686-w64-mingw32\lib
MINGW_LIBS = C:\Qt\Qt5.9.7\Tools\mingw530_32\i686-w64-mingw32\lib


LIBS += $${MINGW_LIBS}\libws2_32.a
LIBS += $${MINGW_LIBS}\libwinmm.a


FORMS += \
    list_selection_dlg.ui \
    auth_dlg.ui \
#    settings_dlg.ui \
    splash_screen.ui \
    main_win.ui \
    form_add_indicators.ui \
    widget_indicator.ui \
    v_scroll_bar.ui \
    ui/messagedifficulttoclose.ui \
    ui/settings_dlg.ui
