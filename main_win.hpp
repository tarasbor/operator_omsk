#ifndef __MAIN_WIN_HPP__
#define __MAIN_WIN_HPP__

#include <QObject>
#include <QMainWindow>
#include <QtGui>
#include <QGraphicsObject>
#include <QMessageBox>
#include <QTimer>
#include <QDebug>

#include <time.hpp>
#include <error.hpp>
#include <console_hw.hpp>

#include "rdg_log.hpp"

#include "tray.hpp"
#include "widget_indicator.hpp"
#include "v_scroll_bar.hpp"

#include "ui/messagedifficulttoclose.h"
#include "eqaudiorecorder.h"

extern _log_file_writer* g_log_file_writer;
extern _eq_net_client_rmo* srv;
extern _mark_console* mark_console;

namespace Ui { class main_win; }

#define SRV_CHECK_STATUS_AND_LOCK \
    if(!srv) \
    { \
        err_msg(QMessageBox::Critical); \
        THROW_EXCEPTION("Server object is not initialized."); \
    } \
    srv->lock(); \
    if(srv->status() != _eq_net_client_rmo::client_status_ok) \
    { \
        srv->unlock(); \
        err_msg(QMessageBox::Critical); \
        THROW_EXCEPTION("Server object has inconsistent state."); \
    }

#define SRV_CHECK_STATUS \
    if(!srv) \
    { \
        err_msg(QMessageBox::Critical); \
        THROW_EXCEPTION("Server object is not initialized."); \
    } \
    srv->lock(); \
    if(srv->status() != _eq_net_client_rmo::client_status_ok) \
    { \
        srv->unlock(); \
        err_msg(QMessageBox::Critical); \
        THROW_EXCEPTION("Server object has inconsistent state."); \
    } \
    srv->unlock();


class _main_win : public QMainWindow
{
        Q_OBJECT
    
    public:

        explicit _main_win(QWidget *parent = 0);
        ~_main_win();

        void init_form();

    signals:

        void show_tray_messsage(QString title, QString message);

    private:

        EqAudioRecorder aRecorder;

        //wa4timer
        class _timer : public _thread
        {
            private:
                _main_win* main_win;

            public:
                _timer(_main_win* w) : main_win(w) {}
                virtual ~_timer() {}

                virtual int Poll();
        };
        _timer* timer;

        class _video_gw:public _thread
        {
            private:
                bool cm_need_write;
                struct _cam
                {
                    std::string url;
                    _time send_at;
                };

                list<_cam> cm_cams;

            public:
                _video_gw(const std::string &cams);
                virtual ~_video_gw() {}

                virtual int Poll();
                virtual void need_write(bool n) { cm_need_write=n; };
        };
        _video_gw *video_gw;

        Ui::main_win* m_ui;

        //QSettings m_user_settings;
        //QSettings m_program_settings;

        _v_scroll_bar* m_sb_indicators;

        map<string, int> m_queues_prev_size;
        _v_scroll_bar* m_sb_queues;

        QTime m_ticket_in_proc_service_time;

        //Указатель на кнопку вызова следующего талона из любой очереди
        //Кнопка отображается в конце списка всех очередей (по настройке)
        QPushButton *buttoNextTicket2;
        QLabel *labelNextTicket2;
        QString queueIdCallNextTicket;

//        void set_visible_indicators(void) {};
//        void set_state(QString win_state, QString msg) {};
        void update_queue_buttons(void);
        void on_ticket_open(void);
        void on_ticket_close(void);
        void printticket(QString queuename, QString ticketnum);

        void exit_by_error(QString name_file, QString message, int kz, bool release_monitor)  throw(_error);

        inline const QList<_widget_indicator*> get_side_indicators_list();
        void set_state_side_indicator(QString name_ind, bool state);
        void add_side_indicator(QString name, QString text, bool state);
        int  count_visible_side_indicators(void);
        void align_side_indicators();
        _widget_indicator* get_indicator(QString name_indicator);
        bool init_scroll_bar(QObject* object, QEvent* event, _v_scroll_bar* sb, const QScrollBar* qsb);
        QString ms_to_hms(int sec);

        void err_msg(QMessageBox::Icon icon, const char* text = NULL);

    public:

        _rdg_tray* m_tray;
        MessageDifficultToClose messageBoxT; //Используется для вывода сообщения о превышенном времени ожедания

    protected:

        bool eventFilter(QObject* obj, QEvent* event);
        void set_cur_time(void);
        void closeEvent(QCloseEvent* event);
        //wa4timer
        //void timerEvent (QTimerEvent* event); //refresh the window, clock, indicators
    //    void changeEvent(QEvent*);

       _time m_prev_any_change_time;

        int m_qi; // index of selected queue

    signals:
        //wa4timer
        void timerEventPost(void);

    private slots:

        //wa4timer
        void timerEvent(); //refresh the window, clock, indicators//wa4timer
        //
        void on_btn_win_minimize_clicked();
        void on_btn_win_close_clicked();
        //
        void on_btn_settings_clicked();
        void on_btn_report_clicked();
        void on_btn_relogin_clicked();
        void on_btn_break_clicked();
        //
        void on_btn_add_side_indicators_clicked();
        void show_side_indicator(QString name_ind);
        void hide_side_indicator(void);
        //
        void do_call_next_ticket();
        void do_call_next_ticket_by_num();
        //
        void on_btn_less_issues_clicked();
        void on_btn_more_issues_clicked();
        void on_btn_issues_1_clicked();
        void on_btn_issues_2_clicked();
        void on_btn_issues_3_clicked();
        void on_btn_issues_4_clicked();
        void on_btn_issues_5_clicked();
        //
        void on_btn_recall_clicked();
        void on_btn_return_to_queue_clicked();
        void on_btn_close_ticket_clicked();
        void on_btn_forward_clicked();
        void on_btn_set_absent_clicked();
        void on_btn_create_new_ticket_clicked();
};

#endif
