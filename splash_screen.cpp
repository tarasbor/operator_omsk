#include "splash_screen.hpp"
#include "ui_splash_screen.h"

#ifdef DEBUG
    #include <QDebug>
#endif

_splash_screen::_splash_screen(QWidget *parent) :
    QDialog(parent, Qt::MSWindowsFixedSizeDialogHint | Qt::FramelessWindowHint),
    ui(new Ui::_splash_screen),
    currentPixmap(0)
{
    ui->setupUi(this);

    ui->lbl_app_name->setText(QApplication::applicationName());

    QImage img;
    img.load(":/signal0");
    pixmaps.push_back(QPixmap::fromImage(img));
    img.load(":/signal1");
    pixmaps.push_back(QPixmap::fromImage(img));
    img.load(":/signal2");
    pixmaps.push_back(QPixmap::fromImage(img));
    img.load(":/signal3");
    pixmaps.push_back(QPixmap::fromImage(img));

    connect(&timer, SIGNAL(timeout()), SLOT(changeImage()));
    timer.start(100);
    changeImage();
}

_splash_screen::~_splash_screen()
{
    delete ui;
}

void _splash_screen::changeImage()
{
    if(currentPixmap >= pixmaps.length())
        currentPixmap = 0;
    ui->lbl_animation->setPixmap(pixmaps.at(currentPixmap));
    currentPixmap++;
}
