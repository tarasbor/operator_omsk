#include "form_add_indicators.hpp"
#include "ui_form_add_indicators.h"
#include "widget_indicator.hpp"

#ifdef DEBUG
    #include <QDebug>
#endif

const int timeout_interval = 3000;

_form_add_indicators::_form_add_indicators(const QList<_widget_indicator*>& wis, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::_form_add_indicators)
{
    ui->setupUi(this);

    foreach(_widget_indicator* wi, wis)
        if(wi->isHidden())
        {
            hidden_indicators.push_back(wi->get_name());
            hidden_indicators_name.push_back(wi->objectName());
        }

    int count_indicators = hidden_indicators.size();

    ui->lbl_name_indicator->setText(hidden_indicators.at(0));
    ui->lbl_name_indicator->setObjectName(hidden_indicators_name.at(0));
    ui->btn_add_indicator->setObjectName(hidden_indicators_name.at(0));

    QHBoxLayout*    lay_indicator;
    QLabel*         lbl_name_indicator;
    QSpacerItem*    horizontalSpacer;
    QPushButton*    btn_add_indicator;
    QSpacerItem*    horizontalSpacer_3;

    for(int i = 1; i < hidden_indicators.size(); ++i)
    {
        lay_indicator = new QHBoxLayout();
        lay_indicator->setObjectName(QString::fromUtf8("lay_indicator"));
        lbl_name_indicator = new QLabel(this);
        lbl_name_indicator->setObjectName(hidden_indicators_name.at(i));
        lbl_name_indicator->setMinimumSize(QSize(300, 0));
        lbl_name_indicator->setText(hidden_indicators.at(i));

        lbl_name_indicator->setStyleSheet(ui->lbl_name_indicator->styleSheet());
        lay_indicator->addWidget(lbl_name_indicator);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        lay_indicator->addItem(horizontalSpacer);

        btn_add_indicator = new QPushButton(this);
        btn_add_indicator->setObjectName(hidden_indicators_name.at(i));
        btn_add_indicator->setMinimumSize(QSize(0, 24));
        btn_add_indicator->setMaximumSize(QSize(24, 16777215));
        btn_add_indicator->setStyleSheet(QString::fromUtf8("border-image: url();"));
        btn_add_indicator->setIcon(QIcon(":/images/plus.png"));

        connect(btn_add_indicator, SIGNAL(clicked()), this, SLOT(on_btn_add_indicator_clicked()));
        lay_indicator->addWidget(btn_add_indicator);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        lay_indicator->addItem(horizontalSpacer_3);

        ui->lay_indicators->addLayout(lay_indicator);
    }

    QSizeF sz = ui->lay_indicator->sizeHint();
    setGeometry(0,0, geometry().width(), geometry().height() + (count_indicators-1) * sz.height());

    installEventFilter(this);
    timer_id = startTimer(timeout_interval);

    setFocus();
}

_form_add_indicators::~_form_add_indicators()
{
    delete ui;
}

void _form_add_indicators::on_btn_add_indicator_clicked()
{
    emit show_side_indicator(sender()->objectName());

    // hide selected row

    QList<QWidget*> wi = ui->fr_indicators->findChildren<QWidget*>(sender()->objectName());
    QWidget* cur_widget;
    foreach(cur_widget, wi)
        delete cur_widget;

    //close form if it's empty

    QRegExp rx("ind_");
    QList<QWidget*> all_ind = ui->fr_indicators->findChildren<QWidget*>(rx);
    if(all_ind.size() == 0)
        close();
}

bool _form_add_indicators::eventFilter (QObject* watched, QEvent* event)
{
    if(event->type() == QEvent::Leave || event->type() == QEvent::FocusOut)
        close();

    return QDialog::eventFilter(watched, event);
}
void _form_add_indicators::timerEvent(QTimerEvent *event)
{
    close();
}
