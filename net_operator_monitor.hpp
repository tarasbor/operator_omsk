#ifndef __NET_OPERATOR_RMO_H__
#define __NET_OPERATOR_RMO_H__

//#include <net_cmd_client.hpp>
//#include <operator_monitor.hpp>
#include <net_ctrl.hpp>

typedef i_eq_ctrl<i_eq_oper_mon> i_eq_oper_mon_ctrl;

extern i_eq_oper_mon_ctrl* ctrl;

i_eq_oper_mon_ctrl* create_ctrl(const string &ip, int port, const string &uname, const string &upass);

#endif
