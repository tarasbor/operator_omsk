#include <log.hpp>
#include <clthread.hpp>
#include <ut_xml_par.hpp>
#include <text_help.hpp>

#include <net_datagram_client.hpp>
#include <net_cmd_client.hpp>
#include "net_operator_monitor.hpp"

#ifdef LOAD_FROM_XML
#include <debug.hpp>
#include <QDateTime>
#include <debug_extern.h>
#define UPDATE_EVRY_TICK_  // Если определён UPDATE_EVRY_TICK - обновления выполняются в каждом цикле
                          // Если не определён - в соответствии с значениями времени из лог-файла.
const int           count_dbg_files=2;
#ifdef UPDATE_EVRY_TICK
const int           time_sleep=1000;
#else
const int           time_sleep=500;
#endif
static int          cur_dbg_file=1;
static int          c_sleep=0;
static _time::time_val_i   diff_time=0;
QString cur_xml;
QString next_xml;

#endif

#define DEBUG_LEVEL		10

class _oper;
class _wplace;
class _q;
class _qs_list;
class _ticket;
class _tickets_list;

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
class _oper : public i_eq_oper_mon::i_oper
{
private:
    _id cm_id;

    string cm_name;
    string cm_login;

//    int cm_wait_ms_time_at_cday;
//    int cm_service_ms_time_at_cday;

    int cm_serviced_ticket_count_at_cday;

protected:

    virtual ~_oper() {}
    virtual void release() { delete this; }

public:

    _oper() : /*cm_id(0), */cm_name(""), cm_login(""), cm_serviced_ticket_count_at_cday(0) {}

    virtual const _id &id() const { return cm_id; }
    virtual const string &name() const { return cm_name; }
    virtual const string &login() const { return cm_login; }

    virtual int serviced_ticket_count_at_cday() const { return cm_serviced_ticket_count_at_cday; }

//    virtual int wait_ms_time_at_cday() const { return cm_wait_ms_time_at_cday; }
//    virtual int service_ms_time_at_cday() const { return cm_service_ms_time_at_cday; }
  };

// ---------------------------------------------------------------------
// implementation for local wplace
// ---------------------------------------------------------------------
class _wplace : public i_eq_oper_mon::i_wplace
{
    friend class _monitor;

private:

    _id m_id;
    string m_name;
    _oper *m_oper;

protected:
    virtual ~_wplace() {}
    virtual void release() { delete this; }

public:

    virtual const _id &id() const { return m_id; }
    virtual const string &name() const { return m_name; }
//    virtual const string &department_name() const;

    virtual int limit_wait_ticket_ms_time() const { return 300; } // 5 min
    virtual int limit_service_ticket_ms_time() const { return 300; } // 5 min

    virtual const i_eq_oper_mon::i_oper *cur_oper() const { return m_oper; }
    virtual const i_eq_oper_mon::i_ticket_in_proc *cur_ticket() const { return NULL; }
    virtual const i_eq_oper_mon::i_qs_list* qs_list() const { return NULL; }

    virtual void wp_lock() const {}
    virtual void wp_unlock() const {}
};

// ---------------------------------------------------------------------
// implementation for local q
// ---------------------------------------------------------------------
class _q : public i_eq_oper_mon::i_q
{
private:

    _id cm_id;
    string cm_name;
    string cm_vname;

    _tickets_list *t_list;

protected:

    virtual ~_q() {}
    virtual void release() { delete this; }

public:

    _q() : t_list(NULL) {}

    virtual const _id &id() const { return cm_id; }
    virtual const string &name() const { return cm_name; }
    virtual const string &vname() const { return cm_vname; }

    virtual const i_eq_oper_mon::i_tickets_list *tickets_list() const { /*return t_list;TODO*/ }
};

// ---------------------------------------------------------------------
// implementation for local qs list
// ---------------------------------------------------------------------
class _qs_list : public i_eq_oper_mon::i_qs_list
{
private:

    typedef list<_q *> _qs;
    _qs qs;

protected:

    virtual ~_qs_list() {}
    virtual void release() { delete this; }

public:

    _qs_list() {}

    class iterator : public i_eq_oper_mon::i_qs_list::iterator
    {
    private:
        const _qs *qs;
        _qs::const_iterator rit;
    public:
        void release() {}
        iterator(const _qs *qsl):qs(qsl),rit(qsl->begin()) { }
        virtual void operator++(int) { rit++; }
        virtual bool eol() const { return (rit==qs->end()); }
        virtual const _q *operator->() { return (*rit); }
        virtual operator const i_eq_oper_mon::i_q*() { return (*rit); }
    };
    virtual iterator *begin() const { return new iterator(&qs); }
    virtual int count() const  { return qs.size(); }
};

// ---------------------------------------------------------------------
// implementation for waiting ticket
// ---------------------------------------------------------------------
class _ticket : public i_eq_oper_mon::i_ticket
{
private:

    _id cm_id;
    _time cm_create_time;
    string cm_number;

protected:

    virtual ~_ticket() {}
    virtual void release() { delete this; }

public:

    _ticket() : /*cm_id(0), */cm_create_time(0), cm_number("") {}

    virtual const _id &id() const { return cm_id; }
    virtual const string &number() const { return cm_number; }
    virtual const _time &create_time() const { return cm_create_time; }

    virtual void call() const {}
};

// ---------------------------------------------------------------------
// implementation for ticket in processing
// ---------------------------------------------------------------------
class _ticket_in_proc : public i_eq_oper_mon::i_ticket_in_proc
{
private:

    _id cm_id;
    _time cm_create_time;
    _time cm_start_proc_time;
    string cm_number;

protected:

    virtual ~_ticket_in_proc() {}
    virtual void release() { delete this; }

public:

    _ticket_in_proc() : /*cm_id(0), */cm_create_time(0), cm_number("") {}

    virtual const _id &id() const { return cm_id; }
    virtual const string &number() const { return cm_number; }
    virtual const _time &create_time() const { return cm_create_time; }
    virtual const _time &start_proc_time() const { return cm_start_proc_time; }

    virtual void recall() const {}
    virtual void close_as_err() const {}
    virtual void close_as_absent() const {}
    virtual void close_as_served(int issues_count) const {}

    virtual void redirect(const _q& q) {}
    virtual void back_to_q() {}
};


// ---------------------------------------------------------------------
// implementation for waiting tickets list
// ---------------------------------------------------------------------
class _tickets_list : public i_eq_oper_mon::i_tickets_list
{
private:

    _q *cm_in_q;
    typedef list<_ticket *> _tickets;
    _tickets tickets;

protected:

    virtual ~_tickets_list() {}
    virtual void release() { delete this; }

public:

    _tickets_list(_q *inq) {}

    class iterator:public i_eq_oper_mon::i_tickets_list::iterator
    {
    private:
        const _tickets *tickets;
        _tickets::const_iterator rit;
    public:
        void release() {}
        iterator(const _tickets *tl):tickets(tl),rit(tl->begin()) { }
        virtual void operator++(int) { rit++; }
        virtual bool eol() const { return (rit==tickets->end()); }
        virtual const _ticket *operator->() { return (*rit); }
        virtual operator const i_eq_oper_mon::i_ticket*() { return (*rit); }
    };
    virtual iterator *begin() const { return new iterator(&tickets); }
    virtual int count() const  { return tickets.size(); }
  };

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------

class _monitor : public i_eq_oper_mon
{
private:

    _wplace wplace;
    _locker cm_lock;

protected:

    virtual ~_monitor() {}
    virtual void release() { delete this; }

public:

    _monitor() {}

    virtual const _wplace& cur_wplace() const { return wplace; }
    virtual const _qs_list* qs_list_all() const {return NULL;}

    virtual void lock() { cm_lock.lock(); }
    virtual bool try_lock() { return cm_lock.try_lock(); }
    virtual void unlock() { cm_lock.unlock(); }
};

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------

class _ctrl;

i_eq_oper_mon_ctrl* ctrl;

// ---------------------------------------------------------------------

class _ctrl_timer : public _thread
{
private:
    _ctrl *ctrl;
public:
    _ctrl_timer(_ctrl *c):ctrl(c) {}
    virtual ~_ctrl_timer() {}
    virtual int Poll() {}//TODO
};

// ---------------------------------------------------------------------

class _ctrl : public i_eq_oper_mon_ctrl
{
    friend i_eq_oper_mon_ctrl* create_ctrl(const string &ip, int port, const string &uname, const string &upass);

private:
    _net_cmd_client *nc;
    _net_datagram_client *dc;

    _ctrl_timer *timer;

    i_eq_ctrl_base::_status cm_status;

    string cm_ip;
    string cm_port_str;
    string cm_data_port_str;
    string cm_uname;
    string cm_upass;

    int con_count;

protected:

    _ctrl(const string &ip, int port, const string &uname, const string &upass);
    virtual ~_ctrl();

public:

    virtual void release() { delete this; }

    virtual i_eq_ctrl_base::_status status() const { return cm_status; }

    virtual bool updated() { return false; } //TODO
};

// ---------------------------------------------------------------------

_ctrl::_ctrl(const string &ip,int port,const string &uname,const string &upass) :
    nc(NULL), dc(NULL), timer(NULL),
    cm_status(i_eq_ctrl_base::status_do_connect),
    cm_ip(ip), cm_uname(uname), cm_upass(upass), con_count(0)
{
    char tmp[32];
    cm_port_str=itoa(port,tmp,10);

    mon = new _monitor;

    timer=new _ctrl_timer(this);
    timer->SetSleepTime(100);
    timer->Start();
}

// ---------------------------------------------------------------------

_ctrl::~_ctrl()
{
    if (timer)
    {
        timer->Kill();
        while(timer->GetStatus()!=TH_STATUS_KILLED) Sleep(10);
        delete timer;
        timer=NULL;
    }

//    disconnect();
    if (nc) nc->release();
    if (dc) dc->release();
    if (mon) mon->release();
}

// ---------------------------------------------------------------------

i_eq_oper_mon_ctrl* create_ctrl(const string &ip, int port, const string &uname, const string &upass)
{
    return new _ctrl(ip, port, uname, upass);
}
