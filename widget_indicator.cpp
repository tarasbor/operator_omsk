#include "widget_indicator.hpp"
#include "ui_widget_indicator.h"

#ifdef DEBUG
    #include <QDebug>
#endif

_widget_indicator::_widget_indicator(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::_widget_indicator),
    v_red(false)
{
    ui->setupUi(this);
}

_widget_indicator::~_widget_indicator()
{
    delete ui;
}

void _widget_indicator::set_name(const QString& name)
{
    ui->lbl_name->setText(name);
    ui->lbl_name->repaint();
}

QString _widget_indicator::get_name(void)
{
    return ui->lbl_name->text();
}

void _widget_indicator::set_value(const QString& value)
{
    ui->lbl_value->setText(value);
    ui->lbl_value->repaint();
}

void _widget_indicator::set_value_red(bool red)
{
    v_red = red;

    if(red)
        ui->lbl_value->setStyleSheet("font: 75 32px 'Helvetica'; border-image: url();color: rgb(255, 85, 0);");
    else
        ui->lbl_value->setStyleSheet("font: 75 32px 'Helvetica'; border-image: url();");
}

bool _widget_indicator::is_value_red()
{
    return v_red;
}


void _widget_indicator::on_btn_close_clicked()
{
    emit hide_indicator();
}

