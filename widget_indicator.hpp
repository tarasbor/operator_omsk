#ifndef WIDGET_INDICATOR_HPP
#define WIDGET_INDICATOR_HPP

#include <QWidget>

namespace Ui {
class _widget_indicator;
}

class _widget_indicator : public QWidget
{
    Q_OBJECT

    private:
        Ui::_widget_indicator *ui;
        bool v_red; //Красный-ли цвет текста

    public:
        explicit _widget_indicator(QWidget* parent = 0);
        ~_widget_indicator();

        void    set_name(const QString& name);
        QString get_name(void);
        void    set_value(const QString& value);
        void    set_value_red(bool red);
        bool    is_value_red();

    private slots:
        void on_btn_close_clicked();

    signals:
        void hide_indicator(void);
};

#endif // WIDGET_INDICATOR_HPP
